module.exports = {
  printWidth: 79,
  trailingComma: "all",
  arrowParens: "avoid",
  overrides: [
    {
      files: "*.js",
      options: {
        parser: "flow",
      },
    },
  ],
};
