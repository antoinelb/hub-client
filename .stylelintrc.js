module.exports = {
  plugins: ["stylelint-order", "stylelint-prettier"],
  extends: "stylelint-config-recommended",
  rules: {
    "order/order": ["custom-properties", "declarations"],
    "order/properties-alphabetical-order": true,
    "no-descending-specificity": null,
    "no-empty-source": null,
    "prettier/prettier": true,
  },
};
