module Page.Budget exposing
    ( Model
    , Msg
    , Page
    , fromPage
    , initCmd
    , initModel
    , routeParser
    , update
    , updatedModel
    , view
    )

import Browser.Navigation as Nav
import Config exposing (apis)
import Html exposing (Html, a, div, main_, nav, text)
import Html.Attributes exposing (class, href, id)
import Html.Events exposing (onClick)
import Json.Decode as Decode exposing (Decoder)
import Page.Budget.Account as Account
    exposing
        ( Account
        , AccountType
        , accountDecoder
        , accountTypeDecoder
        )
import Page.Budget.Overview as Overview
import Page.Budget.Transactions as Transactions
    exposing
        ( Transaction
        , transactionDecoder
        )
import Time
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Http exposing (DetailedError, expectJson, get, handleError)



-- model


type Page
    = Overview
    | Transactions
    | Account String


type alias Model =
    { loading : Bool
    , page : Page
    , accountTypes : List AccountType
    , accounts : List Account
    , transactions : List Transaction
    , overviewPage : Overview.Model
    , transactionsPage : Transactions.Model
    }


initModel : Page -> Model
initModel page =
    { loading = True
    , page = page
    , accountTypes = []
    , accounts = []
    , transactions = []
    , overviewPage = Overview.initModel
    , transactionsPage = Transactions.initModel
    }


updatedModel : Model -> Page -> Model
updatedModel model page =
    { model | page = page }



-- update


type Msg
    = NoOp
    | ChangePage Page
    | ReceivedAccountTypes Bool (Result DetailedError (List AccountType))
    | ReceivedAccounts Bool (Result DetailedError (List Account))
    | ReceivedTransactions (Result DetailedError (List Transaction))
    | OverviewMsg Overview.Msg
    | TransactionsMsg Transactions.Msg


update : Msg -> Model -> { model : Model, cmd : Cmd Msg, error : Maybe (Maybe String) }
update msg model =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing }
    in
    case msg of
        NoOp ->
            return

        ChangePage page ->
            { return | model = { model | page = page } }

        ReceivedAccountTypes getAccountsNext result ->
            case result of
                Ok accountTypes ->
                    let
                        cmd =
                            if getAccountsNext then
                                getAccounts accountTypes True

                            else
                                Cmd.none
                    in
                    { return
                        | model = { model | accountTypes = accountTypes }
                        , error = Just Nothing
                        , cmd = cmd
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error getting the data."
                    in
                    { return | error = Just (Just error) }

        ReceivedAccounts getTransactionsNext result ->
            case result of
                Ok accounts ->
                    let
                        cmd =
                            if getTransactionsNext then
                                getTransactions accounts

                            else
                                Cmd.none
                    in
                    { return
                        | model = { model | accounts = accounts, loading = False }
                        , error = Just Nothing
                        , cmd = cmd
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error getting the data."
                    in
                    { return | error = Just (Just error) }

        ReceivedTransactions result ->
            case result of
                Ok transactions ->
                    { return
                        | model =
                            { model
                                | transactions = transactions
                                , loading = False
                            }
                        , error = Just Nothing
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error getting the data."
                    in
                    { return | error = Just (Just error) }

        OverviewMsg subMsg ->
            let
                result =
                    Overview.update subMsg model.overviewPage model.accounts model.accountTypes

                accounts =
                    case result.account of
                        Just account ->
                            model.accounts
                                |> List.filter (\account_ -> account_.account /= account.account)
                                |> List.append [ account ]

                        Nothing ->
                            model.accounts
            in
            { return
                | model = { model | overviewPage = result.model, accounts = accounts }
                , cmd = Cmd.map OverviewMsg result.cmd
                , error = result.error
            }

        TransactionsMsg subMsg ->
            let
                result =
                    Transactions.update subMsg model.transactionsPage model.transactions model.accounts model.accountTypes

                transactions =
                    case result.transactions of
                        Just transactions_ ->
                            model.transactions
                                |> List.filter (\transaction_ -> List.all (\t -> t.id /= transaction_.id) transactions_)
                                |> List.append transactions_

                        Nothing ->
                            model.transactions

                accounts =
                    case result.accounts of
                        Just accounts_ ->
                            model.accounts
                                |> List.filter (\account_ -> List.all (\a -> a.account /= account_.account) accounts_)
                                |> List.append accounts_

                        Nothing ->
                            model.accounts
            in
            { return
                | model = { model | transactionsPage = result.model, transactions = transactions, accounts = accounts }
                , cmd = Cmd.map TransactionsMsg result.cmd
                , error = result.error
            }


getAccountTypes : Bool -> Cmd Msg
getAccountTypes getAccountsNext =
    get
        { url = apis.budget ++ "/accounts/types"
        , expect =
            expectJson
                (ReceivedAccountTypes getAccountsNext)
                (Decode.list accountTypeDecoder)
        }


getAccounts : List AccountType -> Bool -> Cmd Msg
getAccounts accountTypes getTransactionsNext =
    get
        { url = apis.budget ++ "/accounts/"
        , expect =
            expectJson
                (ReceivedAccounts getTransactionsNext)
                (Decode.list (accountDecoder accountTypes))
        }


getTransactions : List Account -> Cmd Msg
getTransactions accounts =
    get
        { url = apis.budget ++ "/transactions/"
        , expect =
            expectJson
                ReceivedTransactions
                (Decode.list (transactionDecoder accounts))
        }


initCmd : Model -> Cmd Msg
initCmd model =
    if model.loading then
        getAccountTypes True

    else
        Cmd.none



-- view


view : Model -> Time.Zone -> Html Msg
view model timeZone =
    let
        content =
            case model.page of
                Overview ->
                    Overview.view model.overviewPage model.accounts model.accountTypes |> Html.map OverviewMsg

                Transactions ->
                    Transactions.view model.transactionsPage model.transactions model.accounts timeZone |> Html.map TransactionsMsg

                Account account ->
                    Account.view
    in
    main_ [ id "budget" ] [ menuView model.page, content ]


menuView : Page -> Html Msg
menuView page =
    let
        overview =
            case page of
                Overview ->
                    a
                        [ href "/budget", class "nav__selected" ]
                        [ text "Overview" ]

                _ ->
                    a [ href "/budget" ] [ text "Overview" ]

        transactions =
            case page of
                Transactions ->
                    a
                        [ href "/budget/transactions", class "nav__selected" ]
                        [ text "Transactions" ]

                _ ->
                    a [ href "/budget/transactions" ] [ text "Transactions" ]
    in
    nav [ id "nav" ]
        [ overview
        , transactions
        , div [] [ text "Accounts" ]
        ]



-- routing


routeParser : Parser (Page -> a) (Page -> a) -> Parser (Page -> a) a
routeParser urlStart =
    urlStart
        </> Parser.oneOf
                [ Parser.map Overview Parser.top
                , Parser.map Transactions (Parser.s "transactions")
                , Parser.map Account (Parser.s "account" </> Parser.string)
                ]


fromPage : Page -> String
fromPage page =
    case page of
        Overview ->
            ""

        Transactions ->
            "/transactions"

        Account account ->
            "/account/" ++ account
