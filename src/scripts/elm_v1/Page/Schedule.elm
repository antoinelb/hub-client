module Page.Schedule exposing (view)

import Html exposing (Html, main_, text)
import Html.Attributes exposing (id)



-- view


view : Html msg
view =
    main_ [ id "schedule" ] [ text "Schedule" ]
