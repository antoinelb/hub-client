module Page.Admin exposing
    ( Model(..)
    , Msg
    , fromPage
    , initCmd
    , initModel
    , routeParser
    , update
    , view
    )

import Html exposing (Html, a, div, h1, main_, section, text)
import Html.Attributes exposing (href, id)
import Page.Admin.Auth as Auth
import Url.Parser exposing (Parser, map, s, top)
import Utils.Icons exposing (svgSprite)



-- model


type Model
    = Index
    | Auth Auth.Model


initModel : Model
initModel =
    Index


pageToText : Model -> String
pageToText page =
    case page of
        Index ->
            "Index"

        Auth _ ->
            "Auth"



-- update


type Msg
    = AuthMsg Auth.Msg


update : Msg -> Model -> { model : Model, cmd : Cmd Msg, error : Maybe (Maybe String) }
update msg model =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing }
    in
    case ( msg, model ) of
        ( AuthMsg subMsg, Auth model_ ) ->
            let
                return_ =
                    Auth.update subMsg model_
            in
            { return | model = Auth return_.model, cmd = Cmd.map AuthMsg return_.cmd, error = return_.error }

        ( _, _ ) ->
            return


initCmd : Model -> Cmd Msg
initCmd model =
    case model of
        Index ->
            Cmd.none

        Auth _ ->
            Auth.initCmd |> Cmd.map AuthMsg



-- view


view : Model -> Html Msg
view model =
    let
        links =
            case model of
                Index ->
                    []

                Auth _ ->
                    [ Index ]

        linkContent =
            links
                |> List.map (\page_ -> ( "/admin" ++ fromPage page_, pageToText page_ ))
                |> List.map
                    (\link -> a [ href (Tuple.first link) ] [ text (Tuple.second link) ])
                |> List.intersperse (svgSprite "chevron-right")

        content =
            case model of
                Index ->
                    indexView

                Auth subModel ->
                    Auth.view subModel |> Html.map AuthMsg

        title =
            case model of
                Index ->
                    "Admin"

                Auth _ ->
                    "Auth"
    in
    main_ [ id "admin" ]
        [ h1 [] [ text title ]
        , div [ id "admin-links" ] linkContent
        , content
        ]


indexView : Html msg
indexView =
    let
        links =
            [ Auth Auth.initModel ]
                |> List.map
                    (\page_ ->
                        a
                            [ "/admin" ++ fromPage page_ |> href ]
                            [ pageToText page_ |> text ]
                    )
    in
    section [ id "admin-table" ] links



-- routing


routeParser : List ( Model, Parser a a )
routeParser =
    [ ( Index, top )
    , ( Auth Auth.initModel, s "auth" )
    ]


fromPage : Model -> String
fromPage page =
    case page of
        Index ->
            ""

        Auth _ ->
            "/auth"
