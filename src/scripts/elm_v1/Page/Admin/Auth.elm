module Page.Admin.Auth exposing (Model, Msg, initCmd, initModel, update, view)

import Config exposing (apis)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , input
        , section
        , text
        )
import Html.Attributes exposing (id, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode
import Json.Encode as Encode
import User exposing (User, userDecoder)
import Utils.Elements exposing (checkbox)
import Utils.Events exposing (onEnter)
import Utils.Http
    exposing
        ( DetailedError
        , expectJson
        , get
        , handleError
        , post
        )
import Utils.Icons exposing (svgSprite)
import Utils.Table as Table



-- model


type alias Model =
    { users : Maybe (List User)
    , tableState :
        Table.Model
            { email : String
            , isAdmin : String
            }
            Msg
    , editing : Maybe ( User, String )
    }


initModel : Model
initModel =
    { users = Nothing
    , tableState =
        Table.initModel
            [ ( "Email", .email, Just ToggleEdit )
            , ( "Admin", .isAdmin, Nothing )
            ]
            .email
            False
            .email
    , editing = Nothing
    }



-- update


type Msg
    = NoOp
    | TableMsg (Table.Msg Msg)
    | ReceivedUsers (Result DetailedError (List User))
    | ToggleEdit String
    | UpdateEmail String
    | UpdateIsAdmin Bool
    | Update
    | ToggleDelete
    | ReceivedUpdatedUser String (Result DetailedError User)


update : Msg -> Model -> { model : Model, cmd : Cmd Msg, error : Maybe (Maybe String) }
update msg model =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing }
    in
    case msg of
        NoOp ->
            return

        TableMsg subMsg ->
            case subMsg of
                Table.ExternalMsg msg_ ->
                    update msg_ model

                _ ->
                    let
                        tableState =
                            Table.update subMsg model.tableState
                    in
                    { return | model = { model | tableState = tableState } }

        ReceivedUsers result ->
            case result of
                Ok users ->
                    { return
                        | model = { model | users = Just users }
                        , error = Just Nothing
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error retrieving the users."
                    in
                    { return | error = Just (Just error) }

        ToggleEdit email ->
            case model.editing of
                Just ( _, key ) ->
                    if email == key then
                        { return | model = { model | editing = Nothing } }

                    else
                        let
                            maybeUser =
                                model.users
                                    |> Maybe.withDefault []
                                    |> List.filter (\user_ -> user_.email == email)
                                    |> List.head
                        in
                        case maybeUser of
                            Just user ->
                                { return | model = { model | editing = Just ( user, email ) } }

                            Nothing ->
                                return

                Nothing ->
                    let
                        maybeUser =
                            model.users
                                |> Maybe.withDefault []
                                |> List.filter (\user_ -> user_.email == email)
                                |> List.head
                    in
                    case maybeUser of
                        Just user ->
                            { return | model = { model | editing = Just ( user, email ) } }

                        Nothing ->
                            return

        UpdateEmail email ->
            case model.editing of
                Just ( user, key ) ->
                    { return
                        | model =
                            { model
                                | editing =
                                    Just ( { user | email = email }, key )
                            }
                    }

                Nothing ->
                    return

        UpdateIsAdmin isAdmin ->
            case model.editing of
                Just ( user, key ) ->
                    { return
                        | model =
                            { model
                                | editing =
                                    Just ( { user | isAdmin = isAdmin }, key )
                            }
                    }

                Nothing ->
                    return

        Update ->
            case model.editing of
                Just ( user, key ) ->
                    { return
                        | model =
                            { model
                                | editing = Nothing
                            }
                        , cmd = updateUser key user
                    }

                Nothing ->
                    return

        ToggleDelete ->
            return

        ReceivedUpdatedUser key result ->
            case result of
                Ok user ->
                    let
                        users =
                            model.users
                                |> Maybe.withDefault []
                                |> List.filter (\user_ -> user_.email /= key)
                                |> List.append [ user ]
                    in
                    { return
                        | model = { model | users = Just users }
                        , error = Just Nothing
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error updating the user."
                    in
                    { return | error = Just (Just error) }


getUsers : Cmd Msg
getUsers =
    get
        { url = apis.auth ++ "/admin/users/"
        , expect = expectJson ReceivedUsers (Decode.list userDecoder)
        }


updateUser : String -> User -> Cmd Msg
updateUser key user =
    let
        body =
            Encode.object
                [ ( "email", Encode.string key )
                , ( "new_email", Encode.string (String.trim user.email) )
                , ( "is_admin", Encode.bool user.isAdmin )
                ]
    in
    post
        { url = apis.auth ++ "/admin/users/update"
        , body = body
        , expect = expectJson (ReceivedUpdatedUser key) userDecoder
        }


initCmd : Cmd Msg
initCmd =
    getUsers



-- view


view : Model -> Html Msg
view model =
    let
        table users =
            let
                users_ =
                    case model.editing of
                        Just ( _, key ) ->
                            List.filter
                                (\user_ -> user_.email /= key)
                                users

                        Nothing ->
                            users

                toUserData user =
                    { email = user.email
                    , isAdmin =
                        if user.isAdmin then
                            "✓"

                        else
                            ""
                    }

                userData =
                    List.map toUserData users_
            in
            Table.view model.tableState userData
                |> Html.map TableMsg

        editView_ =
            case model.editing of
                Just ( user, _ ) ->
                    editView user

                Nothing ->
                    text ""

        buttons =
            let
                buttons_ =
                    case model.editing of
                        Just ( _, key ) ->
                            [ button [ onClick Update ]
                                [ text "Update" ]
                            , button [ onClick (ToggleEdit key) ]
                                [ text "Cancel" ]
                            , button [ onClick ToggleDelete ]
                                [ text "Delete" ]
                            ]

                        Nothing ->
                            []
            in
            div [ id "admin__edit-buttons" ] buttons_
    in
    case model.users of
        Just users ->
            section [ id "admin__users" ]
                [ buttons
                , table users
                , editView_
                ]

        _ ->
            section [ id "admin__users" ]
                [ div [ id "admin__loading" ] [ svgSprite "loading" ] ]


editView : User -> Html Msg
editView user =
    form [ id "admin__edit", onSubmit Update ]
        [ input
            [ id "user__email"
            , type_ "text"
            , value user.email
            , onInput UpdateEmail
            , onEnter Update
            ]
            []
        , checkbox user.isAdmin UpdateIsAdmin
        ]
