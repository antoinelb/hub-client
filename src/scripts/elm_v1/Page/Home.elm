module Page.Home exposing (view)

import Html exposing (Html, a, main_, text)
import Html.Attributes exposing (href, id)
import Utils.Icons exposing (svgSprite)



-- view


view : Html msg
view =
    main_ [ id "home" ]
        [ a [ href "/budget" ]
            [ svgSprite "dollar-sign", text "Budget" ]
        , a [ href "/schedule" ]
            [ svgSprite "calendar", text "Schedule" ]
        ]
