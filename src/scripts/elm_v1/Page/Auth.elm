module Page.Auth exposing (Model, Msg, initCmd, initModel, update, view)

import Config exposing (apis)
import Html exposing (Html, br, button, div, form, h2, input, label, main_, p, text)
import Html.Attributes exposing (autofocus, id, maxlength, type_, value)
import Html.Events exposing (onClick, onFocus, onInput, onSubmit)
import Json.Encode as Encode
import Routing exposing (Route)
import Utils.Events exposing (onEnter, onKeyDown)
import Utils.Http exposing (DetailedError, expectWhatever, handleError, post)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus)



-- model


type State
    = EnteringEmail String
    | RequestingOtp String
    | EnteringOtp String String
    | VerifyingOtp String String


type alias Model =
    { state : State
    , next : Route
    }


initModel : Route -> Model
initModel next =
    { state = EnteringEmail ""
    , next = next
    }



-- msg


type Msg
    = NoOp
    | UpdateEmail String
    | RequestOtp
    | ReceivedOtpRequest (Result DetailedError ())
    | ChangeEmail
    | UpdateOtp { ctrlKey : Bool, key : String }
    | FocusOtp
    | VerifyOtp
    | ReceivedOtpVerification (Result DetailedError ())


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe (Maybe String)
        , signedIn : Bool
        }
update msg model =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing, signedIn = False }
    in
    case msg of
        NoOp ->
            return

        UpdateEmail email ->
            case model.state of
                EnteringEmail _ ->
                    { return | model = { model | state = EnteringEmail email } }

                _ ->
                    return

        RequestOtp ->
            case model.state of
                EnteringEmail email ->
                    { return
                        | model = { model | state = RequestingOtp email }
                        , cmd = requestOtp email
                    }

                _ ->
                    return

        ReceivedOtpRequest result ->
            case model.state of
                RequestingOtp email ->
                    case result of
                        Ok _ ->
                            { return
                                | model = { model | state = EnteringOtp email "      " }
                                , error = Just Nothing
                                , cmd = focus NoOp "auth__otp-0"
                            }

                        Err err ->
                            let
                                error =
                                    handleError err "There was an error sending the email."
                            in
                            { return
                                | model = { model | state = EnteringEmail "" }
                                , error = Just (Just error)
                            }

                _ ->
                    return

        ChangeEmail ->
            let
                email =
                    case model.state of
                        EnteringEmail email_ ->
                            email_

                        RequestingOtp email_ ->
                            email_

                        EnteringOtp email_ _ ->
                            email_

                        VerifyingOtp email_ _ ->
                            email_
            in
            { return
                | model = { model | state = EnteringEmail email }
                , error = Just Nothing
                , cmd = focus NoOp "auth__email"
            }

        UpdateOtp { key } ->
            case model.state of
                EnteringOtp email otp_ ->
                    case key of
                        "Backspace" ->
                            let
                                otp =
                                    removeDigitFromOtp otp_
                            in
                            { return
                                | model = { model | state = EnteringOtp email otp }
                                , cmd = focusOtp otp
                            }

                        key_ ->
                            if String.contains key_ "0123456789" then
                                let
                                    otp =
                                        addDigitToOtp otp_ key
                                in
                                if getCurrentOtpIndex otp == 6 then
                                    { return
                                        | model = { model | state = VerifyingOtp email otp }
                                        , cmd = verifyOtp email otp
                                    }

                                else
                                    { return
                                        | model = { model | state = EnteringOtp email otp }
                                        , cmd = focusOtp otp
                                    }

                            else
                                return

                _ ->
                    return

        FocusOtp ->
            case model.state of
                EnteringOtp _ otp ->
                    { return | cmd = focusOtp otp }

                _ ->
                    return

        VerifyOtp ->
            case model.state of
                EnteringOtp email otp ->
                    { return
                        | model = { model | state = VerifyingOtp email otp }
                        , cmd = verifyOtp email otp
                    }

                _ ->
                    return

        ReceivedOtpVerification result ->
            case model.state of
                VerifyingOtp email _ ->
                    case result of
                        Ok _ ->
                            { return
                                | model = { model | state = EnteringEmail "" }
                                , error = Just Nothing
                                , signedIn = True
                            }

                        Err err ->
                            let
                                error =
                                    handleError err "There was an error sending verifying the otp."
                            in
                            { return
                                | model = { model | state = EnteringOtp email "      " }
                                , error = Just (Just error)
                                , cmd = focus NoOp "auth__otp-0"
                            }

                _ ->
                    return


initCmd : Cmd Msg
initCmd =
    focus NoOp "auth__email"


requestOtp : String -> Cmd Msg
requestOtp email =
    let
        body =
            Encode.object
                [ ( "email", Encode.string email ) ]
    in
    post
        { url = apis.auth ++ "/otp/request"
        , body = body
        , expect = expectWhatever ReceivedOtpRequest
        }


verifyOtp : String -> String -> Cmd Msg
verifyOtp email otp =
    let
        body =
            Encode.object
                [ ( "email", Encode.string email )
                , ( "otp", Encode.string otp )
                ]
    in
    post
        { url = apis.auth ++ "/otp/verify"
        , body = body
        , expect = expectWhatever ReceivedOtpVerification
        }



-- view


view : Model -> Html Msg
view model =
    let
        form_ =
            case model.state of
                EnteringEmail email ->
                    requestOtpView email

                RequestingOtp email ->
                    requestingOtpView email

                EnteringOtp email otp ->
                    verifyOtpView email otp

                VerifyingOtp _ _ ->
                    verifyingOtpView
    in
    main_ [ id "auth" ]
        [ form_ ]


requestOtpView : String -> Html Msg
requestOtpView email =
    form [ id "auth__box", onSubmit RequestOtp ]
        [ h2 [] [ text "Sign in with your email" ]
        , p []
            [ text "We will send you a one time password with which you will be able to sign in."
            ]
        , label []
            [ text "Email"
            , input
                [ id "auth__email"
                , type_ "text"
                , autofocus True
                , value email
                , onInput UpdateEmail
                , onEnter RequestOtp
                ]
                []
            ]
        , input
            [ type_ "submit", onClick RequestOtp, value "Send" ]
            []
        ]


requestingOtpView : String -> Html Msg
requestingOtpView email =
    div [ id "auth__box" ]
        [ h2 [] [ text "Requesting one time password" ]
        , p []
            [ text "We are sending an email to", br [] [], text email ]
        , div [ id "auth__loading" ] [ svgSprite "loading" ]
        , button
            [ onClick ChangeEmail ]
            [ text "Change email" ]
        ]


verifyOtpView : String -> String -> Html Msg
verifyOtpView email otp =
    let
        otpInput =
            \idx digit ->
                let
                    digit_ =
                        if digit == ' ' then
                            ""

                        else
                            String.fromChar digit
                in
                input
                    [ id ("auth__otp-" ++ String.fromInt idx)
                    , type_ "number"
                    , maxlength 1
                    , value digit_
                    , onKeyDown UpdateOtp
                    , onFocus FocusOtp
                    ]
                    []

        otpInputs =
            String.toList otp
                |> List.indexedMap otpInput
    in
    form [ id "auth__box", onSubmit VerifyOtp ]
        [ h2 [] [ text "Verify one time password" ]
        , p []
            [ text "Please enter the one time password you should have received by email at the address"
            , br [] []
            , text email
            ]
        , label []
            [ text "One time password"
            , div [ id "auth__otp", onClick FocusOtp ] otpInputs
            ]
        , button
            [ type_ "button", onClick ChangeEmail ]
            [ text "Change email" ]
        ]


verifyingOtpView : Html Msg
verifyingOtpView =
    div [ id "auth__box" ]
        [ h2 [] [ text "Verifying one time password" ]
        , p []
            [ text "We are verifying the one time password matches" ]
        , div [ id "auth__loading" ] [ svgSprite "loading" ]
        ]



-- utils


getCurrentOtpIndex : String -> Int
getCurrentOtpIndex otp =
    otp
        |> String.toList
        |> List.indexedMap (\i c -> ( i, c ))
        |> List.filter (\c -> Tuple.second c /= ' ')
        |> List.reverse
        |> List.head
        |> Maybe.map (\c -> Tuple.first c + 1)
        |> Maybe.withDefault 0


focusOtp : String -> Cmd Msg
focusOtp otp =
    let
        idx =
            getCurrentOtpIndex otp
    in
    focus NoOp ("auth__otp-" ++ String.fromInt idx)


addDigitToOtp : String -> String -> String
addDigitToOtp otp digit =
    case String.uncons digit of
        Just ( n, _ ) ->
            if Char.isDigit n then
                let
                    idx =
                        getCurrentOtpIndex otp
                in
                otp
                    |> String.toList
                    |> List.indexedMap
                        (\i c ->
                            if i == idx then
                                n

                            else
                                c
                        )
                    |> String.fromList

            else
                otp

        Nothing ->
            otp


removeDigitFromOtp : String -> String
removeDigitFromOtp otp =
    let
        idx =
            getCurrentOtpIndex otp
    in
    otp
        |> String.toList
        |> List.indexedMap
            (\i c ->
                if i == idx - 1 then
                    ' '

                else
                    c
            )
        |> String.fromList
