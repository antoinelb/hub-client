module Page.Loading exposing (view)

import Html exposing (Html, h3, main_, text)
import Html.Attributes exposing (id)
import Utils.Icons exposing (svgSprite)



-- view


view : String -> Html msg
view text_ =
    main_ [ id "loading" ]
        [ svgSprite "loading"
        , h3 [] [ text text_ ]
        ]
