module Page.Budget.Transactions exposing
    ( Model
    , Msg
    , Transaction
    , initModel
    , transactionDecoder
    , update
    , view
    )

import Config exposing (apis)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , h2
        , h3
        , input
        , label
        , option
        , section
        , select
        , span
        , text
        )
import Html.Attributes exposing (class, for, hidden, id, selected, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import Page.Budget.Account exposing (Account, AccountType, accountDecoder)
import Time
import Utils.Events exposing (onStopPropagationKeyDown)
import Utils.Http exposing (DetailedError, expectJson, handleError, post)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus, formatDate, formatNumber, toTitle)
import Utils.Table as Table



-- model


type alias Transaction =
    { id : Int
    , account : Account
    , item : String
    , category : String
    , amount : Float
    , date : Time.Posix
    , group : Int
    , createdOn : Time.Posix
    , lastModified : Time.Posix
    }


type alias Model =
    { addingTransactions : Maybe (List Transaction)
    , addingTransfer : Maybe ( Transaction, Transaction )
    , editingTransaction : Maybe Transaction
    , showErrors : Bool
    , tableState :
        Table.Model
            { id : String
            , item : String
            , category : String
            , account : String
            , date : String
            , amount : String
            , group : String
            }
            Msg
    }


initModel : Model
initModel =
    { addingTransactions = Nothing
    , addingTransfer = Nothing
    , editingTransaction = Nothing
    , showErrors = False
    , tableState =
        Table.initModel
            [ ( "Id", .id, Nothing )
            , ( "Item", .item, Nothing )
            , ( "Category", .category, Nothing )
            , ( "Account", .account, Nothing )
            , ( "Date", .date, Nothing )
            , ( "Amount", .amount, Nothing )
            , ( "Group", .group, Nothing )
            ]
            .id
            True
            .id
    }


initialTransaction : List Account -> Maybe Transaction
initialTransaction accounts =
    List.head accounts
        |> Maybe.map
            (\account ->
                { id = 0
                , item = ""
                , category = ""
                , account = account
                , date = Time.millisToPosix 0
                , amount = 0
                , group = 0
                , createdOn = Time.millisToPosix 0
                , lastModified = Time.millisToPosix 0
                }
            )


initialTransfer : List Account -> Maybe ( Transaction, Transaction )
initialTransfer accounts =
    let
        transactionFrom_ =
            initialTransaction (List.take 1 accounts)

        transactionTo_ =
            initialTransaction (List.take 2 accounts |> List.drop 1)
    in
    case ( transactionFrom_, transactionTo_ ) of
        ( Just transactionFrom, Just transactionTo ) ->
            Just ( transactionFrom, transactionTo )

        ( _, _ ) ->
            Nothing



-- update


type Msg
    = NoOp
    | TableMsg (Table.Msg Msg)
    | ToggleAddTransactions
    | AddTransactions
    | UpdateTransaction
    | UpdateAddTransactionsItem Int String
    | UpdateAddTransactionsCategory Int String
    | UpdateAddTransactionsAmount Int String
    | UpdateAddTransactionsAccount String
    | AddNewTransaction
    | CheckAddTransactionAction { key : String, ctrlKey : Bool }
    | ReceivedTransactionsCreated (Result DetailedError { transactions : List Transaction, account : Account })
    | ToggleAddTransfer
    | AddTransfer
    | UpdateAddTransferAccountFrom String
    | UpdateAddTransferAccountTo String
    | UpdateAddTransferAmount String
    | CheckAddTransferAction { key : String, ctrlKey : Bool }
    | ReceivedTransferCreated (Result DetailedError { transactions : List Transaction, accounts : List Account })


update :
    Msg
    -> Model
    -> List Transaction
    -> List Account
    -> List AccountType
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe (Maybe String)
        , transactions : Maybe (List Transaction)
        , accounts : Maybe (List Account)
        }
update msg model transactions accounts accountTypes =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , transactions = Nothing
            , accounts = Nothing
            }
    in
    case msg of
        NoOp ->
            return

        TableMsg subMsg ->
            case subMsg of
                Table.ExternalMsg msg_ ->
                    update msg_ model transactions accounts accountTypes

                _ ->
                    let
                        tableState =
                            Table.update subMsg model.tableState
                    in
                    { return | model = { model | tableState = tableState } }

        ToggleAddTransactions ->
            case model.addingTransactions of
                Just _ ->
                    { return | model = { model | addingTransactions = Nothing } }

                Nothing ->
                    case initialTransaction accounts of
                        Just transaction ->
                            { return
                                | model = { model | addingTransactions = Just [ transaction ] }
                                , cmd = focus NoOp "new__item-0"
                            }

                        Nothing ->
                            return

        AddTransactions ->
            case model.addingTransactions of
                Just transactions_ ->
                    if List.all (\t -> t.item /= "" && t.category /= "" && t.amount /= 0) transactions_ then
                        { return
                            | model = { model | showErrors = False }
                            , cmd = createTransactions transactions_ accounts accountTypes
                        }

                    else
                        case transactions_ |> List.reverse |> List.head of
                            Just t ->
                                if t.item /= "" || t.category /= "" || t.amount /= 0 then
                                    { return | model = { model | showErrors = True } }

                                else
                                    return

                            Nothing ->
                                return

                Nothing ->
                    return

        UpdateTransaction ->
            return

        UpdateAddTransactionsItem idx item ->
            case model.addingTransactions of
                Just transactions_ ->
                    let
                        transactions__ =
                            transactions_
                                |> List.indexedMap
                                    (\i transaction ->
                                        if i == idx then
                                            { transaction | item = item }

                                        else
                                            transaction
                                    )
                    in
                    { return
                        | model =
                            { model | addingTransactions = Just transactions__ }
                    }

                Nothing ->
                    return

        UpdateAddTransactionsCategory idx category ->
            case model.addingTransactions of
                Just transactions_ ->
                    let
                        transactions__ =
                            transactions_
                                |> List.indexedMap
                                    (\i transaction ->
                                        if i == idx then
                                            { transaction | category = category }

                                        else
                                            transaction
                                    )
                    in
                    { return
                        | model =
                            { model | addingTransactions = Just transactions__ }
                    }

                Nothing ->
                    return

        UpdateAddTransactionsAmount idx amount_ ->
            case model.addingTransactions of
                Just transactions_ ->
                    let
                        amount__ =
                            if amount_ == "" then
                                Just 0

                            else
                                String.toFloat amount_
                    in
                    case amount__ of
                        Just amount ->
                            let
                                transactions__ =
                                    transactions_
                                        |> List.indexedMap
                                            (\i transaction ->
                                                if i == idx then
                                                    { transaction | amount = amount }

                                                else
                                                    transaction
                                            )
                            in
                            { return
                                | model =
                                    { model | addingTransactions = Just transactions__ }
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateAddTransactionsAccount account_ ->
            case model.addingTransactions of
                Just transactions_ ->
                    let
                        account__ =
                            accounts
                                |> List.filter (\a -> a.account == account_)
                                |> List.head
                    in
                    case account__ of
                        Just account ->
                            let
                                transactions__ =
                                    transactions_
                                        |> List.map
                                            (\transaction ->
                                                { transaction | account = account }
                                            )
                            in
                            { return
                                | model =
                                    { model | addingTransactions = Just transactions__ }
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        AddNewTransaction ->
            case model.addingTransactions of
                Just transactions_ ->
                    case List.head transactions_ of
                        Just transaction ->
                            { return
                                | model =
                                    { model
                                        | addingTransactions =
                                            Just
                                                (List.append transactions_
                                                    [ { transaction
                                                        | item = ""
                                                        , category = ""
                                                        , amount = 0
                                                      }
                                                    ]
                                                )
                                    }
                                , cmd = focus NoOp ("new__item-" ++ String.fromInt (List.length transactions_))
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        CheckAddTransactionAction { key, ctrlKey } ->
            case model.addingTransactions of
                Just transactions_ ->
                    case key of
                        "Backspace" ->
                            case transactions_ |> List.reverse |> List.head of
                                Just transaction ->
                                    if transaction.item == "" then
                                        { return
                                            | model =
                                                { model
                                                    | addingTransactions =
                                                        Just
                                                            (transactions_ |> List.reverse |> List.drop 1 |> List.reverse)
                                                }
                                            , cmd = focus NoOp ("new__amount-" ++ String.fromInt (List.length transactions_ - 2))
                                        }

                                    else
                                        return

                                Nothing ->
                                    return

                        "Enter" ->
                            if ctrlKey then
                                if
                                    List.all
                                        (\t ->
                                            t.item
                                                /= ""
                                                && t.category
                                                /= ""
                                                && t.amount
                                                /= 0
                                        )
                                        transactions_
                                then
                                    return

                                else
                                    { return | model = { model | showErrors = True } }

                            else
                                case List.head transactions_ of
                                    Just transaction ->
                                        { return
                                            | model =
                                                { model
                                                    | addingTransactions =
                                                        Just
                                                            (List.append transactions_
                                                                [ { transaction
                                                                    | item = ""
                                                                    , category = ""
                                                                    , amount = 0
                                                                  }
                                                                ]
                                                            )
                                                }
                                            , cmd = focus NoOp ("new__item-" ++ String.fromInt (List.length transactions_))
                                        }

                                    Nothing ->
                                        return

                        _ ->
                            return

                Nothing ->
                    return

        ReceivedTransactionsCreated result ->
            case result of
                Ok result_ ->
                    { return
                        | model = { model | addingTransactions = Nothing }
                        , error = Just Nothing
                        , transactions = Just result_.transactions
                        , accounts = Just [ result_.account ]
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error creating the transactions."
                    in
                    { return | error = Just (Just error) }

        ToggleAddTransfer ->
            case model.addingTransfer of
                Just _ ->
                    { return | model = { model | addingTransfer = Nothing } }

                Nothing ->
                    case initialTransfer accounts of
                        Just ( transactionFrom, transactionTo ) ->
                            { return
                                | model = { model | addingTransfer = Just ( transactionFrom, transactionTo ) }
                                , cmd = focus NoOp "new__amount"
                            }

                        Nothing ->
                            return

        AddTransfer ->
            case model.addingTransfer of
                Just ( transactionFrom, transactionTo ) ->
                    if transactionFrom.amount /= 0 && transactionTo.amount /= 0 then
                        { return
                            | model = { model | showErrors = False }
                            , cmd = createTransfer ( transactionFrom, transactionTo ) accounts accountTypes
                        }

                    else
                        { return | model = { model | showErrors = True } }

                Nothing ->
                    return

        UpdateAddTransferAccountFrom account ->
            case model.addingTransfer of
                Just ( transactionFrom, transactionTo ) ->
                    let
                        account_ =
                            accounts
                                |> List.filter (\a -> a.account == account)
                                |> List.head
                    in
                    case account_ of
                        Just a ->
                            { return | model = { model | addingTransfer = Just ( { transactionFrom | account = a }, transactionTo ) } }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateAddTransferAccountTo account ->
            case model.addingTransfer of
                Just ( transactionFrom, transactionTo ) ->
                    let
                        account_ =
                            accounts
                                |> List.filter (\a -> a.account == account)
                                |> List.head
                    in
                    case account_ of
                        Just a ->
                            { return | model = { model | addingTransfer = Just ( transactionFrom, { transactionTo | account = a } ) } }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateAddTransferAmount amount_ ->
            case model.addingTransfer of
                Just ( transactionFrom, transactionTo ) ->
                    let
                        a =
                            String.toFloat amount_
                    in
                    case a of
                        Just amount ->
                            { return | model = { model | addingTransfer = Just ( { transactionFrom | amount = -amount }, { transactionTo | amount = amount } ) } }

                        Nothing ->
                            return

                Nothing ->
                    return

        CheckAddTransferAction { key, ctrlKey } ->
            case model.addingTransfer of
                Just ( transactionFrom, transactionTo ) ->
                    case key of
                        "Enter" ->
                            if ctrlKey then
                                if transactionFrom.amount /= 0 && transactionTo.amount /= 0 then
                                    return

                                else
                                    { return | model = { model | showErrors = True } }

                            else
                                return

                        _ ->
                            return

                Nothing ->
                    return

        ReceivedTransferCreated result ->
            case result of
                Ok result_ ->
                    { return
                        | model = { model | addingTransfer = Nothing }
                        , error = Just Nothing
                        , transactions = Just result_.transactions
                        , accounts = Just result_.accounts
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error creating the transfer."
                    in
                    { return | error = Just (Just error) }


createTransactions : List Transaction -> List Account -> List AccountType -> Cmd Msg
createTransactions transactions accounts accountTypes =
    case List.head transactions of
        Just transaction ->
            let
                body =
                    Encode.object
                        [ ( "account", Encode.string transaction.account.account )
                        , ( "items"
                          , Encode.list Encode.string (List.map .item transactions)
                          )
                        , ( "categories"
                          , Encode.list Encode.string (List.map .category transactions)
                          )
                        , ( "amounts"
                          , Encode.list Encode.float (List.map .amount transactions)
                          )
                        ]

                decoder =
                    Decode.map2
                        (\transactions_ account ->
                            { transactions = transactions_, account = account }
                        )
                        (Decode.field "transactions"
                            (Decode.list (transactionDecoder accounts))
                        )
                        (Decode.field "account" (accountDecoder accountTypes))
            in
            post
                { url = apis.budget ++ "/transactions/add"
                , body = body
                , expect = expectJson ReceivedTransactionsCreated decoder
                }

        Nothing ->
            Cmd.none


createTransfer : ( Transaction, Transaction ) -> List Account -> List AccountType -> Cmd Msg
createTransfer ( transactionFrom, transactionTo ) accounts accountTypes =
    let
        body =
            Encode.object
                [ ( "account_from", Encode.string transactionFrom.account.account )
                , ( "account_to", Encode.string transactionTo.account.account )
                , ( "amount"
                  , Encode.float transactionFrom.amount
                  )
                ]

        decoder =
            Decode.map2
                (\transactions_ accounts_ ->
                    { transactions = transactions_, accounts = accounts_ }
                )
                (Decode.field "transactions"
                    (Decode.list (transactionDecoder accounts))
                )
                (Decode.field "accounts" (Decode.list (accountDecoder accountTypes)))
    in
    post
        { url = apis.budget ++ "/transactions/transfer"
        , body = body
        , expect = expectJson ReceivedTransferCreated decoder
        }



-- view


view : Model -> List Transaction -> List Account -> Time.Zone -> Html Msg
view model transactions accounts timeZone =
    section [ id "budget__transactions" ]
        [ transactionsView model transactions timeZone
        , addTransactionView model.addingTransactions model.addingTransfer accounts model.showErrors
        ]


transactionsView : Model -> List Transaction -> Time.Zone -> Html Msg
transactionsView model transactions timeZone =
    let
        table transactions_ =
            let
                toTransactionData transaction =
                    { id = transaction.id |> String.fromInt
                    , item = transaction.item
                    , category = transaction.category
                    , account = transaction.account.account
                    , date = transaction.date |> formatDate timeZone
                    , amount = transaction.amount |> String.fromFloat
                    , group = transaction.group |> String.fromInt
                    }

                transactionData =
                    List.map toTransactionData transactions_
            in
            Table.view model.tableState transactionData
                |> Html.map TableMsg

        editView_ =
            case model.editingTransaction of
                Just transaction ->
                    editView transaction

                Nothing ->
                    text ""
    in
    div [ id "budget__transactions__transactions" ]
        [ h2 [] [ text "Transactions" ], table transactions, editView_ ]


addTransactionView :
    Maybe (List Transaction)
    -> Maybe ( Transaction, Transaction )
    -> List Account
    -> Bool
    -> Html Msg
addTransactionView maybeTransactions maybeTransfer accounts showErrors =
    let
        hidden_ =
            case ( maybeTransactions, maybeTransfer ) of
                ( Nothing, Nothing ) ->
                    True

                ( _, _ ) ->
                    False

        transactionsDiv transactions_ =
            let
                toInputs i_ transaction =
                    let
                        i =
                            String.fromInt i_
                    in
                    div [ class "new__transaction" ]
                        [ input
                            [ id ("new__item-" ++ i)
                            , class
                                (if showErrors && transaction.item == "" then
                                    "new__error"

                                 else
                                    ""
                                )
                            , type_ "text"
                            , onInput (UpdateAddTransactionsItem i_)
                            , onStopPropagationKeyDown CheckAddTransactionAction
                            ]
                            []
                        , input
                            [ type_ "text"
                            , class
                                (if showErrors && transaction.category == "" then
                                    "new__error"

                                 else
                                    ""
                                )
                            , onInput (UpdateAddTransactionsCategory i_)
                            , onStopPropagationKeyDown CheckAddTransactionAction
                            ]
                            []
                        , input
                            [ id ("new__amount-" ++ i)
                            , type_ "number"
                            , class
                                (if showErrors && transaction.amount == 0 then
                                    "new__error"

                                 else
                                    ""
                                )
                            , onInput (UpdateAddTransactionsAmount i_)
                            , onStopPropagationKeyDown CheckAddTransactionAction
                            ]
                            []
                        ]
            in
            div [ id "new__transactions" ]
                [ div [ id "new__transactions__header" ]
                    [ span [] [ text "Item" ]
                    , span [] [ text "Category" ]
                    , span [] [ text "Amount" ]
                    ]
                , div [ id "new__transactions__container" ]
                    (List.indexedMap toInputs transactions_)
                , button [ type_ "button", onClick AddNewTransaction ] [ svgSprite "plus" ]
                ]

        total =
            case maybeTransactions of
                Just transactions ->
                    transactions
                        |> List.map .amount
                        |> List.sum

                Nothing ->
                    0

        form_ =
            case ( maybeTransactions, maybeTransfer ) of
                ( Nothing, Just ( transactionFrom, transactionTo ) ) ->
                    form [ id "new__transfer", onSubmit AddTransfer ]
                        [ h2 [] [ text "New transfer" ]
                        , label [ for "new__account-from" ]
                            [ text "From" ]
                        , select
                            [ id "new__account-from"
                            , onInput UpdateAddTransferAccountFrom
                            ]
                            (List.map
                                (\account ->
                                    option
                                        [ value account.account
                                        , selected (account.account == transactionFrom.account.account)
                                        ]
                                        [ account.account
                                            |> String.replace "_" " "
                                            |> toTitle
                                            |> text
                                        ]
                                )
                                accounts
                            )
                        , label [ for "new__account-to" ] [ text "To" ]
                        , select [ id "new__account-to", onInput UpdateAddTransferAccountTo ]
                            (List.map
                                (\account ->
                                    option
                                        [ value account.account
                                        , selected (account.account == transactionTo.account.account)
                                        ]
                                        [ account.account
                                            |> String.replace "_" " "
                                            |> toTitle
                                            |> text
                                        ]
                                )
                                accounts
                            )
                        , label [ for "new__amount" ] [ text "Amount" ]
                        , input
                            [ id "new__amount"
                            , type_ "number"
                            , class
                                (if showErrors && transactionFrom.amount == 0 then
                                    "new__error"

                                 else
                                    ""
                                )
                            , onInput UpdateAddTransferAmount
                            , onStopPropagationKeyDown CheckAddTransferAction
                            ]
                            []
                        , input [ type_ "submit", value "Transfer" ] []
                        , input [ type_ "button", value "Cancel", onClick ToggleAddTransfer ] []
                        ]

                ( Just transactions, Nothing ) ->
                    form [ onSubmit AddTransactions ]
                        [ h2 [] [ text "New transactions" ]
                        , div [ id "new__account__container" ]
                            [ label [ for "new__account" ] [ text "Account" ]
                            , select [ id "new__account", onInput UpdateAddTransactionsAccount ]
                                (List.map
                                    (\account ->
                                        option [ value account.account ]
                                            [ account.account
                                                |> String.replace "_" " "
                                                |> toTitle
                                                |> text
                                            ]
                                    )
                                    accounts
                                )
                            ]
                        , transactionsDiv transactions
                        , div [ id "new__total" ]
                            [ h3 [] [ text "Total" ]
                            , span [] [ total |> formatNumber 2 |> text ]
                            ]
                        , input [ type_ "submit", value "Create" ] []
                        , input [ type_ "button", value "Cancel", onClick ToggleAddTransactions ] []
                        ]

                ( _, _ ) ->
                    form [ hidden True ] []
    in
    div [ id "budget__transactions__add-transactions" ]
        [ form_
        , button [ hidden (not hidden_), onClick ToggleAddTransactions ]
            [ svgSprite "plus", text "Add transactions" ]
        , button [ hidden (not hidden_), onClick ToggleAddTransfer ]
            [ svgSprite "chevrons-right", text "Add transfer" ]
        ]


editView : Transaction -> Html Msg
editView transaction =
    form [ id "budget__transactions__edit-transaction", onSubmit UpdateTransaction ]
        []



-- decoders


transactionDecoder : List Account -> Decoder Transaction
transactionDecoder accounts =
    let
        timeDecoder =
            Decode.int
                |> Decode.andThen
                    (\time ->
                        Time.millisToPosix (time * 1000)
                            |> Decode.succeed
                    )

        initialDecoder =
            Decode.succeed
                (\id user account item category amount date group createdOn lastModified ->
                    { id = id
                    , user = user
                    , account = account
                    , item = item
                    , category = category
                    , amount = amount
                    , date = date
                    , group = group
                    , createdOn = createdOn
                    , lastModified = lastModified
                    }
                )
                |> required "id" Decode.int
                |> required "user" Decode.string
                |> required "account" Decode.string
                |> required "item" Decode.string
                |> required "category" Decode.string
                |> required "amount" Decode.float
                |> required "date" timeDecoder
                |> required "group" Decode.int
                |> required "created_on" timeDecoder
                |> required "last_modified" timeDecoder

        getAccount initialDecoded =
            accounts
                |> List.filter
                    (\account ->
                        account.user
                            == initialDecoded.user
                            && account.account
                            == initialDecoded.account
                    )
                |> List.head
                |> (\account ->
                        case account of
                            Just a ->
                                Decode.succeed
                                    { id = initialDecoded.id
                                    , account = a
                                    , item = initialDecoded.item
                                    , category = initialDecoded.category
                                    , amount = initialDecoded.amount
                                    , date = initialDecoded.date
                                    , group = initialDecoded.group
                                    , createdOn = initialDecoded.createdOn
                                    , lastModified = initialDecoded.lastModified
                                    }

                            Nothing ->
                                Decode.fail
                                    ("There is no account "
                                        ++ initialDecoded.account
                                        ++ "."
                                    )
                   )
    in
    Decode.andThen getAccount initialDecoder
