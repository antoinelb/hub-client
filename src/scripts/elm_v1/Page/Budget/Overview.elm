module Page.Budget.Overview exposing
    ( Model
    , Msg
    , initModel
    , update
    , view
    )

import Config exposing (apis)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , h2
        , input
        , label
        , option
        , section
        , select
        , span
        , text
        )
import Html.Attributes exposing (for, hidden, id, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Encode as Encode
import Page.Budget.Account exposing (Account, AccountType, accountDecoder)
import Time
import Utils.Events exposing (onEnter)
import Utils.Http exposing (DetailedError, expectJson, handleError, post)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus, toTitle)
import Utils.Table as Table



-- model


type alias Model =
    { addingAccount : Maybe Account
    , editingAccount : Maybe Account
    , tableState :
        Table.Model
            { account : String
            , type_ : String
            , amount : String
            }
            Msg
    }


initModel : Model
initModel =
    { addingAccount = Nothing
    , editingAccount = Nothing
    , tableState =
        Table.initModel
            [ ( "Account", .account, Nothing )
            , ( "Type", .type_, Nothing )
            , ( "Amount", .amount, Just ToggleEditAccount )
            ]
            .account
            False
            .account
    }


initialAccount : List AccountType -> Maybe Account
initialAccount accountTypes =
    List.head accountTypes
        |> Maybe.map
            (\accountType ->
                { user = ""
                , account = ""
                , accountType = accountType
                , amount = 0
                , dailyAdjustmentItem = "adjustment"
                , dailyAdjustmentCategory = "adjustment"
                , createdOn = Time.millisToPosix 0
                , lastModified = Time.millisToPosix 0
                }
            )



-- update


type Msg
    = NoOp
    | TableMsg (Table.Msg Msg)
    | ToggleAddAccount
    | CreateAccount
    | UpdateNewAccountName String
    | UpdateNewAccountType String
    | UpdateNewAccountAmount String
    | UpdateNewAccountDailyAdjustmentItem String
    | UpdateNewAccountDailyAdjustmentCategory String
    | ReceivedAccountCreated (Result DetailedError Account)
    | ToggleEditAccount String
    | UpdateAccountAmount String
    | UpdateAccount
    | ReceivedAccountUpdated (Result DetailedError Account)


update :
    Msg
    -> Model
    -> List Account
    -> List AccountType
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe (Maybe String)
        , account : Maybe Account
        }
update msg model accounts accountTypes =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing, account = Nothing }
    in
    case msg of
        NoOp ->
            return

        TableMsg subMsg ->
            case subMsg of
                Table.ExternalMsg msg_ ->
                    update msg_ model accounts accountTypes

                _ ->
                    let
                        tableState =
                            Table.update subMsg model.tableState
                    in
                    { return | model = { model | tableState = tableState } }

        ToggleAddAccount ->
            case model.addingAccount of
                Just _ ->
                    { return | model = { model | addingAccount = Nothing } }

                Nothing ->
                    case initialAccount accountTypes of
                        Just account ->
                            { return | model = { model | addingAccount = Just account }, cmd = focus NoOp "new__account" }

                        Nothing ->
                            { return | error = Just (Just "The website host needs to add account types first.") }

        CreateAccount ->
            case model.addingAccount of
                Just account ->
                    { return | cmd = createAccount account accountTypes }

                Nothing ->
                    return

        UpdateNewAccountName name ->
            case model.addingAccount of
                Just account ->
                    { return
                        | model =
                            { model
                                | addingAccount =
                                    Just { account | account = name }
                            }
                    }

                Nothing ->
                    return

        UpdateNewAccountType type_ ->
            case model.addingAccount of
                Just account ->
                    let
                        accountType =
                            accountTypes
                                |> List.filter (\accountType_ -> accountType_.type_ == type_)
                                |> List.head
                    in
                    case accountType of
                        Just accountType_ ->
                            { return
                                | model =
                                    { model
                                        | addingAccount =
                                            Just { account | accountType = accountType_ }
                                    }
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateNewAccountAmount amount ->
            case model.addingAccount of
                Just account ->
                    case String.toFloat amount of
                        Just amount_ ->
                            { return
                                | model =
                                    { model
                                        | addingAccount =
                                            Just { account | amount = amount_ }
                                    }
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateNewAccountDailyAdjustmentItem item ->
            case model.addingAccount of
                Just account ->
                    { return
                        | model =
                            { model
                                | addingAccount =
                                    Just { account | dailyAdjustmentItem = item }
                            }
                    }

                Nothing ->
                    return

        UpdateNewAccountDailyAdjustmentCategory category ->
            case model.addingAccount of
                Just account ->
                    { return
                        | model =
                            { model
                                | addingAccount =
                                    Just { account | dailyAdjustmentCategory = category }
                            }
                    }

                Nothing ->
                    return

        ReceivedAccountCreated result ->
            case result of
                Ok account ->
                    { return
                        | model = { model | addingAccount = Nothing }
                        , error = Just Nothing
                        , account = Just account
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error creating the account."
                    in
                    { return | error = Just (Just error) }

        ToggleEditAccount account_ ->
            case model.editingAccount of
                Just account ->
                    if account.account == account_ then
                        { return | model = { model | editingAccount = Nothing } }

                    else
                        let
                            maybeAccount =
                                accounts
                                    |> List.filter (\a -> a.account == account_)
                                    |> List.head
                        in
                        case maybeAccount of
                            Just a ->
                                { return
                                    | model = { model | editingAccount = Just a }
                                    , cmd = focus NoOp "edit__amount"
                                }

                            Nothing ->
                                return

                Nothing ->
                    let
                        maybeAccount =
                            accounts
                                |> List.filter (\a -> a.account == account_)
                                |> List.head
                    in
                    case maybeAccount of
                        Just a ->
                            { return
                                | model = { model | editingAccount = Just a }
                                , cmd = focus NoOp "edit__amount"
                            }

                        Nothing ->
                            return

        UpdateAccountAmount amount ->
            case model.editingAccount of
                Just account ->
                    case String.toFloat amount of
                        Just amount_ ->
                            { return
                                | model =
                                    { model
                                        | editingAccount =
                                            Just { account | amount = amount_ }
                                    }
                            }

                        Nothing ->
                            return

                Nothing ->
                    return

        UpdateAccount ->
            case model.editingAccount of
                Just account ->
                    { return | cmd = updateAccount account accountTypes }

                Nothing ->
                    return

        ReceivedAccountUpdated result ->
            case result of
                Ok account ->
                    { return
                        | model = { model | editingAccount = Nothing }
                        , error = Just Nothing
                        , account = Just account
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error updating the account."
                    in
                    { return | error = Just (Just error) }


createAccount : Account -> List AccountType -> Cmd Msg
createAccount account accountTypes =
    let
        body =
            if account.accountType.adjustedDaily then
                Encode.object
                    [ ( "account", Encode.string account.account )
                    , ( "account_type", Encode.string account.accountType.type_ )
                    , ( "amount", Encode.float account.amount )
                    , ( "daily_adjustment_item"
                      , Encode.string account.dailyAdjustmentItem
                      )
                    , ( "daily_adjustment_category"
                      , Encode.string account.dailyAdjustmentCategory
                      )
                    ]

            else
                Encode.object
                    [ ( "account", Encode.string account.account )
                    , ( "account_type", Encode.string account.accountType.type_ )
                    , ( "amount", Encode.float account.amount )
                    ]
    in
    post
        { url = apis.budget ++ "/accounts/add"
        , body = body
        , expect = expectJson ReceivedAccountCreated (accountDecoder accountTypes)
        }


updateAccount : Account -> List AccountType -> Cmd Msg
updateAccount account accountTypes =
    let
        body =
            Encode.object
                [ ( "account", Encode.string account.account )
                , ( "amount", Encode.float account.amount )
                ]
    in
    post
        { url = apis.budget ++ "/accounts/update"
        , body = body
        , expect = expectJson ReceivedAccountUpdated (accountDecoder accountTypes)
        }



-- view


view : Model -> List Account -> List AccountType -> Html Msg
view model accounts accountTypes =
    section [ id "budget__overview" ]
        [ overviewView model accounts
        , addAccountView model.addingAccount accountTypes
        ]


overviewView : Model -> List Account -> Html Msg
overviewView model accounts =
    let
        table accounts_ =
            let
                accounts__ =
                    case model.editingAccount of
                        Just account ->
                            List.filter
                                (\a -> a.account /= account.account)
                                accounts_

                        Nothing ->
                            accounts_

                toAccountData account =
                    { account = account.account
                    , type_ = account.accountType.type_
                    , amount = String.fromFloat account.amount
                    }

                accountData =
                    List.map toAccountData accounts__
            in
            Table.view model.tableState accountData
                |> Html.map TableMsg

        editView_ =
            case model.editingAccount of
                Just account ->
                    editView account

                Nothing ->
                    text ""
    in
    div [ id "budget__overview__overview" ]
        [ h2 [] [ text "Overview" ], table accounts, editView_ ]


addAccountView : Maybe Account -> List AccountType -> Html Msg
addAccountView maybeAccount accountTypes =
    let
        hidden_ =
            case maybeAccount of
                Just _ ->
                    False

                Nothing ->
                    True

        notAdjustedDaily =
            case maybeAccount of
                Just account ->
                    not account.accountType.adjustedDaily

                Nothing ->
                    True
    in
    div [ id "budget__overview__add-account" ]
        [ form [ hidden hidden_, onSubmit CreateAccount ]
            [ h2 [] [ text "New account" ]
            , label [ for "new__account" ] [ text "Account name" ]
            , input
                [ id "new__account"
                , type_ "text"
                , Maybe.map .account maybeAccount
                    |> Maybe.withDefault ""
                    |> value
                , onInput UpdateNewAccountName
                ]
                []
            , label [ for "new__account-type" ] [ text "Account type" ]
            , select [ id "new__account-type", onInput UpdateNewAccountType ]
                (List.map
                    (\accountType ->
                        option [ value accountType.type_ ]
                            [ accountType.type_
                                |> String.replace "_" " "
                                |> toTitle
                                |> text
                            ]
                    )
                    accountTypes
                )
            , label [ for "new__amount" ] [ text "Initial amount" ]
            , input
                [ id "new__amount"
                , type_ "text"
                , onInput UpdateNewAccountAmount
                ]
                []
            , label [ for "new__adjusted-daily-item", hidden notAdjustedDaily ]
                [ text "Name of adjustment transaction item" ]
            , input
                [ id "new__adjusted-daily-item"
                , type_ "text"
                , Maybe.map .dailyAdjustmentItem maybeAccount
                    |> Maybe.withDefault ""
                    |> value
                , onInput UpdateNewAccountDailyAdjustmentItem
                , hidden notAdjustedDaily
                ]
                []
            , label [ for "new__adjusted-daily-category", hidden notAdjustedDaily ]
                [ text "Name of adjustment transaction category" ]
            , input
                [ id "new__adjusted-daily-category"
                , type_ "text"
                , Maybe.map .dailyAdjustmentCategory maybeAccount
                    |> Maybe.withDefault ""
                    |> value
                , onInput UpdateNewAccountDailyAdjustmentCategory
                , hidden notAdjustedDaily
                ]
                []
            , input [ type_ "submit", value "Create" ] []
            , input [ type_ "button", value "Cancel", onClick ToggleAddAccount ] []
            ]
        , button [ hidden (not hidden_), onClick ToggleAddAccount ]
            [ svgSprite "plus", text "Add account" ]
        ]


editView : Account -> Html Msg
editView account =
    form [ id "budget__overview__edit-account", onSubmit UpdateAccount ]
        [ span [] [ text account.account ]
        , span []
            [ text account.accountType.type_ ]
        , input
            [ id "edit__amount"
            , type_ "number"
            , onInput UpdateAccountAmount
            , onEnter UpdateAccount
            ]
            []
        ]
