module Page.Budget.Account exposing
    ( Account
    , AccountType
    , accountDecoder
    , accountTypeDecoder
    , view
    )

import Html exposing (Html, section)
import Html.Attributes exposing (id)
import Json.Decode as Decode exposing (Decoder)
import Time



-- model


type alias AccountType =
    { type_ : String
    , isCredit : Bool
    , adjustedDaily : Bool
    }


type alias Account =
    { user : String
    , account : String
    , accountType : AccountType
    , amount : Float
    , dailyAdjustmentItem : String
    , dailyAdjustmentCategory : String
    , createdOn : Time.Posix
    , lastModified : Time.Posix
    }



-- view


view : Html msg
view =
    section [ id "budget__account" ] []



-- decoders


accountTypeDecoder : Decoder AccountType
accountTypeDecoder =
    Decode.map3 AccountType
        (Decode.field "type" Decode.string)
        (Decode.field "is_credit" Decode.bool)
        (Decode.field "adjusted_daily" Decode.bool)


accountDecoder : List AccountType -> Decoder Account
accountDecoder accountTypes =
    let
        getAccountType type_ =
            accountTypes
                |> List.filter (\accountType -> accountType.type_ == type_)
                |> List.head
                |> (\accountType ->
                        case accountType of
                            Just accountType_ ->
                                Decode.succeed accountType_

                            Nothing ->
                                Decode.fail ("There is no account type " ++ type_ ++ ".")
                   )

        accountTypeDecoder_ =
            Decode.string
                |> Decode.andThen getAccountType

        timeDecoder =
            Decode.int
                |> Decode.andThen (\time -> Time.millisToPosix (time * 1000) |> Decode.succeed)
    in
    Decode.map8 Account
        (Decode.field "user" Decode.string)
        (Decode.field "account" Decode.string)
        (Decode.field "account_type" accountTypeDecoder_)
        (Decode.field "amount" Decode.float)
        (Decode.field "daily_adjustment_item" Decode.string)
        (Decode.field "daily_adjustment_category" Decode.string)
        (Decode.field "created_on" timeDecoder)
        (Decode.field "last_modified" timeDecoder)
