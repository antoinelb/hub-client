module Main exposing (main)

import Auth
import Browser
import Browser.Navigation as Nav
import Html exposing (Html, a, div, h1, header, text)
import Html.Attributes exposing (class, hidden, href, id)
import Page
import Routing
import Settings
import Settings.Theme as Theme
import Task
import Time
import Url exposing (Url)
import User exposing (User)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (toCmd)



-- main


type alias Flags =
    { theme : String
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- model


type alias Model =
    { key : Nav.Key
    , error : Maybe String
    , lastMsg : Maybe Msg
    , timeZone : Time.Zone
    , user : Maybe User
    , auth : Auth.Model
    , page : Page.Model
    , settings : Settings.Model
    }


initModel : Flags -> Url -> Nav.Key -> Model
initModel { theme } url key =
    let
        route =
            Routing.toRoute url
    in
    { key = key
    , error = Nothing
    , lastMsg = Nothing
    , timeZone = Time.utc
    , user = Nothing
    , auth = Auth.initModel
    , page = Page.initModel route
    , settings = Settings.initModel theme
    }



-- update


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | UpdateTimeZone Time.Zone
    | AuthMsg Auth.Msg
    | PageMsg Page.Msg
    | SettingsMsg (Settings.Msg Msg)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( model_, cmd_ ) =
            case msg of
                LinkClicked request ->
                    case request of
                        Browser.Internal url ->
                            ( model, Nav.pushUrl model.key (Url.toString url) )

                        Browser.External href ->
                            ( model, Nav.load href )

                UrlChanged url ->
                    ( model
                    , Routing.toRoute url
                        |> Page.updatePage model.page
                        |> Cmd.map PageMsg
                    )

                UpdateTimeZone zone ->
                    ( { model | timeZone = zone }, Cmd.none )

                AuthMsg subMsg ->
                    let
                        result =
                            Auth.update subMsg model.auth model.user

                        error =
                            case result.error of
                                Just err ->
                                    err

                                Nothing ->
                                    model.error

                        routeCmd =
                            if result.updateRoute then
                                case model.page of
                                    Page.Loading _ route ->
                                        case result.user of
                                            Just _ ->
                                                route
                                                    |> Routing.fromRoute
                                                    |> Nav.pushUrl model.key

                                            Nothing ->
                                                case route of
                                                    Routing.Auth _ ->
                                                        route
                                                            |> Routing.fromRoute
                                                            |> Nav.pushUrl model.key

                                                    _ ->
                                                        route
                                                            |> Routing.fromRoute
                                                            |> Just
                                                            |> Routing.Auth
                                                            |> Routing.fromRoute
                                                            |> Nav.pushUrl model.key

                                    _ ->
                                        Cmd.none

                            else
                                Cmd.none

                        resubmitCmd =
                            case model.lastMsg of
                                Just msg_ ->
                                    if result.reSubmit then
                                        toCmd msg_

                                    else
                                        Cmd.none

                                Nothing ->
                                    Cmd.none
                    in
                    ( { model
                        | auth = result.model
                        , user = result.user
                        , error = error
                      }
                    , Cmd.batch [ Cmd.map AuthMsg result.cmd, routeCmd, resubmitCmd ]
                    )

                PageMsg subMsg ->
                    let
                        result_ =
                            Page.update subMsg model.page model.key

                        error =
                            case result_.error of
                                Just err ->
                                    err

                                Nothing ->
                                    model.error

                        cmd =
                            if result_.updateUser then
                                Cmd.batch
                                    [ Cmd.map PageMsg result_.cmd
                                    , Auth.getUserInfo |> Cmd.map AuthMsg
                                    ]

                            else
                                Cmd.map PageMsg result_.cmd
                    in
                    ( { model | page = result_.model, error = error }
                    , cmd
                    )

                SettingsMsg subMsg ->
                    case subMsg of
                        Settings.ExternalMsg msg_ ->
                            update msg_ model

                        _ ->
                            let
                                result_ =
                                    Settings.update subMsg model.settings

                                error =
                                    case result_.error of
                                        Just err ->
                                            err

                                        Nothing ->
                                            model.error
                            in
                            ( { model | settings = result_.model, error = error }
                            , Cmd.map SettingsMsg result_.cmd
                            )
    in
    case model_.error of
        Just error ->
            if error == "The token expired." then
                case model.user of
                    Just user ->
                        ( { model_ | error = Nothing }
                        , Cmd.batch
                            [ cmd_
                            , Auth.getAccessToken False |> Cmd.map AuthMsg
                            ]
                        )

                    Nothing ->
                        ( model_, cmd_ )

            else
                ( model_, cmd_ )

        Nothing ->
            ( { model_ | lastMsg = Just msg }, cmd_ )


initCmd : Model -> Cmd Msg
initCmd model =
    Cmd.batch
        [ Task.perform UpdateTimeZone Time.here
        , Auth.initCmd |> Cmd.map AuthMsg
        , Page.initCmd model.page |> Cmd.map PageMsg
        ]



-- view


view : Model -> Browser.Document Msg
view model =
    let
        class_ =
            case model.settings.theme of
                Theme.Dark ->
                    ""

                Theme.Light ->
                    "light"

        ( title, body_ ) =
            Page.view model.page model.timeZone

        body =
            Html.map PageMsg body_
    in
    { title = title
    , body =
        [ div [ id "root", class class_ ]
            [ errorView model.error
            , headerView model.user model.page
            , Settings.view model.settings |> Html.map SettingsMsg
            , body
            ]
        ]
    }


errorView : Maybe String -> Html msg
errorView error =
    let
        ( error_, hidden_ ) =
            case error of
                Just err ->
                    ( err, False )

                Nothing ->
                    ( "", True )
    in
    div [ id "error", hidden hidden_ ] [ text error_ ]


headerView : Maybe User -> Page.Model -> Html msg
headerView maybeUser page =
    let
        adminButton =
            case maybeUser of
                Just user ->
                    if user.isAdmin then
                        case page of
                            Page.Admin _ ->
                                a [ id "header__admin", href "/", class "header__button" ]
                                    [ svgSprite "home", text "Home" ]

                            _ ->
                                a [ id "header__admin", href "/admin", class "header__button" ]
                                    [ svgSprite "shield", text "Admin" ]

                    else
                        text ""

                Nothing ->
                    text ""
    in
    header [ id "header" ]
        [ a [ href "/" ] [ h1 [] [ text "Hub" ] ]
        , adminButton
        ]



-- subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- init


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        model =
            initModel flags url key
    in
    ( model, initCmd model )
