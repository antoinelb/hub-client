port module Page exposing
    ( Model(..)
    , Msg
    , initCmd
    , initModel
    , update
    , updatePage
    , view
    )

import Browser.Navigation as Nav
import Html exposing (Html)
import Page.Admin as Admin
import Page.Auth as Auth
import Page.Budget as Budget
import Page.Home as Home
import Page.Loading as Loading
import Page.Schedule as Schedule
import Routing exposing (Route)
import Time
import Url
import Utils.Misc exposing (toCmd)



-- model


type Model
    = Home
    | Loading String Route
    | Auth Auth.Model
    | Admin Admin.Model
    | Budget Budget.Model
    | Schedule


initModel : Route -> Model
initModel route =
    Loading "" route


routeToPage : Model -> Route -> Model
routeToPage previousModel route =
    case route of
        Routing.Home ->
            Home

        Routing.Auth next ->
            let
                next_ =
                    next
                        |> Maybe.withDefault "/"
                        |> Url.fromString
                        |> Maybe.map Routing.toRoute
                        |> Maybe.withDefault Routing.Home
            in
            Auth (Auth.initModel next_)

        Routing.Admin admin ->
            Admin admin

        Routing.Budget page ->
            case previousModel of
                Budget budget ->
                    Budget (Budget.updatedModel budget page)

                _ ->
                    Budget (Budget.initModel page)

        Routing.Schedule ->
            Schedule



-- update


type Msg
    = UpdatePage Model
    | AuthMsg Auth.Msg
    | AdminMsg Admin.Msg
    | BudgetMsg Budget.Msg


update :
    Msg
    -> Model
    -> Nav.Key
    -> { model : Model, cmd : Cmd Msg, error : Maybe (Maybe String), updateUser : Bool }
update msg model key =
    let
        result =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateUser = False
            }
    in
    case ( model, msg ) of
        ( _, UpdatePage page ) ->
            let
                cmd =
                    case page of
                        Home ->
                            Cmd.none

                        Loading _ _ ->
                            Cmd.none

                        Auth _ ->
                            Auth.initCmd |> Cmd.map AuthMsg

                        Admin subModel ->
                            Admin.initCmd subModel |> Cmd.map AdminMsg

                        Budget subModel ->
                            Budget.initCmd subModel |> Cmd.map BudgetMsg

                        Schedule ->
                            Cmd.none

                loadingCmd =
                    case page of
                        Loading _ _ ->
                            updateLoading True

                        _ ->
                            updateLoading False
            in
            { result | model = page, cmd = Cmd.batch [ cmd, loadingCmd ] }

        ( Auth subModel, AuthMsg subMsg ) ->
            let
                result_ =
                    Auth.update subMsg subModel

                cmd =
                    if result_.signedIn then
                        Cmd.batch
                            [ Cmd.map AuthMsg result_.cmd
                            , subModel.next
                                |> Routing.fromRoute
                                |> Nav.pushUrl key
                            ]

                    else
                        Cmd.map AuthMsg result_.cmd
            in
            { result
                | model = Auth result_.model
                , cmd = cmd
                , error = result_.error
                , updateUser = result_.signedIn
            }

        ( Admin subModel, AdminMsg subMsg ) ->
            let
                result_ =
                    Admin.update subMsg
                        subModel
            in
            { result
                | model = Admin result_.model
                , cmd = Cmd.map AdminMsg result_.cmd
                , error = result_.error
            }

        ( Budget subModel, BudgetMsg subMsg ) ->
            let
                result_ =
                    Budget.update subMsg subModel
            in
            { result
                | model = Budget result_.model
                , cmd = Cmd.map BudgetMsg result_.cmd
                , error = result_.error
            }

        ( _, _ ) ->
            result


updatePage : Model -> Route -> Cmd Msg
updatePage previousModel route =
    routeToPage previousModel route |> UpdatePage |> toCmd


initCmd : Model -> Cmd Msg
initCmd model =
    case model of
        Home ->
            Cmd.none

        Loading _ _ ->
            updateLoading True

        Auth _ ->
            Auth.initCmd |> Cmd.map AuthMsg

        Admin subModel ->
            Admin.initCmd subModel |> Cmd.map AdminMsg

        Budget subModel ->
            Budget.initCmd subModel |> Cmd.map BudgetMsg

        Schedule ->
            Cmd.none



-- view


view : Model -> Time.Zone -> ( String, Html Msg )
view model timeZone =
    case model of
        Home ->
            ( "Hub", Home.view )

        Loading text _ ->
            ( "Hub", Loading.view text )

        Auth subModel ->
            ( "Auth | Hub", Auth.view subModel |> Html.map AuthMsg )

        Admin subModel ->
            ( "Admin | Hub", Admin.view subModel |> Html.map AdminMsg )

        Budget subModel ->
            ( "Budget | Hub", Budget.view subModel timeZone |> Html.map BudgetMsg )

        Schedule ->
            ( "Schedule | Hub", Schedule.view )



-- ports


port updateLoading : Bool -> Cmd msg
