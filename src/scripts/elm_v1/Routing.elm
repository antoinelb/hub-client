module Routing exposing (Route(..), fromRoute, toRoute)

import Page.Admin as Admin
import Page.Budget as Budget
import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, top)
import Url.Parser.Query as Query


type Route
    = Home
    | Auth (Maybe String)
    | Admin Admin.Model
    | Budget Budget.Page
    | Schedule


routeParser : Parser (Route -> a) a
routeParser =
    List.concat
        [ [ map Home top
          , map Auth (s "auth" <?> Query.string "next")
          , map Schedule (s "schedule")
          ]
        , Admin.routeParser
            |> List.map
                (\route ->
                    map
                        (Admin (Tuple.first route))
                        (s "admin" </> Tuple.second route)
                )
        , [ map Budget (Budget.routeParser (s "budget")) ]
        ]
        |> oneOf


toRoute : Url -> Route
toRoute url =
    Maybe.withDefault Home (parse routeParser url)


fromRoute : Route -> String
fromRoute route =
    case route of
        Home ->
            "/"

        Auth maybeNext ->
            case maybeNext of
                Just next ->
                    if next == "/" then
                        "/auth"

                    else
                        "/auth?next=" ++ next

                Nothing ->
                    "/auth"

        Admin adminPage ->
            "/admin" ++ Admin.fromPage adminPage

        Budget budgetPage ->
            "/budget" ++ Budget.fromPage budgetPage

        Schedule ->
            "/schedule"
