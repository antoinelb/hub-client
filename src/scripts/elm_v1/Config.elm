module Config exposing (Apis, apis)


type alias Apis =
    { auth : String
    , budget : String
    }


apis : Apis
apis =
    { auth = "{{ auth_api }}"
    , budget = "{{ budget_api }}"
    }
