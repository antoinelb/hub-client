module User exposing (User, userDecoder)

import Json.Decode as Decode exposing (Decoder)



-- model


type alias User =
    { email : String
    , isAdmin : Bool
    , pin : Maybe String
    }


userDecoder : Decoder User
userDecoder =
    Decode.map3 User
        (Decode.field "email" Decode.string)
        (Decode.field "is_admin" Decode.bool)
        (Decode.field "pin" (Decode.nullable Decode.string))
