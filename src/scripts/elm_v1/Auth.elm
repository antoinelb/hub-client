module Auth exposing
    ( Model(..)
    , Msg
    , getAccessToken
    , getUserInfo
    , initCmd
    , initModel
    , update
    )

import Config exposing (apis)
import User exposing (User, userDecoder)
import Utils.Http
    exposing
        ( DetailedError
        , expectJson
        , expectWhatever
        , get
        , handleError
        )



-- model


type Model
    = Anonymous
    | SignedIn


initModel : Model
initModel =
    Anonymous



-- update


type Msg
    = ReceivedAccessToken Bool (Result DetailedError ())
    | ReceivedUserInfo (Result DetailedError User)


update :
    Msg
    -> Model
    -> Maybe User
    ->
        { model : Model
        , cmd : Cmd Msg
        , user : Maybe User
        , error : Maybe (Maybe String)
        , updateRoute : Bool
        , reSubmit : Bool
        }
update msg model user =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , user = user
            , error = Nothing
            , updateRoute = False
            , reSubmit = False
            }
    in
    case msg of
        ReceivedAccessToken alsoGetInfo result ->
            case result of
                Ok _ ->
                    let
                        cmd =
                            if alsoGetInfo then
                                getUserInfo

                            else
                                Cmd.none
                    in
                    { return
                        | model = SignedIn
                        , error = Just Nothing
                        , cmd = cmd
                        , reSubmit = True
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just (Just error_)
                    in
                    { return | error = error, updateRoute = True }

        ReceivedUserInfo result_ ->
            case result_ of
                Ok user_ ->
                    { return
                        | model = SignedIn
                        , user = Just user_
                        , error = Just Nothing
                        , updateRoute = True
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just (Just error_)
                    in
                    { return | error = error, updateRoute = True }


getAccessToken : Bool -> Cmd Msg
getAccessToken alsoGetInfo =
    get
        { url = apis.auth ++ "/users/refresh"
        , expect = expectWhatever (ReceivedAccessToken alsoGetInfo)
        }


getUserInfo : Cmd Msg
getUserInfo =
    get
        { url = apis.auth ++ "/users/info"
        , expect = expectJson ReceivedUserInfo userDecoder
        }


initCmd : Cmd Msg
initCmd =
    getAccessToken True
