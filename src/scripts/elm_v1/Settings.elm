module Settings exposing (Model, Msg(..), initModel, update, view)

import Browser.Dom as Dom
import Html exposing (Html, aside, button, div)
import Html.Attributes exposing (attribute, class, id)
import Settings.Theme as Theme
import Task
import Utils.Events exposing (onEscape, stopPropagationOnClick)
import Utils.Icons exposing (svgSprite)
import Utils.Touch as Touch



-- model


type alias Model =
    { menuOpen : Bool
    , touchPositionX : Maybe Float
    , theme : Theme.Model
    }


initModel : String -> Model
initModel theme =
    { menuOpen = False
    , touchPositionX = Nothing
    , theme = Theme.initModel theme
    }



-- update


type Msg msg
    = NoOp
    | ToggleMenu
    | StartMenuSwipe Touch.Touch
    | EndMenuSwipe Touch.Touch
    | ExternalMsg msg
    | ThemeMsg Theme.Msg


update : Msg msg -> Model -> { model : Model, cmd : Cmd (Msg msg), error : Maybe (Maybe String) }
update msg model =
    let
        return =
            { model = model, cmd = Cmd.none, error = Nothing }
    in
    case msg of
        NoOp ->
            return

        ToggleMenu ->
            { return
                | model = { model | menuOpen = not model.menuOpen }
                , cmd =
                    Task.attempt (\_ -> NoOp) (Dom.blur "menu-button")
            }

        StartMenuSwipe touchEvent ->
            { return | model = { model | touchPositionX = Just touchEvent.clientX } }

        EndMenuSwipe touchEvent ->
            case model.touchPositionX of
                Nothing ->
                    return

                Just positionX ->
                    if model.menuOpen then
                        if positionX < touchEvent.clientX then
                            { return
                                | model =
                                    { model
                                        | menuOpen = False
                                        , touchPositionX = Nothing
                                    }
                            }

                        else
                            { return | model = { model | touchPositionX = Nothing } }

                    else if positionX > touchEvent.clientX then
                        { return
                            | model =
                                { model
                                    | menuOpen = True
                                    , touchPositionX = Nothing
                                }
                        }

                    else
                        { return
                            | model =
                                { model | touchPositionX = Nothing }
                        }

        ExternalMsg _ ->
            return

        ThemeMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Theme.update subMsg model.theme
            in
            { return
                | model = { model | theme = subModel, menuOpen = False }
                , cmd = Cmd.map ThemeMsg cmd
            }



-- view


view : Model -> Html (Msg msg)
view model =
    let
        attributes =
            if model.menuOpen then
                [ id "settings"
                , class "settings--open"
                , stopPropagationOnClick ToggleMenu
                , onEscape ToggleMenu
                ]

            else
                [ id "settings" ]
    in
    div attributes
        [ menuView model
        ]


menuView : Model -> Html (Msg msg)
menuView model =
    div [ id "menu" ]
        [ menuIconView
        , touchAreaView
        , asideView model
        ]


menuIconView : Html (Msg msg)
menuIconView =
    button
        [ id "menu-button"
        , attribute "aria-label" "Settings"
        , stopPropagationOnClick ToggleMenu
        ]
        [ svgSprite "menu" ]


touchAreaView : Html (Msg msg)
touchAreaView =
    div
        [ id "menu__swipe-area"
        , Touch.onTouchEvent Touch.TouchStart StartMenuSwipe
        , Touch.onTouchEvent Touch.TouchEnd EndMenuSwipe
        ]
        []


asideView : Model -> Html (Msg msg)
asideView model =
    let
        asideClass =
            if model.menuOpen then
                "menu--showing"

            else
                ""
    in
    aside
        [ class asideClass
        , stopPropagationOnClick NoOp
        , Touch.onTouchEvent Touch.TouchStart StartMenuSwipe
        , Touch.onTouchEvent Touch.TouchEnd EndMenuSwipe
        ]
        [ div [ id "menu__bg" ] []
        , Theme.themeButton model.theme
            |> Html.map ThemeMsg
        ]
