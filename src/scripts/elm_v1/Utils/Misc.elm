module Utils.Misc exposing
    ( focus
    , formatDate
    , formatNumber
    , loseFocus
    , toCmd
    , toTitle
    )

import Browser.Dom as Dom
import Task
import Time


toCmd : msg -> Cmd msg
toCmd msg =
    Task.succeed msg |> Task.perform identity


loseFocus : msg -> String -> Cmd msg
loseFocus msg id =
    Task.attempt (\_ -> msg) (Dom.blur id)


focus : msg -> String -> Cmd msg
focus msg id =
    Task.attempt (\_ -> msg) (Dom.focus id)


toTitle : String -> String
toTitle text =
    let
        titleWord word =
            (String.left 1 word |> String.toUpper) ++ String.dropLeft 1 word
    in
    text
        |> String.split " "
        |> List.map titleWord
        |> String.join " "


formatDate : Time.Zone -> Time.Posix -> String
formatDate timeZone date =
    let
        year =
            date |> Time.toYear timeZone |> String.fromInt

        month =
            date |> Time.toMonth timeZone |> monthToInt |> String.fromInt |> String.padLeft 2 '0'

        day =
            date |> Time.toDay timeZone |> String.fromInt |> String.padLeft 2 '0'
    in
    year ++ "-" ++ month ++ "-" ++ day


monthToInt : Time.Month -> Int
monthToInt month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12


formatNumber : Int -> Float -> String
formatNumber nDecimals number_ =
    let
        n =
            String.fromFloat number_

        split =
            String.split "." n

        integer =
            List.head split

        decimal =
            List.tail split
                |> Maybe.andThen List.head
    in
    case ( integer, decimal ) of
        ( Nothing, _ ) ->
            ""

        ( Just i, Nothing ) ->
            i ++ ".00"

        ( Just i, Just d ) ->
            i ++ "." ++ String.padRight nDecimals '0' d
