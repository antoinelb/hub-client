module Dashboard.Projects exposing
    ( Model
    , Msg
    , Project
    , hotkeys
    , initCmd
    , initModel
    , projectDecoder
    , update
    , view
    )

import Config exposing (apis)
import Hotkeys exposing (Hotkey)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , h2
        , h3
        , input
        , label
        , section
        , span
        , text
        )
import Html.Attributes as Attr
    exposing
        ( attribute
        , class
        , for
        , hidden
        , id
        , type_
        , value
        )
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Random
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , expectJson
        , expectWhatever
        , handleError
        , post
        )
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus)



-- model


type alias Project =
    { id : Int
    , name : String
    , colour : Int
    }


type EditMode
    = Adding
    | Updating


type alias Model =
    { nextColour : Int
    , editingProject :
        Maybe
            { mode : EditMode
            , project : Project
            , showErrors : Bool
            }
    , deletingProject : Bool
    }


initModel : Model
initModel =
    { nextColour = 285
    , editingProject = Nothing
    , deletingProject = False
    }


initProject : Int -> Project
initProject nextColour =
    { id = 0
    , name = ""
    , colour = nextColour
    }



-- update


type Msg
    = NoOp
    | UpdateNextColour Int
    | ToggleEditProject (Maybe Int)
    | UpdateProjectName String
    | UpdateProjectColour String
    | UpdateProject
    | ReceivedProject (Result DetailedError Project)
    | ToggleDeleteProject
    | DeleteProject
    | ReceivedDeleteProjectConfirmation Project (Result DetailedError ())


update :
    Msg
    -> Model
    -> Tokens
    -> Maybe (List Project)
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , project : Maybe ( Project, Bool )
        }
update msg model tokens projects =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , project = Nothing
            }
    in
    case msg of
        NoOp ->
            return

        UpdateNextColour colour ->
            { return | model = { model | nextColour = colour } }

        ToggleEditProject maybeId ->
            case model.editingProject of
                Just _ ->
                    { return | model = { model | editingProject = Nothing } }

                Nothing ->
                    case maybeId of
                        Just id ->
                            let
                                project_ =
                                    projects
                                        |> Maybe.withDefault []
                                        |> List.filter (\p -> p.id == id)
                                        |> List.head
                            in
                            case project_ of
                                Just project ->
                                    { return
                                        | model =
                                            { model
                                                | editingProject =
                                                    Just
                                                        { mode = Updating
                                                        , project = project
                                                        , showErrors = False
                                                        }
                                            }
                                        , cmd = focus NoOp "edit-project__name"
                                    }

                                Nothing ->
                                    return

                        Nothing ->
                            { return
                                | model =
                                    { model
                                        | editingProject =
                                            Just
                                                { mode = Adding
                                                , project = initProject model.nextColour
                                                , showErrors = False
                                                }
                                    }
                                , cmd = focus NoOp "edit-project__name"
                            }

        UpdateProjectName name ->
            case model.editingProject of
                Just { mode, project, showErrors } ->
                    { return
                        | model =
                            { model
                                | editingProject =
                                    Just
                                        { mode = mode
                                        , project = { project | name = name }
                                        , showErrors = showErrors
                                        }
                            }
                    }

                Nothing ->
                    return

        UpdateProjectColour colour_ ->
            case model.editingProject of
                Just { mode, project, showErrors } ->
                    case String.toInt colour_ of
                        Just colour ->
                            { return
                                | model =
                                    { model
                                        | editingProject =
                                            Just
                                                { mode = mode
                                                , project = { project | colour = colour }
                                                , showErrors = showErrors
                                                }
                                    }
                            }

                        Nothing ->
                            { return
                                | model =
                                    { model
                                        | editingProject =
                                            Just
                                                { mode = mode
                                                , project = { project | colour = 0 }
                                                , showErrors = showErrors
                                                }
                                    }
                            }

                Nothing ->
                    return

        UpdateProject ->
            case model.editingProject of
                Just { mode, project } ->
                    case mode of
                        Adding ->
                            if validateEditProject projects project then
                                { return
                                    | cmd = addProject tokens project
                                    , updateLastMsg = True
                                }

                            else
                                { return
                                    | model =
                                        { model
                                            | editingProject =
                                                Just
                                                    { mode = mode
                                                    , project = project
                                                    , showErrors = True
                                                    }
                                        }
                                }

                        Updating ->
                            return

                Nothing ->
                    return

        ReceivedProject result ->
            case result of
                Ok project ->
                    { return
                        | model =
                            { model
                                | editingProject = Nothing
                                , deletingProject = False
                            }
                        , cmd = Random.generate UpdateNextColour (Random.int 0 360)
                        , project = Just ( project, True )
                    }

                Err err ->
                    let
                        error =
                            case model.editingProject of
                                Just { mode } ->
                                    case mode of
                                        Adding ->
                                            handleError err "There was an error creating the project."

                                        Updating ->
                                            handleError err "There was an error updating the project."

                                Nothing ->
                                    handleError err "There was an error creating the project."
                    in
                    { return
                        | error = Just error
                    }

        ToggleDeleteProject ->
            case model.editingProject |> Maybe.map .mode |> Maybe.withDefault Adding of
                Adding ->
                    { return | model = { model | deletingProject = False } }

                Updating ->
                    { return
                        | model =
                            { model
                                | deletingProject = not model.deletingProject
                            }
                    }

        DeleteProject ->
            if model.deletingProject then
                case model.editingProject of
                    Just { mode, project } ->
                        case mode of
                            Adding ->
                                return

                            Updating ->
                                { return | cmd = deleteProject tokens project }

                    Nothing ->
                        return

            else
                return

        ReceivedDeleteProjectConfirmation project result ->
            case result of
                Ok _ ->
                    { return
                        | model =
                            { model
                                | editingProject = Nothing
                                , deletingProject = False
                            }
                        , project = Just ( project, False )
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error deleting the project."
                    in
                    { return
                        | error = Just error
                    }


initCmd : Cmd Msg
initCmd =
    Random.generate UpdateNextColour (Random.int 0 360)


addProject : Tokens -> Project -> Cmd Msg
addProject tokens project =
    let
        body =
            Encode.object
                [ ( "name", Encode.string project.name )
                , ( "colour", Encode.int project.colour )
                ]
    in
    post tokens
        { url = apis.schedule ++ "/projects/add"
        , body = body
        , expect = expectJson ReceivedProject projectDecoder
        }


deleteProject : Tokens -> Project -> Cmd Msg
deleteProject tokens project =
    let
        body =
            Encode.object
                [ ( "id", Encode.int project.id )
                ]
    in
    post tokens
        { url = apis.schedule ++ "/projects/delete"
        , body = body
        , expect = expectWhatever (ReceivedDeleteProjectConfirmation project)
        }



-- view


view : (String -> String) -> Model -> Maybe (List Project) -> Html Msg
view translate model maybeProjects =
    let
        projectListView_ =
            case maybeProjects of
                Just projects ->
                    projectListView translate projects

                Nothing ->
                    div [ id "projects__list", class "loading" ] [ svgSprite "loading" ]

        buttonsView_ =
            case maybeProjects of
                Just _ ->
                    buttonsView translate model

                Nothing ->
                    text ""
    in
    section [ id "projects" ]
        [ div [ id "projects__container" ]
            [ h2 [] [ "Projects" |> translate |> text ]
            , projectListView_
            ]
        , buttonsView_
        , editProjectView translate model maybeProjects
        ]


projectListView : (String -> String) -> List Project -> Html Msg
projectListView translate projects =
    if List.isEmpty projects then
        div [ id "projects__list" ] [ "You don't have any projects yet." |> translate |> text ]

    else
        let
            toItem project =
                div [ class "projects__project" ]
                    [ span
                        [ class "projects__project__name"
                        , attribute "style"
                            ("--project-colour: "
                                ++ String.fromInt project.colour
                            )
                        , onClick (ToggleEditProject (Just project.id))
                        ]
                        [ project.name |> text ]
                    , span [] [ "0" |> text ]
                    , span [] [ "0" |> text ]
                    , span []
                        [ "0" |> text ]
                    ]
        in
        div [ id "projects__list" ]
            (List.append
                [ div [ id "projects__header" ]
                    [ span [] []
                    , span [] [ "Today" |> translate |> text ]
                    , span [] [ "This week" |> translate |> text ]
                    , span [] [ "This month" |> translate |> text ]
                    ]
                ]
                (List.map toItem projects)
            )


buttonsView : (String -> String) -> Model -> Html Msg
buttonsView _ model =
    let
        hidden_ =
            case model.editingProject of
                Just _ ->
                    True

                Nothing ->
                    False
    in
    div [ class "section-buttons", hidden hidden_ ]
        [ div [ class "section-buttons__center" ]
            [ button [ onClick (ToggleEditProject Nothing) ] [ svgSprite "plus" ] ]
        , div [ class "section-buttons__right" ]
            [ button [] [ svgSprite "bar-chart-2" ] ]
        ]


editProjectView : (String -> String) -> Model -> Maybe (List Project) -> Html Msg
editProjectView translate model projects_ =
    let
        projects =
            Maybe.withDefault [] projects_

        hidden_ =
            case model.editingProject of
                Just _ ->
                    False

                Nothing ->
                    True

        ( nameValue, nameClass, nameError ) =
            case model.editingProject of
                Just { project, showErrors } ->
                    if showErrors then
                        case validateProjectName projects project of
                            Just error ->
                                ( project.name
                                , "input-error"
                                , error
                                )

                            Nothing ->
                                ( project.name, "", "" )

                    else
                        ( project.name, "", "" )

                Nothing ->
                    ( "", "", "" )

        ( colourValue, colourClass, colourError ) =
            case model.editingProject of
                Just { project, showErrors } ->
                    let
                        colour =
                            String.fromInt project.colour
                    in
                    if showErrors then
                        case validateProjectColour projects project of
                            Just error ->
                                ( colour
                                , "input-error"
                                , error
                                )

                            Nothing ->
                                ( colour, "", "" )

                    else
                        ( colour, "", "" )

                Nothing ->
                    ( "", "", "" )

        title =
            case Maybe.map .mode model.editingProject |> Maybe.withDefault Adding of
                Adding ->
                    "New project" |> translate

                Updating ->
                    ("Edit project " |> translate) ++ nameValue

        errors =
            let
                errors_ =
                    [ nameError, colourError ]
            in
            if List.all String.isEmpty errors_ then
                div [ class "form-error", hidden True ] []

            else
                div [ class "form-error", hidden False ]
                    (List.map
                        (\e -> span [] [ e |> text ])
                        errors_
                    )

        buttons =
            case Maybe.map .mode model.editingProject |> Maybe.withDefault Adding of
                Adding ->
                    div [ class "buttons buttons--adding" ]
                        [ button
                            [ type_ "button"
                            , onClick (ToggleEditProject Nothing)
                            ]
                            [ "Cancel" |> translate |> text ]
                        , button
                            [ type_ "submit" ]
                            [ "Add" |> translate |> text ]
                        ]

                Updating ->
                    div [ class "buttons buttons--updating" ]
                        [ button
                            [ type_ "button"
                            , onClick (ToggleEditProject Nothing)
                            ]
                            [ "Cancel" |> translate |> text ]
                        , button
                            [ type_ "submit" ]
                            [ "Add" |> translate |> text ]
                        , button
                            [ type_ "button"
                            , onClick ToggleDeleteProject
                            ]
                            [ "Delete" |> translate |> text ]
                        ]
    in
    form
        [ id "edit-project"
        , hidden hidden_
        , onSubmit UpdateProject
        ]
        [ h2 [] [ text title ]
        , errors
        , label
            [ for "edit-project__name" ]
            [ "Name" |> translate |> text ]
        , input
            [ id "edit-project__name"
            , class nameClass
            , type_ "text"
            , value nameValue
            , onInput UpdateProjectName
            ]
            []
        , label
            [ for "edit-project__colour" ]
            [ "Colour" |> translate |> text ]
        , input
            [ id "edit-project__colour"
            , class colourClass
            , type_ "number"
            , Attr.min "0"
            , Attr.max "360"
            , value colourValue
            , onInput UpdateProjectColour
            ]
            []
        , span
            [ attribute "style" ("--project-colour: " ++ colourValue ++ ";") ]
            []
        , buttons
        , div [ id "edit-project__delete", hidden (not model.deletingProject) ]
            [ h3 []
                [ "Are you sure you want to delete project "
                    ++ nameValue
                    ++ "?"
                    |> translate
                    |> text
                ]
            , button [ onClick ToggleDeleteProject ] [ "Cancel" |> translate |> text ]
            , button [ onClick DeleteProject ] [ "Delete" |> translate |> text ]
            ]
        ]



-- decoders


projectDecoder : Decoder Project
projectDecoder =
    Decode.map3 Project
        (Decode.field "id" Decode.int)
        (Decode.field "name" Decode.string)
        (Decode.field "colour" Decode.int)



-- validators


validateProjectName : List Project -> Project -> Maybe String
validateProjectName projects project =
    if String.isEmpty project.name then
        Just "You need to provide a name."

    else if List.any (\p -> p.name == project.name) projects then
        Just "There already is a project with that name."

    else
        Nothing


validateProjectColour : List Project -> Project -> Maybe String
validateProjectColour projects project =
    if project.colour < 0 || project.colour > 360 then
        Just "The colour value must be between 0 and 360."

    else if List.any (\p -> p.colour == project.colour) projects then
        Just "There already is a project with that colour."

    else
        Nothing


validateEditProject : Maybe (List Project) -> Project -> Bool
validateEditProject maybeProjects project =
    let
        projects =
            Maybe.withDefault [] maybeProjects
    in
    case
        ( validateProjectName projects project
        , validateProjectColour projects project
        )
    of
        ( Nothing, Nothing ) ->
            True

        ( _, _ ) ->
            False



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys model =
    let
        closeHotkey =
            if model.deletingProject then
                [ { key = "Escape", ctrl = False, msg = ToggleDeleteProject } ]

            else
                case model.editingProject of
                    Just _ ->
                        [ { key = "Escape"
                          , ctrl = False
                          , msg = ToggleEditProject Nothing
                          }
                        ]

                    Nothing ->
                        []
    in
    List.concat
        [ closeHotkey
        ]
