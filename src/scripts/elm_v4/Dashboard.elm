module Dashboard exposing
    ( Model
    , Msg
    , hotkeys
    , initCmd
    , initModel
    , update
    , view
    )

import Config exposing (apis)
import Dashboard.Projects as Projects exposing (Project, projectDecoder)
import Hotkeys exposing (Hotkey)
import Html exposing (Html, main_)
import Html.Attributes exposing (id)
import Json.Decode as Decode
import Utils.Http exposing (DetailedError, Tokens, expectJson, get, handleError)



-- model


type alias Data =
    { projects : Maybe (List Project) }


type alias Model =
    { data : Data
    , projects : Projects.Model
    }


initModel : Model
initModel =
    { data =
        { projects = Nothing }
    , projects = Projects.initModel
    }



-- update


type Msg
    = NoOp
    | ReceivedProjects (Result DetailedError (List Project))
    | ProjectsMsg Projects.Msg


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case msg of
        NoOp ->
            return

        ReceivedProjects result ->
            case result of
                Ok projects ->
                    let
                        data =
                            model.data
                    in
                    { return
                        | model =
                            { model
                                | data = { data | projects = Just projects }
                            }
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error accessing the server."
                    in
                    { return
                        | error = Just error
                    }

        ProjectsMsg subMsg ->
            let
                result_ =
                    Projects.update subMsg model.projects tokens model.data.projects

                projects =
                    case result_.project of
                        Just ( project, add ) ->
                            if add then
                                model.data.projects
                                    |> Maybe.withDefault []
                                    |> List.filter (\p -> p.id /= project.id)
                                    |> List.append [ project ]
                                    |> Just

                            else
                                model.data.projects
                                    |> Maybe.withDefault []
                                    |> List.filter (\p -> p.id /= project.id)
                                    |> Just

                        Nothing ->
                            model.data.projects

                data =
                    model.data
            in
            { return
                | model =
                    { model
                        | projects = result_.model
                        , data = { data | projects = projects }
                    }
                , cmd = Cmd.map ProjectsMsg result_.cmd
                , error = result_.error
                , updateLastMsg = result_.updateLastMsg
            }


getProjects : Tokens -> Cmd Msg
getProjects tokens =
    get tokens
        { url = apis.schedule ++ "/projects/"
        , expect = expectJson ReceivedProjects (Decode.list projectDecoder)
        }


initCmd : Model -> Tokens -> Cmd Msg
initCmd model tokens =
    let
        projectsCmd =
            case model.data.projects of
                Just _ ->
                    Cmd.none

                Nothing ->
                    getProjects tokens
    in
    Cmd.batch
        [ projectsCmd
        , Projects.initCmd |> Cmd.map ProjectsMsg
        ]



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    main_ [ id "dashboard" ]
        [ Projects.view translate model.projects model.data.projects
            |> Html.map ProjectsMsg
        ]



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys model =
    List.concat
        [ Projects.hotkeys model.projects |> Hotkeys.map ProjectsMsg
        ]
