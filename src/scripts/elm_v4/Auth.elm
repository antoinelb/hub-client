port module Auth exposing
    ( Model(..)
    , Msg
    , User
    , getTokens
    , getUser
    , getUserInfo
    , initCmd
    , initModel
    , initUser
    , refreshAccessToken
    , signOut
    , update
    , userDecoder
    , view
    )

import Config exposing (apis)
import Html exposing (Html, br, button, div, form, h2, input, label, main_, p, text)
import Html.Attributes exposing (autofocus, id, maxlength, type_, value)
import Html.Events exposing (onClick, onFocus, onInput, onSubmit)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Utils.Events exposing (onEnter, onKeyDown)
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , authGet
        , expectJson
        , expectWhatever
        , handleError
        , unAuthenticatedPost
        )
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus)



-- model


type alias User =
    { email : String
    , isAdmin : Bool
    }


type Model
    = EnteringEmail String
    | RequestingOtp String
    | EnteringOtp String String
    | VerifyingOtp String String
    | RequestingAccess String (Maybe User)
    | RequestingInfo Tokens (Maybe User)
    | Authenticated { user : User, tokens : Tokens }


initUser : User
initUser =
    { email = ""
    , isAdmin = False
    }


initModel : Maybe String -> Model
initModel refreshToken =
    case refreshToken of
        Just token ->
            RequestingAccess token Nothing

        Nothing ->
            EnteringEmail ""



-- update


type Msg
    = NoOp
    | UpdateEmail String
    | RequestOtp
    | ReceivedOtpRequest (Result DetailedError ())
    | ChangeEmail
    | UpdateOtp { ctrlKey : Bool, key : String, tag : String }
    | FocusOtp
    | VerifyOtp
    | ReceivedOtpVerification (Result DetailedError Tokens)
    | ReceivedAccessToken (Maybe User) (Result DetailedError Tokens)
    | ReceivedUserInfo Tokens (Maybe User) (Result DetailedError User)
    | ReceivedSignOut (Result DetailedError ())


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , resubmit : Bool
        }
update msg model =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , resubmit = False
            }
    in
    case msg of
        NoOp ->
            return

        UpdateEmail email ->
            case model of
                EnteringEmail _ ->
                    { return | model = EnteringEmail email }

                _ ->
                    return

        RequestOtp ->
            case model of
                EnteringEmail email ->
                    { return
                        | model = RequestingOtp email
                        , cmd = requestOtp email
                    }

                _ ->
                    return

        ReceivedOtpRequest result ->
            case model of
                RequestingOtp email ->
                    case result of
                        Ok _ ->
                            { return
                                | model = EnteringOtp email "      "
                                , cmd = focus NoOp "auth__otp-0"
                            }

                        Err err ->
                            let
                                error =
                                    handleError err "There was an error sending the email."
                            in
                            { return
                                | model = EnteringEmail ""
                                , error = Just error
                            }

                _ ->
                    return

        ChangeEmail ->
            let
                email =
                    case model of
                        EnteringEmail email_ ->
                            email_

                        RequestingOtp email_ ->
                            email_

                        EnteringOtp email_ _ ->
                            email_

                        VerifyingOtp email_ _ ->
                            email_

                        _ ->
                            ""
            in
            { return
                | model = EnteringEmail email
                , cmd = focus NoOp "auth__email"
            }

        UpdateOtp input_ ->
            case model of
                EnteringOtp email otp_ ->
                    case input_.key of
                        "Backspace" ->
                            let
                                otp =
                                    removeDigitFromOtp otp_
                            in
                            { return
                                | model = EnteringOtp email otp
                                , cmd = focusOtp otp
                            }

                        key_ ->
                            if String.contains key_ "0123456789" then
                                let
                                    otp =
                                        addDigitToOtp otp_ input_.key
                                in
                                if getCurrentOtpIndex otp == 6 then
                                    { return
                                        | model = VerifyingOtp email otp
                                        , cmd = verifyOtp email otp
                                    }

                                else
                                    { return
                                        | model = EnteringOtp email otp
                                        , cmd = focusOtp otp
                                    }

                            else
                                return

                _ ->
                    return

        FocusOtp ->
            case model of
                EnteringOtp _ otp ->
                    { return | cmd = focusOtp otp }

                _ ->
                    return

        VerifyOtp ->
            case model of
                EnteringOtp email otp ->
                    { return
                        | model = VerifyingOtp email otp
                        , cmd = verifyOtp email otp
                    }

                _ ->
                    return

        ReceivedOtpVerification result ->
            case model of
                VerifyingOtp email _ ->
                    case result of
                        Ok tokens ->
                            { return
                                | model = RequestingInfo tokens Nothing
                                , cmd = getUserInfo tokens Nothing
                            }

                        Err err ->
                            let
                                error =
                                    handleError err "There was an error sending verifying the otp."
                            in
                            { return
                                | model = EnteringOtp email "      "
                                , error = Just error
                                , cmd = focus NoOp "auth__otp-0"
                            }

                _ ->
                    return

        ReceivedAccessToken user result ->
            case result of
                Ok tokens ->
                    { return
                        | model = RequestingInfo tokens user
                        , cmd = getUserInfo tokens user
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        ( model_, error, cmd ) =
                            if error_ == "You don't have access to this resource." then
                                ( model, Nothing, Cmd.none )

                            else
                                ( EnteringEmail ""
                                , Just error_
                                , focus NoOp "auth__email"
                                )
                    in
                    { return
                        | model = model_
                        , error = error
                        , cmd = cmd
                    }

        ReceivedUserInfo tokens maybeUser result ->
            case result of
                Ok user ->
                    let
                        ( cmd, resubmit ) =
                            case maybeUser of
                                Just _ ->
                                    ( saveRefreshToken (Just tokens.refresh)
                                    , True
                                    )

                                Nothing ->
                                    ( saveRefreshToken (Just tokens.refresh)
                                    , False
                                    )
                    in
                    { return
                        | model = Authenticated { user = user, tokens = tokens }
                        , cmd = cmd
                        , resubmit = resubmit
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                    }

        ReceivedSignOut result ->
            case result of
                Ok _ ->
                    { return
                        | model = EnteringEmail ""
                        , cmd = saveRefreshToken Nothing
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                    }


initCmd : Model -> Cmd Msg
initCmd model =
    case model of
        EnteringEmail _ ->
            focus NoOp "auth__email"

        RequestingAccess token refreshing ->
            refreshAccessToken token refreshing

        _ ->
            Cmd.none


requestOtp : String -> Cmd Msg
requestOtp email =
    let
        body =
            Encode.object
                [ ( "email", Encode.string email ) ]
    in
    unAuthenticatedPost
        { url = apis.auth ++ "/otp/request"
        , body = body
        , expect = expectWhatever ReceivedOtpRequest
        }


verifyOtp : String -> String -> Cmd Msg
verifyOtp email otp =
    let
        tokenDecoder =
            Decode.map2 (\access refresh -> { access = access, refresh = refresh })
                (Decode.field "access-token" Decode.string)
                (Decode.field "refresh-token" Decode.string)

        body =
            Encode.object
                [ ( "email", Encode.string email )
                , ( "otp", Encode.string otp )
                ]
    in
    unAuthenticatedPost
        { url = apis.auth ++ "/otp/verify"
        , body = body
        , expect = expectJson ReceivedOtpVerification tokenDecoder
        }


refreshAccessToken : String -> Maybe User -> Cmd Msg
refreshAccessToken token user =
    let
        headers =
            [ Http.header "refresh-token" token ]

        tokensDecoder =
            Decode.map
                (\access -> { access = access, refresh = token })
                (Decode.field "access-token" Decode.string)
    in
    Http.request
        { method = "GET"
        , url = apis.auth ++ "/users/refresh"
        , body = Http.emptyBody
        , expect = expectJson (ReceivedAccessToken user) tokensDecoder
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


getUserInfo : Tokens -> Maybe User -> Cmd Msg
getUserInfo tokens user =
    authGet tokens
        { url = apis.auth ++ "/users/info"
        , expect = expectJson (ReceivedUserInfo tokens user) userDecoder
        }


signOut : Model -> Cmd Msg
signOut model =
    let
        refreshToken =
            case model of
                Authenticated { tokens } ->
                    tokens.refresh

                RequestingInfo { refresh } _ ->
                    refresh

                RequestingAccess refresh _ ->
                    refresh

                _ ->
                    ""

        headers =
            [ Http.header "refresh-token" refreshToken ]
    in
    if String.isEmpty refreshToken then
        Cmd.none

    else
        Http.request
            { method = "POST"
            , url = apis.auth ++ "/users/signout"
            , body = Encode.object [] |> Http.jsonBody
            , expect = expectWhatever ReceivedSignOut
            , headers = headers
            , timeout = Nothing
            , tracker = Nothing
            }



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        form_ =
            case model of
                EnteringEmail email ->
                    requestOtpView translate email

                RequestingOtp email ->
                    requestingOtpView translate email

                EnteringOtp email otp ->
                    verifyOtpView translate email otp

                VerifyingOtp _ _ ->
                    verifyingOtpView translate

                RequestingAccess _ _ ->
                    loadingView translate

                RequestingInfo _ _ ->
                    loadingView translate

                Authenticated _ ->
                    text ""
    in
    main_ [ id "auth" ]
        [ form_ ]


requestOtpView : (String -> String) -> String -> Html Msg
requestOtpView translate email =
    form [ id "auth__box", onSubmit RequestOtp ]
        [ h2 [] [ "Sign in with your email" |> translate |> text ]
        , p []
            [ "We will send you a one time password with which you will be able to sign in." |> translate |> text
            ]
        , label []
            [ "Email" |> translate |> text
            , input
                [ id "auth__email"
                , type_ "text"
                , autofocus True
                , value email
                , onInput UpdateEmail
                , onEnter RequestOtp
                ]
                []
            ]
        , input
            [ type_ "submit", onClick RequestOtp, "Send" |> translate |> value ]
            []
        ]


requestingOtpView : (String -> String) -> String -> Html Msg
requestingOtpView translate email =
    div [ id "auth__box" ]
        [ h2 [] [ "Requesting one time password" |> translate |> text ]
        , p []
            [ "We are sending an email to" |> translate |> text, br [] [], text email ]
        , div [ id "auth__loading" ] [ svgSprite "loading" ]
        , button
            [ onClick ChangeEmail ]
            [ "Change email" |> translate |> text ]
        ]


verifyOtpView : (String -> String) -> String -> String -> Html Msg
verifyOtpView translate email otp =
    let
        otpInput =
            \idx digit ->
                let
                    digit_ =
                        if digit == ' ' then
                            ""

                        else
                            String.fromChar digit
                in
                input
                    [ id ("auth__otp-" ++ String.fromInt idx)
                    , type_ "number"
                    , maxlength 1
                    , value digit_
                    , onKeyDown UpdateOtp
                    , onFocus FocusOtp
                    ]
                    []

        otpInputs =
            String.toList otp
                |> List.indexedMap otpInput
    in
    form [ id "auth__box", onSubmit VerifyOtp ]
        [ h2 [] [ "Verify one time password" |> translate |> text ]
        , p []
            [ "Please enter the one time password you should have received by email at the address" |> translate |> text
            , br [] []
            , text email
            ]
        , label []
            [ "One time password" |> translate |> text
            , div [ id "auth__otp", onClick FocusOtp ] otpInputs
            ]
        , button
            [ type_ "button", onClick ChangeEmail ]
            [ "Change email" |> translate |> text ]
        ]


verifyingOtpView : (String -> String) -> Html Msg
verifyingOtpView translate =
    div [ id "auth__box" ]
        [ h2 [] [ "Verifying one time password" |> translate |> text ]
        , p []
            [ "We are verifying the one time password matches" |> translate |> text ]
        , div [ id "auth__loading" ] [ svgSprite "loading" ]
        ]


loadingView : (String -> String) -> Html Msg
loadingView translate =
    div [ id "loading" ]
        [ h2 [] [ "Authenticating..." |> translate |> text ]
        , svgSprite "loading"
        ]



-- decoders


userDecoder : Decoder User
userDecoder =
    Decode.map2 User
        (Decode.field "email" Decode.string)
        (Decode.field "is_admin" Decode.bool)



-- utils


getCurrentOtpIndex : String -> Int
getCurrentOtpIndex otp =
    otp
        |> String.toList
        |> List.indexedMap (\i c -> ( i, c ))
        |> List.filter (\c -> Tuple.second c /= ' ')
        |> List.reverse
        |> List.head
        |> Maybe.map (\c -> Tuple.first c + 1)
        |> Maybe.withDefault 0


focusOtp : String -> Cmd Msg
focusOtp otp =
    let
        idx =
            getCurrentOtpIndex otp
    in
    focus NoOp ("auth__otp-" ++ String.fromInt idx)


addDigitToOtp : String -> String -> String
addDigitToOtp otp digit =
    case String.uncons digit of
        Just ( n, _ ) ->
            if Char.isDigit n then
                let
                    idx =
                        getCurrentOtpIndex otp
                in
                otp
                    |> String.toList
                    |> List.indexedMap
                        (\i c ->
                            if i == idx then
                                n

                            else
                                c
                        )
                    |> String.fromList

            else
                otp

        Nothing ->
            otp


removeDigitFromOtp : String -> String
removeDigitFromOtp otp =
    let
        idx =
            getCurrentOtpIndex otp
    in
    otp
        |> String.toList
        |> List.indexedMap
            (\i c ->
                if i == idx - 1 then
                    ' '

                else
                    c
            )
        |> String.fromList


getUser : Model -> Maybe User
getUser model =
    case model of
        RequestingAccess _ maybeUser ->
            maybeUser

        RequestingInfo _ maybeUser ->
            maybeUser

        Authenticated { user } ->
            Just user

        _ ->
            Nothing


getTokens : Model -> Maybe Tokens
getTokens model =
    case model of
        RequestingInfo tokens _ ->
            Just tokens

        Authenticated { tokens } ->
            Just tokens

        _ ->
            Nothing



-- ports


port saveRefreshToken : Maybe String -> Cmd msg
