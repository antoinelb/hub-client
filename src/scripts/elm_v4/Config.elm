module Config exposing (Apis, apis, debug, errorExpiry)


type alias Apis =
    { auth : String
    , budget : String
    , schedule : String
    }


apis : Apis
apis =
    { auth = "{{ auth_api }}"
    , budget = "{{ budget_api }}"
    , schedule = "{{ schedule_api }}"
    }


debug_ : String
debug_ =
    "{{ debug }}"


debug : Bool
debug =
    debug_ == "1"


{-| Number of seconds after which errors dissapear
-}
errorExpiry : Int
errorExpiry =
    10
