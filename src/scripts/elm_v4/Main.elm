module Main exposing (main)

import Auth
import Browser
import Config
import Dashboard
import Hotkeys
import Html exposing (Html, div, h1, header, span, text)
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import I18Next exposing (initialTranslations, translationsDecoder)
import Json.Decode as Decode
import Json.Encode as Encode
import Settings
import Settings.Language as Language
import Settings.Theme as Theme
import Time
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (toCmd)



-- main


type alias Flags =
    { theme : String
    , language : String
    , translations : Encode.Value
    , refreshToken : Maybe String
    }


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- model


type alias Error =
    { text : String
    , expiry : Time.Posix
    }


type alias Model =
    { currentTime : Time.Posix
    , currentZone : Time.Zone
    , errors : List Error
    , lastMsg : Maybe Msg
    , auth : Auth.Model
    , settings : Settings.Model
    , dashboard : Dashboard.Model
    }


initModel : Flags -> Model
initModel { theme, language, translations, refreshToken } =
    let
        translations_ =
            case Decode.decodeValue translationsDecoder translations of
                Ok t ->
                    t

                Err _ ->
                    initialTranslations
    in
    { currentTime = Time.millisToPosix 0
    , currentZone = Time.utc
    , errors = []
    , lastMsg = Nothing
    , auth = Auth.initModel refreshToken
    , settings = Settings.initModel theme language translations_
    , dashboard = Dashboard.initModel
    }



-- update


type Msg
    = NoOp
    | UpdateTime Time.Posix
    | UpdateZone Time.Zone
    | RemoveError Int
    | AuthMsg Auth.Msg
    | SettingsMsg Settings.Msg
    | DashboardMsg Dashboard.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( model_, cmd_, updateLastMsg ) =
            case msg of
                NoOp ->
                    ( model, Cmd.none, False )

                UpdateTime time ->
                    ( { model | currentTime = time }, Cmd.none, False )

                UpdateZone zone ->
                    ( { model | currentZone = zone }, Cmd.none, False )

                RemoveError idx ->
                    let
                        errors =
                            model.errors
                                |> List.indexedMap Tuple.pair
                                |> List.filter (\error -> Tuple.first error /= idx)
                                |> List.map Tuple.second
                    in
                    ( { model | errors = errors }, Cmd.none, False )

                AuthMsg subMsg ->
                    let
                        result_ =
                            Auth.update subMsg model.auth

                        errors =
                            case result_.error of
                                Just err ->
                                    model.errors
                                        ++ [ toError err model.currentTime ]

                                Nothing ->
                                    model.errors

                        authCmd =
                            Cmd.map AuthMsg result_.cmd

                        dashboardCmd =
                            let
                                maybeTokens =
                                    Auth.getTokens result_.model
                            in
                            case maybeTokens of
                                Just tokens ->
                                    Dashboard.initCmd model.dashboard tokens
                                        |> Cmd.map DashboardMsg

                                Nothing ->
                                    Cmd.none

                        ( resubmitCmd, lastMsg ) =
                            case model.lastMsg of
                                Just msg_ ->
                                    if result_.resubmit then
                                        ( toCmd msg_, Just msg )

                                    else
                                        ( Cmd.none, Just msg_ )

                                Nothing ->
                                    ( Cmd.none, Nothing )
                    in
                    ( { model | auth = result_.model, errors = errors, lastMsg = lastMsg }
                    , Cmd.batch [ authCmd, dashboardCmd, resubmitCmd ]
                    , False
                    )

                SettingsMsg subMsg ->
                    let
                        result_ =
                            Settings.update subMsg model.settings

                        errors =
                            case result_.error of
                                Just err ->
                                    model.errors
                                        ++ [ toError err model.currentTime ]

                                Nothing ->
                                    model.errors

                        cmd =
                            if result_.signOut then
                                Cmd.batch
                                    [ Cmd.map SettingsMsg result_.cmd
                                    , Auth.signOut model.auth |> Cmd.map AuthMsg
                                    ]

                            else
                                Cmd.map SettingsMsg result_.cmd
                    in
                    ( { model | settings = result_.model, errors = errors }
                    , cmd
                    , result_.updateLastMsg
                    )

                DashboardMsg subMsg ->
                    let
                        maybeTokens =
                            Auth.getTokens model.auth
                    in
                    case maybeTokens of
                        Just tokens ->
                            let
                                result_ =
                                    Dashboard.update subMsg model.dashboard tokens

                                errors =
                                    case result_.error of
                                        Just err ->
                                            model.errors
                                                ++ [ toError err model.currentTime ]

                                        Nothing ->
                                            model.errors
                            in
                            ( { model | dashboard = result_.model, errors = errors }
                            , Cmd.map DashboardMsg result_.cmd
                            , result_.updateLastMsg
                            )

                        Nothing ->
                            ( model, Cmd.none, False )

        refreshAccessToken =
            model_.errors
                |> List.reverse
                |> List.head
                |> Maybe.map .text
                |> Maybe.map (\t -> t == "The token expired.")
                |> Maybe.withDefault False
    in
    if refreshAccessToken then
        case model.auth of
            Auth.Authenticated { user, tokens } ->
                ( { model_
                    | errors =
                        model_.errors
                            |> List.reverse
                            |> List.drop 1
                            |> List.reverse
                  }
                , Cmd.batch
                    [ cmd_
                    , Auth.refreshAccessToken tokens.refresh (Just user)
                        |> Cmd.map AuthMsg
                    ]
                )

            _ ->
                ( model_, cmd_ )

    else if updateLastMsg then
        case model.lastMsg of
            Just (AuthMsg _) ->
                ( { model_ | lastMsg = Nothing }, cmd_ )

            _ ->
                ( { model_ | lastMsg = Just msg }, cmd_ )

    else
        ( model_, cmd_ )


initCmd : Model -> Cmd Msg
initCmd model =
    Auth.initCmd model.auth |> Cmd.map AuthMsg



-- view


view : Model -> Html Msg
view model =
    let
        translate =
            Language.initTranslate model.settings.language

        class_ =
            case model.settings.theme of
                Theme.Dark ->
                    ""

                Theme.Light ->
                    "light"
    in
    div [ id "root", class class_ ]
        [ headerView translate model
        , mainView translate model
        ]


headerView : (String -> String) -> Model -> Html Msg
headerView translate model =
    let
        signedIn =
            case model.auth of
                Auth.Authenticated _ ->
                    True

                _ ->
                    False
    in
    header []
        [ errorView translate model.errors
        , svgSprite "logo"
        , h1 [] [ text "Hub" ]
        , Settings.view translate model.settings signedIn |> Html.map SettingsMsg
        ]


mainView : (String -> String) -> Model -> Html Msg
mainView translate model =
    case model.auth of
        Auth.Authenticated _ ->
            Dashboard.view translate model.dashboard |> Html.map DashboardMsg

        Auth.RequestingAccess _ _ ->
            Dashboard.view translate model.dashboard |> Html.map DashboardMsg

        Auth.RequestingInfo _ _ ->
            Dashboard.view translate model.dashboard |> Html.map DashboardMsg

        _ ->
            Auth.view translate model.auth |> Html.map AuthMsg


errorView : (String -> String) -> List Error -> Html Msg
errorView translate errors =
    div [ id "error" ]
        (List.indexedMap
            (\i error ->
                span [ onClick (RemoveError i) ]
                    [ error.text |> translate |> text ]
            )
            errors
            ++ [ span [] [] ]
        )



-- subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ updateHotkeys model
        , updateTime
        ]


updateTime : Sub Msg
updateTime =
    if Config.debug then
        Sub.none

    else
        Time.every 1000 UpdateTime


updateHotkeys : Model -> Sub Msg
updateHotkeys model =
    let
        hotkeys =
            List.concat
                [ Settings.hotkeys model.settings |> Hotkeys.map SettingsMsg
                , Dashboard.hotkeys model.dashboard |> Hotkeys.map DashboardMsg
                ]
    in
    Hotkeys.createEvent hotkeys



-- utils


toError : String -> Time.Posix -> Error
toError text_ currentTime =
    { text = text_
    , expiry =
        currentTime
            |> Time.posixToMillis
            |> (\t -> t + (Config.errorExpiry * 1000))
            |> Time.millisToPosix
    }



-- init


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        model =
            initModel flags
    in
    ( model, initCmd model )
