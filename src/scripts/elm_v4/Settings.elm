module Settings exposing (Model, Msg, hotkeys, initModel, update, view)

import Hotkeys exposing (Hotkey)
import Html exposing (Html, button, div, span, text)
import Html.Attributes exposing (attribute, class, id)
import Html.Events exposing (onClick)
import I18Next exposing (Translations)
import Settings.Language as Language
import Settings.Theme as Theme
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (loseFocus)



-- model


type alias Model =
    { open : Bool
    , theme : Theme.Model
    , language : Language.Model
    }


initModel : String -> String -> Translations -> Model
initModel theme language translations =
    { open = False
    , theme = Theme.initModel theme
    , language = Language.initModel translations language
    }



-- update


type Msg
    = NoOp
    | ToggleOpen
    | Close
    | SignOut
    | ThemeMsg Theme.Msg
    | LanguageMsg Language.Msg


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , signOut : Bool
        }
update msg model =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , signOut = False
            }
    in
    case msg of
        NoOp ->
            return

        ToggleOpen ->
            { return
                | model = { model | open = not model.open }
                , cmd = loseFocus NoOp "settings__button"
            }

        Close ->
            { return
                | model = { model | open = False }
            }

        SignOut ->
            { return | model = { model | open = False }, signOut = True }

        ThemeMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Theme.update subMsg model.theme
            in
            { return
                | model = { model | theme = subModel }
                , cmd = Cmd.map ThemeMsg cmd
            }

        LanguageMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Language.update subMsg model.language
            in
            { return
                | model = { model | language = subModel }
                , cmd = Cmd.map LanguageMsg cmd
            }



-- view


view : (String -> String) -> Model -> Bool -> Html Msg
view translate model signedIn =
    let
        class_ =
            if model.open then
                "settings--open"

            else
                ""
    in
    div [ id "settings", class class_ ]
        [ buttonView translate
        , itemsView translate model signedIn
        ]


buttonView : (String -> String) -> Html Msg
buttonView _ =
    button
        [ id "settings__button"
        , attribute "aria-label" "Settings"
        , onClick ToggleOpen
        ]
        [ svgSprite "menu" ]


itemsView : (String -> String) -> Model -> Bool -> Html Msg
itemsView translate model signedIn =
    let
        signoutBtn_ =
            if signedIn then
                button [ onClick SignOut ]
                    [ svgSprite "logout", span [] [ "Sign out" |> translate |> text ] ]

            else
                text ""
    in
    div [ id "settings__items" ]
        [ div [ id "settings__bg" ] []
        , Theme.view translate model.theme |> Html.map ThemeMsg
        , Language.view model.language |> Html.map LanguageMsg
        , signoutBtn_
        ]



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys model =
    let
        closeHotkey =
            if model.open then
                [ { key = "Escape", ctrl = False, msg = Close } ]

            else
                []
    in
    List.concat
        [ closeHotkey
        , Theme.hotkeys model.theme |> Hotkeys.map ThemeMsg
        , Language.hotkeys model.language |> Hotkeys.map LanguageMsg
        ]
