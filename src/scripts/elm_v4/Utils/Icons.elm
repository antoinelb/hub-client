module Utils.Icons exposing (svgSprite)

import Svg exposing (Svg, svg, use)
import Svg.Attributes exposing (class, xlinkHref)


svgSprite : String -> Svg msg
svgSprite name =
    svg [ class "icon" ]
        [ use [ xlinkHref ("#" ++ name) ] []
        ]
