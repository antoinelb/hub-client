module Utils.Http exposing
    ( DetailedError(..)
    , Tokens
    , authGet
    , authPost
    , expectJson
    , expectString
    , expectWhatever
    , get
    , handleError
    , post
    , unAuthenticatedGet
    , unAuthenticatedPost
    )

import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode exposing (Value)


type DetailedError
    = BadUrl String
    | Timeout
    | NetworkError
    | BadStatus Int String
    | BadBody String


type alias Tokens =
    { refresh : String
    , access : String
    }


expectJson : (Result DetailedError a -> msg) -> Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    let
        toResult =
            \string ->
                Result.mapError Decode.errorToString (Decode.decodeString decoder string)

        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (toResult body)
    in
    Http.expectStringResponse toMsg <|
        resolve


expectString : (Result DetailedError String -> msg) -> Http.Expect msg
expectString toMsg =
    let
        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (Ok body)
    in
    Http.expectStringResponse toMsg <|
        resolve


expectWhatever : (Result DetailedError () -> msg) -> Http.Expect msg
expectWhatever toMsg =
    let
        toResult =
            \_ ->
                Ok ()

        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (toResult body)
    in
    Http.expectStringResponse toMsg <|
        resolve


handleError : DetailedError -> String -> String
handleError error defaultMessage =
    case error of
        BadStatus _ error_ ->
            error_

        Timeout ->
            "The server took too long to reach."

        NetworkError ->
            "The server couldn't be reached. "
                ++ "Check your internet connection and extensions."

        _ ->
            defaultMessage


get : Tokens -> { url : String, expect : Http.Expect msg } -> Cmd msg
get { access } { url, expect } =
    let
        headers =
            [ Http.header "access-token" access ]
    in
    Http.request
        { method = "GET"
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


post : Tokens -> { url : String, body : Value, expect : Http.Expect msg } -> Cmd msg
post { access } { url, body, expect } =
    let
        headers =
            [ Http.header "access-token" access ]
    in
    Http.request
        { method = "POST"
        , url = url
        , body = Http.jsonBody body
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


authGet : Tokens -> { url : String, expect : Http.Expect msg } -> Cmd msg
authGet { access, refresh } { url, expect } =
    let
        headers =
            [ Http.header "refresh-token" refresh
            , Http.header "access-token" access
            ]
    in
    Http.request
        { method = "GET"
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


authPost : Tokens -> { url : String, body : Value, expect : Http.Expect msg } -> Cmd msg
authPost { access, refresh } { url, body, expect } =
    let
        headers =
            [ Http.header "refresh-token" refresh
            , Http.header "access-token" access
            ]
    in
    Http.request
        { method = "POST"
        , url = url
        , body = Http.jsonBody body
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


unAuthenticatedGet : { url : String, expect : Http.Expect msg } -> Cmd msg
unAuthenticatedGet { url, expect } =
    Http.request
        { method = "GET"
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , headers = []
        , timeout = Nothing
        , tracker = Nothing
        }


unAuthenticatedPost : { url : String, body : Value, expect : Http.Expect msg } -> Cmd msg
unAuthenticatedPost { url, body, expect } =
    Http.request
        { method = "POST"
        , url = url
        , body = Http.jsonBody body
        , expect = expect
        , headers = []
        , timeout = Nothing
        , tracker = Nothing
        }
