port module Settings.Language exposing
    ( Model
    , Msg
    , hotkeys
    , initModel
    , initTranslate
    , update
    , view
    )

import Hotkeys exposing (Hotkey)
import Html exposing (Html, button, div, span, text)
import Html.Attributes exposing (class, hidden, id)
import Html.Events exposing (onClick)
import I18Next exposing (Translations, t)
import Utils.Events exposing (stopPropagationOnClick)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (loseFocus, toTitle)



-- external functions


initTranslate : Model -> (String -> String)
initTranslate model =
    \text ->
        case model.language of
            English ->
                text

            French ->
                t model.translations (text ++ ".fr")



-- model


type Language
    = English
    | French


type alias Model =
    { translations : Translations
    , language : Language
    , listOpen : Bool
    }


languageList : List Language
languageList =
    [ English
    , French
    ]


initModel : Translations -> String -> Model
initModel translations language =
    case language of
        "english" ->
            { translations = translations, language = English, listOpen = False }

        "français" ->
            { translations = translations, language = French, listOpen = False }

        _ ->
            { translations = translations, language = English, listOpen = False }



-- update


type Msg
    = NoOp
    | ToggleLanguageList
    | UpdateLanguage Language


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToggleLanguageList ->
            ( { model | listOpen = not model.listOpen }, Cmd.none )

        UpdateLanguage language ->
            ( { model | language = language, listOpen = False }
            , Cmd.batch
                [ language |> languageToString |> saveLanguage
                , loseFocus NoOp "language"
                ]
            )


languageToString : Language -> String
languageToString language =
    case language of
        English ->
            "english"

        French ->
            "français"



-- view


view : Model -> Html Msg
view model =
    let
        class_ =
            if model.listOpen then
                "language--showing-list"

            else
                ""
    in
    div [ id "language", class class_ ]
        [ div [ id "language__bg", stopPropagationOnClick ToggleLanguageList ] []
        , button [ id "language__button", onClick ToggleLanguageList ]
            [ svgSprite "globe"
            , span [] [ model.language |> languageToString |> toTitle |> text ]
            , span [ class "hotkey" ] [ text "L" ]
            ]
        , languageListView model
        ]


languageListView : Model -> Html Msg
languageListView model =
    let
        list =
            languageList
                |> List.filter (\language -> language /= model.language)
                |> List.sortBy languageToString
                |> List.map
                    (\language ->
                        button [ stopPropagationOnClick (UpdateLanguage language) ]
                            [ language |> languageToString |> toTitle |> text ]
                    )
    in
    div [ id "language__list", hidden (not model.listOpen) ] list



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys model =
    let
        nextLanguage =
            case model.language of
                English ->
                    French

                French ->
                    English
    in
    [ { key = "L", ctrl = False, msg = UpdateLanguage nextLanguage } ]



-- ports


port saveLanguage : String -> Cmd msg
