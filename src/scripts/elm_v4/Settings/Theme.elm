port module Settings.Theme exposing
    ( Model(..)
    , Msg
    , hotkeys
    , initModel
    , update
    , view
    )

import Hotkeys exposing (Hotkey)
import Html exposing (Html, button, span, text)
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (loseFocus)



-- model


type Model
    = Dark
    | Light


initModel : String -> Model
initModel theme =
    case theme of
        "light" ->
            Light

        _ ->
            Dark



-- update


type Msg
    = NoOp
    | ToggleTheme


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToggleTheme ->
            case model of
                Dark ->
                    ( Light
                    , Cmd.batch
                        [ saveTheme "light"
                        , loseFocus NoOp "theme"
                        ]
                    )

                Light ->
                    ( Dark
                    , Cmd.batch
                        [ saveTheme "dark"
                        , loseFocus NoOp "theme"
                        ]
                    )



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        icon =
            case model of
                Dark ->
                    "sun"

                Light ->
                    "moon"

        text_ =
            case model of
                Dark ->
                    "Light"

                Light ->
                    "Dark"
    in
    button [ id "theme", onClick ToggleTheme ]
        [ svgSprite icon
        , span [] [ text_ ++ " theme" |> translate |> text ]
        , span [ class "hotkey" ] [ text "T" ]
        ]



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys _ =
    [ { key = "T", ctrl = False, msg = ToggleTheme } ]



-- ports


port saveTheme : String -> Cmd msg
