port module Utils.DatetimeInput exposing
    ( Model
    , Msg(..)
    , initCmd
    , initModel
    , update
    , view
    )

import Html exposing (Html, div, input, span, text)
import Html.Attributes exposing (class, id, maxlength, placeholder, type_, value)
import Html.Events exposing (onClick, onInput)
import Time
import Time.Extra as TimeExtra
import Utils.Datetime exposing (formatDatetime, monthToInt)
import Utils.Events exposing (onKeyDown)
import Utils.Misc exposing (toCmd)



-- model


type alias Model =
    { datetime : Maybe Time.Posix
    , string : String
    }


initModel : Model
initModel =
    { datetime = Nothing
    , string = "2021 - 05 - 02   14 : 54"
    }



-- update


type Msg
    = UpdateDatetime Time.Posix
    | UpdateInput { ctrlKey : Bool, key : String, tag : String }


update : Msg -> Model -> Time.Zone -> Model
update msg model zone =
    case msg of
        UpdateDatetime time ->
            model

        UpdateInput input_ ->
            model


initCmd : Time.Posix -> Cmd Msg
initCmd time =
    UpdateDatetime time |> toCmd



-- view


view : String -> Model -> Html Msg
view id_ model =
    input
        [ id id_
        , class "datetime-input"
        , type_ "text"
        , value model.string
        , onKeyDown UpdateInput
        ]
        []



-- ports


port focusDatetimeInput : String -> Cmd msg
