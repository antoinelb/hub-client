module Utils.Datetime exposing
    ( formatDate
    , formatDatetime
    , monthToInt
    , parseDatetime
    )

import Parser exposing ((|.), (|=), Parser)
import Time
import Time.Extra as TimeExtra



-- exteral


formatDate : Time.Zone -> Time.Posix -> String
formatDate zone datetime =
    let
        parts =
            TimeExtra.posixToParts zone datetime

        year =
            parts.year
                |> String.fromInt

        month =
            parts.month
                |> monthToInt
                |> String.fromInt
                |> String.padLeft 2 '0'

        day =
            parts.day
                |> String.fromInt
                |> String.padLeft 2 '0'
    in
    year ++ "-" ++ month ++ "-" ++ day


formatDatetime : Time.Zone -> Time.Posix -> String
formatDatetime zone datetime =
    let
        parts =
            TimeExtra.posixToParts zone datetime

        year =
            parts.year
                |> String.fromInt

        month =
            parts.month
                |> monthToInt
                |> String.fromInt
                |> String.padLeft 2 '0'

        day =
            parts.day
                |> String.fromInt
                |> String.padLeft 2 '0'

        hour =
            parts.hour
                |> String.fromInt
                |> String.padLeft 2 '0'

        minute =
            parts.minute
                |> String.fromInt
                |> String.padLeft 2 '0'
    in
    year ++ "-" ++ month ++ "-" ++ day ++ " " ++ hour ++ ":" ++ minute


parseDatetime : String -> Result (List Parser.DeadEnd) TimeExtra.Parts
parseDatetime datetime =
    Parser.run datetimeParser datetime



-- internal


monthToInt : Time.Month -> Int
monthToInt month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12


intToMonth : Int -> Maybe Time.Month
intToMonth month =
    case month of
        1 ->
            Just Time.Jan

        2 ->
            Just Time.Feb

        3 ->
            Just Time.Mar

        4 ->
            Just Time.Apr

        5 ->
            Just Time.May

        6 ->
            Just Time.Jun

        7 ->
            Just Time.Jul

        8 ->
            Just Time.Aug

        9 ->
            Just Time.Sep

        10 ->
            Just Time.Oct

        11 ->
            Just Time.Nov

        12 ->
            Just Time.Dec

        _ ->
            Nothing


datetimeParser : Parser TimeExtra.Parts
datetimeParser =
    Parser.succeed (\y mo d h mi -> TimeExtra.Parts y mo d h mi 0 0)
        |= paddedInt 4
        |. Parser.symbol "-"
        |= paddedMonth
        |. Parser.symbol "-"
        |= paddedInt 2
        |. Parser.symbol " "
        |= paddedInt 2
        |. Parser.symbol ":"
        |= paddedInt 2


paddedInt : Int -> Parser Int
paddedInt length =
    Parser.loop ""
        (\n ->
            if String.length n /= length then
                Parser.problem ("Invalid integer: " ++ n)

            else
                case String.toInt n of
                    Just n_ ->
                        Parser.succeed n_ |> Parser.map Parser.Done

                    Nothing ->
                        Parser.problem ("Invalid integer: " ++ n)
        )


paddedMonth : Parser Time.Month
paddedMonth =
    Parser.loop ""
        (\month ->
            if String.length month /= 2 then
                Parser.problem ("Invalid month: " ++ month)

            else
                case String.toInt month of
                    Just month_ ->
                        case intToMonth month_ of
                            Just m ->
                                Parser.succeed m |> Parser.map Parser.Done

                            Nothing ->
                                Parser.problem ("Invalid month: " ++ month)

                    Nothing ->
                        Parser.problem ("Invalid month: " ++ month)
        )
