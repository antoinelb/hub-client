module Menu exposing (Model, Msg, hotkeys, initModel, update, view)

import Hotkeys exposing (Hotkey)
import Html exposing (Html, button, div, span, text)
import Html.Attributes exposing (class, id, title)
import Html.Events exposing (onClick)
import I18Next exposing (Translations)
import Menu.Language as Language
import Menu.Theme as Theme
import Utils.Events exposing (stopPropagationOnClick)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (loseFocus)
import Utils.Touch as Touch



-- model


type alias Model =
    { open : Bool
    , touchPositionX : Maybe Float
    , theme : Theme.Model
    , language : Language.Model
    }


initModel : String -> String -> Translations -> Model
initModel theme language translations =
    { open = False
    , touchPositionX = Nothing
    , theme = Theme.initModel theme
    , language = Language.initModel translations language
    }



-- update


type Msg
    = NoOp
    | ToggleOpen
    | Close
    | StartMenuSwipe Touch.Touch
    | EndMenuSwipe Touch.Touch
    | SignOut
    | ThemeMsg Theme.Msg
    | LanguageMsg Language.Msg


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , signOut : Bool
        }
update msg model =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , signOut = False
            }
    in
    case msg of
        NoOp ->
            return

        ToggleOpen ->
            { return
                | model = { model | open = not model.open }
                , cmd = loseFocus NoOp "menu__button"
            }

        Close ->
            { return
                | model = { model | open = False }
            }

        StartMenuSwipe touchEvent ->
            { return | model = { model | touchPositionX = Just touchEvent.clientX } }

        EndMenuSwipe touchEvent ->
            case model.touchPositionX of
                Nothing ->
                    return

                Just positionX ->
                    if model.open then
                        if positionX < touchEvent.clientX then
                            { return
                                | model =
                                    { model
                                        | open = False
                                        , touchPositionX = Nothing
                                    }
                            }

                        else
                            { return | model = { model | touchPositionX = Nothing } }

                    else if positionX > touchEvent.clientX then
                        { return
                            | model =
                                { model
                                    | open = True
                                    , touchPositionX = Nothing
                                }
                        }

                    else
                        { return
                            | model =
                                { model | touchPositionX = Nothing }
                        }

        SignOut ->
            { return | model = { model | open = False }, signOut = True }

        ThemeMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Theme.update subMsg model.theme
            in
            { return
                | model = { model | theme = subModel }
                , cmd = Cmd.map ThemeMsg cmd
            }

        LanguageMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Language.update subMsg model.language
            in
            { return
                | model = { model | language = subModel }
                , cmd = Cmd.map LanguageMsg cmd
            }



-- view


view : (String -> String) -> Model -> Bool -> Html Msg
view translate model signedIn =
    let
        class_ =
            if model.open then
                "menu--open"

            else
                ""
    in
    div [ id "menu", class class_ ]
        [ touchAreaView
        , buttonView translate
        , itemsView translate model signedIn
        ]


buttonView : (String -> String) -> Html Msg
buttonView translate =
    button
        [ id "menu__button"
        , "Settings"
            |> translate
            |> title
        , onClick ToggleOpen
        ]
        [ svgSprite "menu" ]


itemsView : (String -> String) -> Model -> Bool -> Html Msg
itemsView translate model signedIn =
    let
        signoutBtn_ =
            if signedIn then
                button [ onClick SignOut ]
                    [ svgSprite "logout", span [] [ "Sign out" |> translate |> text ] ]

            else
                text ""
    in
    div [ id "menu__items" ]
        [ div [ id "menu__bg" ] []
        , Theme.view translate model.theme |> Html.map ThemeMsg
        , Language.view model.language |> Html.map LanguageMsg
        , signoutBtn_
        ]


touchAreaView : Html Msg
touchAreaView =
    div
        [ id "menu__touch-area"
        , Touch.onTouchEvent Touch.TouchStart StartMenuSwipe
        , Touch.onTouchEvent Touch.TouchEnd EndMenuSwipe
        , stopPropagationOnClick ToggleOpen
        ]
        []



-- hotkeys


hotkeys : Model -> List (Hotkey Msg)
hotkeys model =
    let
        closeHotkey =
            if model.open then
                [ { key = "Escape", ctrl = False, msg = Close } ]

            else
                []
    in
    List.concat
        [ closeHotkey
        , Theme.hotkeys model.theme |> Hotkeys.map ThemeMsg
        , Language.hotkeys model.language |> Hotkeys.map LanguageMsg
        ]
