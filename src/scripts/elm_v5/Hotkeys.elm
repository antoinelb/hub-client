module Hotkeys exposing (Hotkey, createEvent, map)

import Browser.Events exposing (onKeyDown)
import Json.Decode as Decode
import Utils.Events exposing (keyInfo)


type alias Hotkey msg =
    { key : String
    , ctrl : Bool
    , msg : msg
    }


createEvent : List (Hotkey msg) -> Sub msg
createEvent hotkeys =
    let
        isKey { key, ctrlKey, tag } =
            case tag of
                "input" ->
                    Decode.fail ""

                _ ->
                    hotkeys
                        |> List.filter (\hotkey -> hotkey.key == key && hotkey.ctrl == ctrlKey)
                        |> List.head
                        |> Maybe.map (\hotkey -> Decode.succeed hotkey.msg)
                        |> Maybe.withDefault (Decode.fail "")
    in
    onKeyDown (Decode.andThen isKey keyInfo)


map : (a -> b) -> List (Hotkey a) -> List (Hotkey b)
map msg hotkeys =
    List.map (\h -> { key = h.key, ctrl = h.ctrl, msg = msg h.msg }) hotkeys
