module Page exposing
    ( Model
    , Msg
    , defaultModel
    , fromPage
    , initCmd
    , initModel
    , routeParser
    , update
    , view
    )

import Html exposing (Html, footer, text)
import Url.Parser as Parser exposing (Parser)
import Utils.Http exposing (Tokens)



-- model


type Model
    = Home
    | NotFound
    | Unauthorized
    | Admin
    | Schedule
    | Budget


initModel : Model -> Model -> Model
initModel oldModel newModel =
    case ( oldModel, newModel ) of
        ( _, _ ) ->
            newModel


defaultModel : Model
defaultModel =
    Schedule



-- update


type Msg
    = NoOp


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case ( msg, model ) of
        ( _, _ ) ->
            return


initCmd : Model -> Model -> Tokens -> Cmd Msg
initCmd oldModel newModel tokens =
    case newModel of
        _ ->
            Cmd.none



-- view


view : (String -> String) -> Model -> { title : String, body : List (Html Msg) }
view translate model =
    let
        ( title, body ) =
            case model of
                Home ->
                    ( "test", text "" )

                NotFound ->
                    ( "test", text "" )

                Unauthorized ->
                    ( "test", text "" )

                Admin ->
                    ( "test", text "" )

                Schedule ->
                    ( "test", text "" )

                Budget ->
                    ( "test", text "" )
    in
    { title = title
    , body = [ body ]
    }



-- routing


routeParser : Parser (Model -> a) a
routeParser =
    List.concat
        []
        |> Parser.oneOf


fromPage : Model -> String
fromPage model =
    case model of
        Home ->
            "/"

        NotFound ->
            "/not-found"

        Unauthorized ->
            "/unauthorized"

        Admin ->
            "/admin"

        Schedule ->
            "/schedule"

        Budget ->
            "/budget"
