port module Auth exposing
    ( Model(..)
    , Msg
    , User
    , getTokens
    , getUser
    , getUserInfo
    , initCmd
    , initModel
    , initUser
    , refreshAccessToken
    , signOut
    , signedIn
    , update
    , userDecoder
    , view
    )

import Config exposing (apis)
import Html
    exposing
        ( Html
        , div
        , form
        , h2
        , input
        , label
        , main_
        , text
        )
import Html.Attributes exposing (autofocus, hidden, id, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Time
import Utils.Datetime exposing (timestampDecoder)
import Utils.Events exposing (onEnter)
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , authGet
        , expectJson
        , expectWhatever
        , handleError
        , unAuthenticatedPost
        )
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus)



-- model


type alias User =
    { username : String
    , isAdmin : Bool
    , createdOn : Time.Posix
    , lastModified : Time.Posix
    }


type alias Credentials =
    { username : String
    , password : String
    , passwordConfirm : Maybe String
    }


type alias PasswordUpdate =
    { oldPassword : String
    , password : String
    , passwordConfirm : String
    }


type Model
    = EnteringCredentials Credentials
    | ReceivingSignInConfirmation Credentials
    | RequestingAccess String (Maybe User)
    | RequestingInfo Tokens (Maybe User)
    | Authenticated { user : User, tokens : Tokens }
    | UpdatingPassword
        { user : User
        , tokens : Tokens
        , passwordUpdate : PasswordUpdate
        }
    | ReceivingUpdatePasswordConfirmation
        { user : User
        , tokens : Tokens
        , passwordUpdate : PasswordUpdate
        }


initUser : User
initUser =
    { username = ""
    , isAdmin = False
    , createdOn = Time.millisToPosix 0
    , lastModified = Time.millisToPosix 0
    }


initModel : Maybe String -> Model
initModel refreshToken =
    case refreshToken of
        Just token ->
            RequestingAccess token Nothing

        Nothing ->
            EnteringCredentials
                { username = ""
                , password = ""
                , passwordConfirm = Nothing
                }



-- update


type Msg
    = NoOp
    | ToggleSignIn
    | SignIn
    | ChangeUsername String
    | ChangePassword String
    | ChangePasswordConfirm String
    | ChangeOldPassword String
    | ReceivedSignInConfirmation (Result DetailedError Tokens)
    | ReceivedAccessToken (Maybe User) (Result DetailedError Tokens)
    | ReceivedUserInfo Tokens (Maybe User) (Result DetailedError User)
    | ReceivedSignOut (Result DetailedError ())
    | UpdatePassword
    | ReceivedUpdatePasswordConfirmation (Result DetailedError ())
    | CancelUpdatePassword


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , resubmit : Bool
        }
update msg model =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , resubmit = False
            }
    in
    case msg of
        NoOp ->
            return

        ToggleSignIn ->
            case model of
                EnteringCredentials credentials ->
                    case credentials.passwordConfirm of
                        Just _ ->
                            { return
                                | model =
                                    EnteringCredentials
                                        { credentials
                                            | passwordConfirm = Nothing
                                        }
                            }

                        Nothing ->
                            { return
                                | model =
                                    EnteringCredentials
                                        { credentials
                                            | passwordConfirm = Just ""
                                        }
                            }

                _ ->
                    return

        SignIn ->
            case model of
                EnteringCredentials credentials ->
                    case credentials.passwordConfirm of
                        Just passwordConfirm ->
                            if credentials.password == passwordConfirm then
                                { return
                                    | model = ReceivingSignInConfirmation credentials
                                    , cmd = signIn credentials True
                                }

                            else
                                { return | error = Just "The passwords don't match" }

                        Nothing ->
                            { return
                                | model = ReceivingSignInConfirmation credentials
                                , cmd = signIn credentials False
                            }

                _ ->
                    return

        ChangeUsername username ->
            case model of
                EnteringCredentials credentials ->
                    { return
                        | model =
                            EnteringCredentials
                                { credentials
                                    | username = username
                                }
                    }

                _ ->
                    return

        ChangePassword password ->
            case model of
                EnteringCredentials credentials ->
                    { return
                        | model =
                            EnteringCredentials
                                { credentials
                                    | password = password
                                }
                    }

                UpdatingPassword { user, tokens, passwordUpdate } ->
                    { return
                        | model =
                            UpdatingPassword
                                { user = user
                                , tokens = tokens
                                , passwordUpdate =
                                    { passwordUpdate
                                        | password = password
                                    }
                                }
                    }

                _ ->
                    return

        ChangePasswordConfirm password ->
            case model of
                EnteringCredentials credentials ->
                    { return
                        | model =
                            EnteringCredentials
                                { credentials
                                    | passwordConfirm = Just password
                                }
                    }

                UpdatingPassword { user, tokens, passwordUpdate } ->
                    { return
                        | model =
                            UpdatingPassword
                                { user = user
                                , tokens = tokens
                                , passwordUpdate =
                                    { passwordUpdate
                                        | passwordConfirm = password
                                    }
                                }
                    }

                _ ->
                    return

        ChangeOldPassword password ->
            case model of
                UpdatingPassword { user, tokens, passwordUpdate } ->
                    { return
                        | model =
                            UpdatingPassword
                                { user = user
                                , tokens = tokens
                                , passwordUpdate =
                                    { passwordUpdate
                                        | oldPassword = password
                                    }
                                }
                    }

                _ ->
                    return

        ReceivedSignInConfirmation result ->
            case model of
                ReceivingSignInConfirmation credentials ->
                    case result of
                        Ok tokens ->
                            { return
                                | model = RequestingInfo tokens Nothing
                                , cmd = getUserInfo tokens Nothing
                            }

                        Err err ->
                            let
                                error =
                                    handleError err "There was an error authenticating you."
                            in
                            { return
                                | model = EnteringCredentials credentials
                                , error = Just error
                            }

                _ ->
                    return

        ReceivedAccessToken user result ->
            case result of
                Ok tokens ->
                    { return
                        | model = RequestingInfo tokens user
                        , cmd = getUserInfo tokens user
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        ( model_, error, cmd ) =
                            if error_ == "You don't have access to this resource." then
                                ( model, Nothing, Cmd.none )

                            else
                                let
                                    model__ =
                                        initModel Nothing
                                in
                                ( model__
                                , Just error_
                                , Cmd.batch
                                    [ saveRefreshToken Nothing
                                    , initCmd model__
                                    ]
                                )
                    in
                    { return
                        | model = model_
                        , error = error
                        , cmd = cmd
                    }

        ReceivedUserInfo tokens maybeUser result ->
            case result of
                Ok user ->
                    let
                        ( cmd, resubmit ) =
                            case maybeUser of
                                Just _ ->
                                    ( saveRefreshToken (Just tokens.refresh)
                                    , True
                                    )

                                Nothing ->
                                    ( saveRefreshToken (Just tokens.refresh)
                                    , False
                                    )
                    in
                    { return
                        | model = Authenticated { user = user, tokens = tokens }
                        , cmd = cmd
                        , resubmit = resubmit
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                    }

        ReceivedSignOut result ->
            case result of
                Ok _ ->
                    let
                        model_ =
                            initModel Nothing
                    in
                    { return
                        | model = model_
                        , cmd = Cmd.batch [ saveRefreshToken Nothing, initCmd model_ ]
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                    }

        UpdatePassword ->
            case model of
                UpdatingPassword { user, tokens, passwordUpdate } ->
                    if passwordUpdate.password == passwordUpdate.passwordConfirm then
                        { return
                            | model =
                                ReceivingUpdatePasswordConfirmation
                                    { user = user
                                    , tokens = tokens
                                    , passwordUpdate = passwordUpdate
                                    }
                            , cmd = updatePassword passwordUpdate
                        }

                    else
                        { return
                            | error = Just "The passwords don't match"
                        }

                _ ->
                    return

        ReceivedUpdatePasswordConfirmation result ->
            case result of
                Ok _ ->
                    return

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error updating the password."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                    }

        CancelUpdatePassword ->
            case model of
                UpdatingPassword { user, tokens } ->
                    { return
                        | model =
                            Authenticated { user = user, tokens = tokens }
                    }

                _ ->
                    return


initCmd : Model -> Cmd Msg
initCmd model =
    case model of
        EnteringCredentials _ ->
            focus NoOp "auth__username"

        RequestingAccess token refreshing ->
            refreshAccessToken token refreshing

        _ ->
            Cmd.none


signIn : Credentials -> Bool -> Cmd Msg
signIn { username, password } signup =
    let
        url =
            if signup then
                "/users/signup"

            else
                "/users/signin"

        body =
            Encode.object
                [ ( "username", Encode.string username )
                , ( "password", Encode.string password )
                ]
    in
    unAuthenticatedPost
        { url = apis.auth ++ url
        , body = body
        , expect = expectJson ReceivedSignInConfirmation tokenDecoder
        }


updatePassword : PasswordUpdate -> Cmd Msg
updatePassword { oldPassword, password } =
    let
        body =
            Encode.object
                [ ( "password", Encode.string password )
                , ( "previous_password", Encode.string oldPassword )
                ]
    in
    unAuthenticatedPost
        { url = apis.auth ++ "/users/update-password"
        , body = body
        , expect = expectWhatever ReceivedUpdatePasswordConfirmation
        }


refreshAccessToken : String -> Maybe User -> Cmd Msg
refreshAccessToken token user =
    let
        headers =
            [ Http.header "refresh-token" token ]

        tokensDecoder =
            Decode.map
                (\access -> { access = access, refresh = token })
                (Decode.field "access-token" Decode.string)
    in
    Http.request
        { method = "GET"
        , url = apis.auth ++ "/users/refresh"
        , body = Http.emptyBody
        , expect = expectJson (ReceivedAccessToken user) tokensDecoder
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


getUserInfo : Tokens -> Maybe User -> Cmd Msg
getUserInfo tokens user =
    authGet tokens
        { url = apis.auth ++ "/users/info"
        , expect = expectJson (ReceivedUserInfo tokens user) userDecoder
        }


signOut : Model -> Cmd Msg
signOut model =
    let
        refreshToken =
            case model of
                Authenticated { tokens } ->
                    tokens.refresh

                RequestingInfo { refresh } _ ->
                    refresh

                RequestingAccess refresh _ ->
                    refresh

                _ ->
                    ""

        headers =
            [ Http.header "refresh-token" refreshToken ]
    in
    if String.isEmpty refreshToken then
        Cmd.none

    else
        Http.request
            { method = "POST"
            , url = apis.auth ++ "/users/signout"
            , body = Encode.object [] |> Http.jsonBody
            , expect = expectWhatever ReceivedSignOut
            , headers = headers
            , timeout = Nothing
            , tracker = Nothing
            }



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        form_ =
            case model of
                EnteringCredentials credentials ->
                    enteringCredentialsView translate credentials

                ReceivingSignInConfirmation _ ->
                    loadingView translate "Authenticating..."

                RequestingAccess _ _ ->
                    loadingView translate "Verifying access..."

                RequestingInfo _ _ ->
                    loadingView translate "Getting info..."

                Authenticated _ ->
                    text ""

                UpdatingPassword { passwordUpdate } ->
                    updatingPasswordView translate passwordUpdate

                ReceivingUpdatePasswordConfirmation _ ->
                    loadingView translate "Updating password..."
    in
    main_ [ id "auth" ]
        [ form_ ]


enteringCredentialsView : (String -> String) -> Credentials -> Html Msg
enteringCredentialsView translate { username, password, passwordConfirm } =
    let
        { title, toggle, passwordConfirmHidden, passwordConfirm_ } =
            case passwordConfirm of
                Just passwordConfirm__ ->
                    { title = "Sign up"
                    , toggle = "Sign in"
                    , passwordConfirmHidden = False
                    , passwordConfirm_ = passwordConfirm__
                    }

                Nothing ->
                    { title = "Sign in"
                    , toggle = "Sign up"
                    , passwordConfirmHidden = True
                    , passwordConfirm_ = ""
                    }
    in
    form [ id "auth__box", onSubmit SignIn ]
        [ h2 [] [ title |> translate |> text ]
        , label []
            [ "Username" |> translate |> text
            , input
                [ id "auth__username"
                , type_ "text"
                , autofocus True
                , value username
                , onInput ChangeUsername
                , onEnter SignIn
                ]
                []
            ]
        , label []
            [ "Password" |> translate |> text
            , input
                [ type_ "password"
                , value password
                , onInput ChangePassword
                , onEnter SignIn
                ]
                []
            ]
        , label [ hidden passwordConfirmHidden ]
            [ "Confirm password" |> translate |> text
            , input
                [ type_ "password"
                , value passwordConfirm_
                , onInput ChangePasswordConfirm
                , onEnter SignIn
                ]
                []
            ]
        , input
            [ type_ "submit", onClick SignIn, title |> translate |> value ]
            []
        , input
            [ type_ "button", onClick ToggleSignIn, toggle |> translate |> value ]
            []
        ]


updatingPasswordView : (String -> String) -> PasswordUpdate -> Html Msg
updatingPasswordView translate { oldPassword, password, passwordConfirm } =
    form [ id "auth__box", onSubmit UpdatePassword ]
        [ h2 [] [ "Update password" |> translate |> text ]
        , label []
            [ "Old password" |> translate |> text
            , input
                [ id "auth__old-password"
                , type_ "password"
                , autofocus True
                , value oldPassword
                , onInput ChangePassword
                , onEnter UpdatePassword
                ]
                []
            ]
        , label []
            [ "Password" |> translate |> text
            , input
                [ type_ "password"
                , value password
                , onInput ChangePassword
                , onEnter UpdatePassword
                ]
                []
            ]
        , label []
            [ "Confirm password" |> translate |> text
            , input
                [ type_ "password"
                , value passwordConfirm
                , onInput ChangePasswordConfirm
                , onEnter UpdatePassword
                ]
                []
            ]
        , input
            [ type_ "submit", onClick SignIn, "Update" |> translate |> value ]
            []
        , input
            [ type_ "button", onClick CancelUpdatePassword, "Cancel" |> translate |> value ]
            []
        ]


loadingView : (String -> String) -> String -> Html Msg
loadingView translate msg =
    div [ id "loading" ]
        [ h2 [] [ msg |> translate |> text ]
        , svgSprite "loading"
        ]



-- decoders


userDecoder : Decoder User
userDecoder =
    Decode.map4 User
        (Decode.field "username" Decode.string)
        (Decode.field "is_admin" Decode.bool)
        (Decode.field "created_on" timestampDecoder)
        (Decode.field "last_modified" timestampDecoder)



-- utils


signedIn : Model -> Bool
signedIn model =
    case model of
        RequestingAccess _ _ ->
            True

        RequestingInfo _ _ ->
            True

        Authenticated _ ->
            True

        UpdatingPassword _ ->
            True

        ReceivingUpdatePasswordConfirmation _ ->
            True

        _ ->
            False


getUser : Model -> Maybe User
getUser model =
    case model of
        RequestingAccess _ maybeUser ->
            maybeUser

        RequestingInfo _ maybeUser ->
            maybeUser

        Authenticated { user } ->
            Just user

        UpdatingPassword { user } ->
            Just user

        ReceivingUpdatePasswordConfirmation { user } ->
            Just user

        _ ->
            Nothing


getTokens : Model -> Maybe Tokens
getTokens model =
    case model of
        RequestingInfo tokens _ ->
            Just tokens

        Authenticated { tokens } ->
            Just tokens

        UpdatingPassword { tokens } ->
            Just tokens

        ReceivingUpdatePasswordConfirmation { tokens } ->
            Just tokens

        _ ->
            Nothing



-- decoders


tokenDecoder : Decoder Tokens
tokenDecoder =
    Decode.map2 (\access refresh -> { access = access, refresh = refresh })
        (Decode.field "access-token" Decode.string)
        (Decode.field "refresh-token" Decode.string)



-- ports


port saveRefreshToken : Maybe String -> Cmd msg
