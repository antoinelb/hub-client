module Routing exposing (Route(..), fromRoute, toRoute)

import Page
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser)


type Route
    = Auth Bool
    | Page Page.Model


routeParser : Parser (Route -> a) a
routeParser =
    List.append
        [ Parser.map (Auth False) (Parser.s "auth")
        , Parser.map (Auth True) (Parser.s "signout")
        ]
        [ Parser.map Page Page.routeParser
        ]
        |> Parser.oneOf


toRoute : Url -> Route
toRoute url =
    Maybe.withDefault (Page Page.notFound) (Parser.parse routeParser url)


fromRoute : Route -> String
fromRoute route =
    case route of
        Auth False ->
            "/auth"

        Auth True ->
            "/signout"

        Page page ->
            Page.fromPage page
