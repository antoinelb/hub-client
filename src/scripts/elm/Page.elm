module Page exposing
    ( Model(..)
    , Msg
    , defaultModel
    , fromPage
    , initCmd
    , initModel
    , notFound
    , routeParser
    , update
    , validateModel
    , view
    )

import Auth
import Html exposing (Html, footer, text)
import Page.Admin as Admin
import Page.Error as Error
import Time
import Url.Parser as Parser exposing (Parser)
import Utils.Http exposing (Tokens)



-- model


type Model
    = Error Error.Model
    | Admin Admin.Model


initModel : Model -> Model -> Model
initModel oldModel newModel =
    case ( oldModel, newModel ) of
        ( _, _ ) ->
            newModel


defaultModel : Model
defaultModel =
    Error Error.NotFound


notFound : Model
notFound =
    Error Error.NotFound


validateModel : Auth.Model -> Model -> Model
validateModel auth model =
    case model of
        Admin _ ->
            case Auth.getUser auth of
                Just user ->
                    if user.isAdmin then
                        model

                    else
                        Error Error.Unauthorized

                Nothing ->
                    Error Error.Unauthorized

        _ ->
            model



-- update


type Msg
    = NoOp
    | AdminMsg Admin.Msg


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case ( msg, model ) of
        ( AdminMsg subMsg, Admin subModel ) ->
            let
                return_ =
                    Admin.update subMsg subModel tokens
            in
            { return
                | model = Admin return_.model
                , cmd = Cmd.map AdminMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( _, _ ) ->
            return


initCmd : Model -> Model -> Tokens -> Cmd Msg
initCmd oldModel newModel tokens =
    case newModel of
        Error _ ->
            Cmd.none

        Admin model ->
            Admin.initCmd model tokens
                |> Cmd.map AdminMsg



-- view


view :
    (String -> String)
    -> Time.Zone
    -> Model
    -> { title : String, body : List (Html Msg) }
view translate zone model =
    let
        ( title, body ) =
            case model of
                Error model_ ->
                    Error.view translate model_

                Admin model_ ->
                    let
                        ( title_, body_ ) =
                            Admin.view translate zone model_
                    in
                    ( title_
                    , Html.map AdminMsg body_
                    )
    in
    { title = title
    , body = [ body ]
    }



-- routing


routeParser : Parser (Model -> a) a
routeParser =
    [ Parser.map Error Error.routeParser
    , Parser.map Admin Admin.routeParser
    ]
        |> Parser.oneOf


fromPage : Model -> String
fromPage model =
    case model of
        Error model_ ->
            Error.routeFromModel model_

        Admin model_ ->
            Admin.routeFromModel model_
