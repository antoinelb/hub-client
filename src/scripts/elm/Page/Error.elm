module Page.Error exposing (Model(..), routeFromModel, routeParser, view)

import Html exposing (Html, a, h3, main_, p, text)
import Html.Attributes exposing (href, id)
import Url.Parser as Parser exposing (Parser)



-- model


type Model
    = NotFound
    | Unauthorized



-- view


view : (String -> String) -> Model -> ( String, Html msg )
view translate model =
    let
        ( title, msg ) =
            case model of
                NotFound ->
                    ( "Not Found", "The page couldn't be found." )

                Unauthorized ->
                    ( "Unauthorized", "You're not authorized to access this page." )

        createLink ( app, link ) =
            a [ href link ] [ app |> translate |> text ]
    in
    ( title
    , main_ [ id "error-page" ]
        [ h3 [] [ msg |> translate |> text ]
        , p []
            (List.append
                [ "Here are the applications available to you:" |> translate |> text ]
                (List.map createLink appsView)
            )
        ]
    )


appsView : List ( String, String )
appsView =
    [ ( "Budget", "/budget" ), ( "Schedule", "/schedule" ) ]



-- routing


routeParser : Parser (Model -> a) a
routeParser =
    [ Parser.map NotFound (Parser.s "not-found")
    , Parser.map Unauthorized (Parser.s "unauthorized")
    ]
        |> Parser.oneOf


routeFromModel : Model -> String
routeFromModel model =
    case model of
        NotFound ->
            "/not-found"

        Unauthorized ->
            "/unauthorized"
