module Page.Admin.Auth exposing (Model, Msg, initCmd, initModel, update, view)

import Auth exposing (User, userDecoder)
import Config exposing (apis)
import Html
    exposing
        ( Html
        , div
        , section
        )
import Html.Attributes exposing (id)
import Json.Decode as Decode
import Time
import Utils.Datetime exposing (formatDatetime)
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , authGet
        , expectJson
        , handleError
        )
import Utils.Icons exposing (svgSprite)
import Utils.Table as Table



-- model


type alias Model =
    { users : Maybe (List User)
    , tableState :
        Table.Model
            { username : String
            , isAdmin : String
            , createdOn : String
            , lastModified : String
            }
            Msg
    , editing : Maybe ( String, User )
    , deleting : Maybe User
    }


initModel : Model
initModel =
    { users = Nothing
    , tableState =
        Table.initModel
            [ ( "Username", .username, Just (ToggleEdit << Just) )
            , ( "Admin", .isAdmin, Nothing )
            , ( "Created on", .createdOn, Nothing )
            , ( "Last modified", .lastModified, Nothing )
            ]
            .username
            False
            .username
    , editing = Nothing
    , deleting = Nothing
    }



-- update


type Msg
    = NoOp
    | TableMsg (Table.Msg Msg)
    | ReceivedUsers (Result DetailedError (List User))
    | ToggleEdit (Maybe String)



-- | ReceivedUser String (Result DetailedError User)
-- | Update
-- | UpdateIsAdmin Bool
-- | ToggleDelete
-- | Delete
-- | ReceivedDeleteConfirm String (Result DetailedError ())


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case msg of
        NoOp ->
            return

        TableMsg subMsg ->
            case subMsg of
                Table.ExternalMsg msg_ ->
                    update msg_ model tokens

                _ ->
                    let
                        tableState =
                            Table.update subMsg model.tableState
                    in
                    { return | model = { model | tableState = tableState } }

        ReceivedUsers result ->
            case result of
                Ok users ->
                    { return
                        | model = { model | users = Just users }
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error retrieving the users."
                    in
                    { return | error = Just error }

        ToggleEdit maybeKey ->
            case maybeKey of
                Just key ->
                    let
                        maybeUser =
                            model.users
                                |> Maybe.withDefault []
                                |> List.filter (\u -> u.username == key)
                                |> List.head
                    in
                    case maybeUser of
                        Just user ->
                            { return | model = { model | editing = Just ( key, user ) } }

                        Nothing ->
                            return

                Nothing ->
                    { return | model = { model | editing = Nothing } }


getUsers : Tokens -> Cmd Msg
getUsers tokens =
    authGet tokens
        { url = apis.auth ++ "/admin/users/"
        , expect = expectJson ReceivedUsers (Decode.list userDecoder)
        }


initCmd : Tokens -> Cmd Msg
initCmd tokens =
    getUsers tokens



-- view


view : (String -> String) -> Time.Zone -> Model -> Html Msg
view translate zone model =
    let
        table users =
            let
                users_ =
                    case model.editing of
                        Just ( key, _ ) ->
                            List.filter
                                (\user_ -> user_.username /= key)
                                users

                        Nothing ->
                            users

                toUserData user =
                    { username = user.username
                    , isAdmin =
                        if user.isAdmin then
                            "✓"

                        else
                            ""
                    , createdOn = formatDatetime zone user.createdOn
                    , lastModified = formatDatetime zone user.lastModified
                    }

                userData =
                    List.map toUserData users_
            in
            Table.view translate model.tableState userData
                |> Html.map TableMsg
    in
    case model.users of
        Just users ->
            section [ id "admin__users" ]
                [ table users
                ]

        _ ->
            section [ id "admin__users" ]
                [ div [ id "admin__loading" ] [ svgSprite "loading" ] ]
