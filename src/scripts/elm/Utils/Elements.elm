module Utils.Elements exposing (checkbox)

import Html exposing (Html, div, input)
import Html.Attributes exposing (checked, class, type_)
import Html.Events exposing (onCheck)


checkbox : Bool -> (Bool -> msg) -> Html msg
checkbox checked_ msg =
    div [ class "checkbox" ]
        [ input [ type_ "checkbox", checked checked_, onCheck msg ] []
        , div [ class "checkbox__bg" ] []
        ]
