module Utils.Touch exposing (Touch, TouchEvent(..), onTouchEvent)

import Html exposing (Attribute)
import Html.Events exposing (on)
import Json.Decode as Decode exposing (Decoder)


type TouchEvent
    = TouchStart
    | TouchEnd


type alias Touch =
    { clientX : Float
    , clientY : Float
    }


onTouchEvent : TouchEvent -> (Touch -> msg) -> Attribute msg
onTouchEvent eventType msg =
    case eventType of
        TouchStart ->
            onTouchStart msg

        TouchEnd ->
            onTouchEnd msg


onTouchEnd : (Touch -> msg) -> Attribute msg
onTouchEnd msg =
    on "touchend" <| eventDecoder msg "changedTouches"


onTouchStart : (Touch -> msg) -> Attribute msg
onTouchStart msg =
    on "touchstart" <| eventDecoder msg "touches"


eventDecoder : (Touch -> msg) -> String -> Decoder msg
eventDecoder msg eventKey =
    Decode.at [ eventKey, "0" ] (Decode.map msg touchDecoder)


touchDecoder : Decoder Touch
touchDecoder =
    Decode.map2 Touch
        (Decode.field "clientX" Decode.float)
        (Decode.field "clientY" Decode.float)
