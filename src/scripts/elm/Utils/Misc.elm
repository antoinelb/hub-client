module Utils.Misc exposing
    ( focus
    , formatNumber
    , loseFocus
    , toCmd
    , toTitle
    )

import Browser.Dom as Dom
import Task


toCmd : msg -> Cmd msg
toCmd msg =
    Task.succeed msg |> Task.perform identity


loseFocus : msg -> String -> Cmd msg
loseFocus msg id =
    Task.attempt (\_ -> msg) (Dom.blur id)


focus : msg -> String -> Cmd msg
focus msg id =
    Task.attempt (\_ -> msg) (Dom.focus id)


toTitle : String -> String
toTitle text =
    let
        titleWord word =
            (String.left 1 word |> String.toUpper) ++ String.dropLeft 1 word
    in
    text
        |> String.split " "
        |> List.map titleWord
        |> String.join " "


formatNumber : Int -> Float -> String
formatNumber nDecimals number_ =
    let
        n =
            String.fromFloat number_

        split =
            String.split "." n

        integer =
            List.head split

        decimal =
            List.tail split
                |> Maybe.andThen List.head
    in
    case ( integer, decimal ) of
        ( Nothing, _ ) ->
            ""

        ( Just i, Nothing ) ->
            i ++ ".00"

        ( Just i, Just d ) ->
            i ++ "." ++ String.padRight nDecimals '0' d
