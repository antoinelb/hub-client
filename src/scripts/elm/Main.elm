module Main exposing (main)

import Auth
import Browser
import Browser.Navigation as Nav
import Config
import Hotkeys
import Html exposing (Html, div, h1, header, span, text)
import Html.Attributes exposing (class, href, id, title)
import Html.Events exposing (onClick)
import I18Next exposing (initialTranslations, translationsDecoder)
import Json.Decode as Decode
import Json.Encode as Encode
import Menu
import Menu.Language as Language
import Menu.Theme as Theme
import Page
import Page.Admin as Admin
import Routing
import Task
import Time
import Url exposing (Url)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (toCmd)



-- main


type alias Flags =
    { theme : String
    , language : String
    , translations : Encode.Value
    , refreshToken : Maybe String
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- model


type alias Error =
    { text : String
    , expiry : Time.Posix
    }


type alias Model =
    { key : Nav.Key
    , currentTime : Time.Posix
    , currentZone : Time.Zone
    , errors : List Error
    , lastMsg : Maybe Msg
    , auth : Auth.Model
    , menu : Menu.Model
    , page : Page.Model
    }


initModel : Flags -> Nav.Key -> Routing.Route -> Model
initModel { theme, language, translations, refreshToken } key route =
    let
        translations_ =
            case Decode.decodeValue translationsDecoder translations of
                Ok t ->
                    t

                Err _ ->
                    initialTranslations

        auth =
            Auth.initModel refreshToken

        page =
            case route of
                Routing.Auth _ ->
                    Page.defaultModel

                Routing.Page page_ ->
                    page_
    in
    { key = key
    , currentTime = Time.millisToPosix 0
    , currentZone = Time.utc
    , errors = []
    , lastMsg = Nothing
    , auth = auth
    , menu = Menu.initModel theme language translations_
    , page = page
    }



-- update


type Msg
    = NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | UpdateTime Time.Posix
    | UpdateZone Time.Zone
    | RemoveError Int
    | AuthMsg Auth.Msg
    | MenuMsg Menu.Msg
    | PageMsg Page.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( model_, cmd_, updateLastMsg ) =
            case msg of
                NoOp ->
                    ( model, Cmd.none, False )

                LinkClicked request ->
                    case request of
                        Browser.Internal url ->
                            ( model, Nav.pushUrl model.key (Url.toString url), False )

                        Browser.External href ->
                            ( model, Nav.load href, False )

                UrlChanged url ->
                    let
                        route =
                            Routing.toRoute url
                    in
                    case route of
                        Routing.Auth False ->
                            ( model
                            , Cmd.none
                            , False
                            )

                        Routing.Auth True ->
                            ( model
                            , Auth.signOut model.auth |> Cmd.map AuthMsg
                            , False
                            )

                        Routing.Page page ->
                            let
                                maybeTokens =
                                    Auth.getTokens model.auth

                                cmd =
                                    case maybeTokens of
                                        Just tokens ->
                                            Page.initCmd model.page page tokens
                                                |> Cmd.map PageMsg

                                        _ ->
                                            Cmd.none
                            in
                            ( { model | page = Page.initModel model.page page }
                            , cmd
                            , False
                            )

                UpdateTime time ->
                    ( { model | currentTime = time }, Cmd.none, False )

                UpdateZone zone ->
                    ( { model | currentZone = zone }, Cmd.none, False )

                RemoveError idx ->
                    let
                        errors =
                            model.errors
                                |> List.indexedMap Tuple.pair
                                |> List.filter (\error -> Tuple.first error /= idx)
                                |> List.map Tuple.second
                    in
                    ( { model | errors = errors }, Cmd.none, False )

                AuthMsg subMsg ->
                    let
                        result_ =
                            Auth.update subMsg model.auth

                        errors =
                            case result_.error of
                                Just err ->
                                    model.errors
                                        ++ [ toError err model.currentTime ]

                                Nothing ->
                                    case result_.model of
                                        Auth.Authenticated _ ->
                                            model.errors
                                                |> List.filter
                                                    (\error ->
                                                        error.text
                                                            |> String.startsWith "Your session has expired"
                                                            |> not
                                                    )

                                        _ ->
                                            model.errors

                        authCmd =
                            Cmd.map AuthMsg result_.cmd

                        ( resubmitCmd, lastMsg ) =
                            case model.lastMsg of
                                Just msg_ ->
                                    if result_.resubmit then
                                        ( toCmd msg_, Just msg )

                                    else
                                        ( Cmd.none, Just msg_ )

                                Nothing ->
                                    ( Cmd.none, Nothing )

                        routeCmd =
                            case result_.model of
                                Auth.Authenticated _ ->
                                    case model.auth of
                                        Auth.RequestingInfo _ Nothing ->
                                            model.page
                                                |> Page.validateModel result_.model
                                                |> Page.fromPage
                                                |> Nav.pushUrl model.key

                                        _ ->
                                            Cmd.none

                                _ ->
                                    Cmd.none
                    in
                    ( { model
                        | auth = result_.model
                        , errors = errors
                        , lastMsg = lastMsg
                      }
                    , Cmd.batch [ authCmd, resubmitCmd, routeCmd ]
                    , False
                    )

                MenuMsg subMsg ->
                    let
                        result_ =
                            Menu.update subMsg model.menu

                        errors =
                            case result_.error of
                                Just err ->
                                    model.errors
                                        ++ [ toError err model.currentTime ]

                                Nothing ->
                                    model.errors

                        cmd =
                            if result_.signOut then
                                Cmd.batch
                                    [ Cmd.map MenuMsg result_.cmd
                                    , Auth.signOut model.auth |> Cmd.map AuthMsg
                                    ]

                            else
                                Cmd.map MenuMsg result_.cmd
                    in
                    ( { model | menu = result_.model, errors = errors }
                    , cmd
                    , result_.updateLastMsg
                    )

                PageMsg subMsg ->
                    case model.auth of
                        Auth.Authenticated { tokens } ->
                            let
                                result_ =
                                    Page.update subMsg model.page tokens

                                errors =
                                    case result_.error of
                                        Just err ->
                                            model.errors
                                                ++ [ toError err model.currentTime ]

                                        Nothing ->
                                            model.errors
                            in
                            ( { model | page = result_.model, errors = errors }
                            , Cmd.map PageMsg result_.cmd
                            , result_.updateLastMsg
                            )

                        _ ->
                            ( model, Cmd.none, False )

        refreshAccessToken =
            model_.errors
                |> List.reverse
                |> List.head
                |> Maybe.map .text
                |> Maybe.map (\t -> t == "The token expired.")
                |> Maybe.withDefault False
    in
    if refreshAccessToken then
        case model.auth of
            Auth.Authenticated { user, tokens } ->
                ( { model_
                    | errors =
                        model_.errors
                            |> List.reverse
                            |> List.drop 1
                            |> List.reverse
                  }
                , Cmd.batch
                    [ cmd_
                    , Auth.refreshAccessToken tokens.refresh (Just user)
                        |> Cmd.map AuthMsg
                    ]
                )

            _ ->
                ( model_, cmd_ )

    else if updateLastMsg then
        case model.lastMsg of
            Just (AuthMsg _) ->
                ( { model_ | lastMsg = Nothing }, cmd_ )

            _ ->
                ( { model_ | lastMsg = Just msg }, cmd_ )

    else
        ( model_, cmd_ )


initCmd : Model -> Routing.Route -> Cmd Msg
initCmd model route =
    let
        cmd =
            case route of
                Routing.Auth False ->
                    [ Auth.initCmd model.auth |> Cmd.map AuthMsg ]

                Routing.Auth True ->
                    [ Auth.signOut model.auth |> Cmd.map AuthMsg ]

                Routing.Page _ ->
                    [ Auth.initCmd model.auth |> Cmd.map AuthMsg
                    , Nav.pushUrl model.key "/auth"
                    ]
    in
    List.append cmd
        [ Task.perform UpdateZone Time.here
        , Task.perform UpdateTime Time.now
        ]
        |> Cmd.batch



-- view


view : Model -> Browser.Document Msg
view model =
    let
        translate =
            Language.initTranslate model.menu.language

        class_ =
            case model.menu.theme of
                Theme.Dark ->
                    ""

                Theme.Light ->
                    "light"

        { title, body } =
            mainView translate model
    in
    { title = title
    , body =
        [ div [ id "root", class class_ ]
            (List.append [ headerView translate model ]
                body
            )
        ]
    }


headerView : (String -> String) -> Model -> Html Msg
headerView translate model =
    let
        signedIn =
            Auth.signedIn model.auth

        class_ =
            if signedIn then
                ""

            else
                "signed-out"

        adminButton =
            case Auth.getUser model.auth of
                Just user ->
                    if user.isAdmin then
                        case model.page of
                            Page.Admin _ ->
                                text ""

                            _ ->
                                Admin.adminButton translate

                    else
                        text ""

                Nothing ->
                    text ""
    in
    if signedIn then
        header [ class class_ ]
            [ errorView translate model.errors
            , h1 [] [ svgSprite "logo", span [] [ text "Hub" ] ]
            , adminButton
            , Menu.view translate model.menu signedIn |> Html.map MenuMsg
            ]

    else
        header [ class class_ ]
            [ errorView translate model.errors
            , h1 [] [ svgSprite "logo", span [] [ text "Hub" ] ]
            , Menu.view translate model.menu signedIn |> Html.map MenuMsg
            ]


mainView :
    (String -> String)
    -> Model
    -> { title : String, body : List (Html Msg) }
mainView translate model =
    if Auth.signedIn model.auth then
        pageView translate model.currentZone model.page

    else
        { title = "Auth | Hub"
        , body = [ Auth.view translate model.auth |> Html.map AuthMsg ]
        }


pageView :
    (String -> String)
    -> Time.Zone
    -> Page.Model
    -> { title : String, body : List (Html Msg) }
pageView translate zone page =
    let
        { title, body } =
            Page.view translate zone page
    in
    { title = title, body = List.map (Html.map PageMsg) body }


errorView : (String -> String) -> List Error -> Html Msg
errorView translate errors =
    div [ id "error" ]
        (List.indexedMap
            (\i error ->
                span [ onClick (RemoveError i) ]
                    [ error.text |> translate |> text ]
            )
            errors
            ++ [ span [] [] ]
        )



-- subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ updateHotkeys model
        , updateTime
        ]


updateTime : Sub Msg
updateTime =
    if Config.debug then
        Sub.none

    else
        Time.every 1000 UpdateTime


updateHotkeys : Model -> Sub Msg
updateHotkeys model =
    let
        hotkeys =
            List.concat
                [ Menu.hotkeys model.menu |> Hotkeys.map MenuMsg
                ]
    in
    Hotkeys.createEvent hotkeys



-- utils


toError : String -> Time.Posix -> Error
toError text_ currentTime =
    { text = text_
    , expiry =
        currentTime
            |> Time.posixToMillis
            |> (\t -> t + (Config.errorExpiry * 1000))
            |> Time.millisToPosix
    }



-- init


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        route =
            Routing.toRoute url

        model =
            initModel flags key route
    in
    ( model, initCmd model route )
