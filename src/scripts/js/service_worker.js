const cacheName = "hub";
const cacheFiles = [
  "/",
  "/index.html",
  "/scripts.js",
  "/styles.css",
  "/manifest.json",
  "/translations.json",
  "/fonts/parisienne-regular.ttf",
  "/icons/icons.svg",
  "/icons/favicon.svg",
  "/icons/loading.svg",
];

self.addEventListener("install", e => {
  console.log("[Service Worker] Installation");
  e.waitUntil(
    (async () => {
      const cache = await caches.open(cacheName);
      console.log("[Service Worker] Caching all: app shell and content");
      await cache.addAll(cacheFiles);
    })(),
  );
});
