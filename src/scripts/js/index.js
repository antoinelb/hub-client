"use strict";

/*********/
/* model */
/*********/

let model;

async function initModel() {
  const flags = getSavedData();
  flags.translations = await getTranslations();
  model = {
    app: Elm.Main.init({
      flags: flags,
    }),
  };

  setPorts();
}

function getSavedData() {
  const theme = localStorage.getItem("theme") || "dark";
  const language = localStorage.getItem("language") || "en";
  const refreshToken = localStorage.getItem("refresh-token", null);
  return { theme: theme, language: language, refreshToken: refreshToken };
}

const getTranslations = async () => {
  const resp = await fetch("/translations.json", { cache: "no-cache" });
  if (resp.ok) {
    return await resp.json();
  } else {
    console.error("Translations file could not be loaded.");
    return {};
  }
};

function setPorts() {
  model.app.ports.saveTheme.subscribe(theme => {
    localStorage.setItem("theme", theme);
    document.body.style.setProperty("--bg", `var(--bg-${theme})`);
  });
  model.app.ports.saveLanguage.subscribe(language => {
    localStorage.setItem("language", language);
  });
  model.app.ports.saveRefreshToken.subscribe(token => {
    if (token) {
      localStorage.setItem("refresh-token", token);
    } else {
      localStorage.removeItem("refresh-token");
    }
  });
}

/********/
/* view */
/********/

async function view() {
  await injectSvgSprite();
}

async function injectSvgSprite() {
  if (!document.getElementById("svg-sprite")) {
    const resp = await fetch("/icons/icons.svg?hash={{ icons_hash }}");
    const sprite = await resp.text();
    document.body.insertAdjacentHTML("beforebegin", sprite);
  }
}

/********/
/* init */
/********/

async function init() {
  await view();
  await initModel();
}

window.onload = async () => init();
