module Main exposing (main)

import Auth
import Browser
import Browser.Navigation as Nav
import Config exposing (apis)
import Html exposing (Html, a, button, div, h1, header, span, text)
import Html.Attributes exposing (class, href, id)
import Html.Events exposing (onClick)
import I18Next exposing (initialTranslations, translationsDecoder)
import Json.Decode as Decode
import Json.Encode as Encode
import Page
import Routing
import Settings
import Settings.Language as Language
import Settings.Theme as Theme
import Task
import Time
import Url exposing (Url)
import Utils.Http exposing (DetailedError, expectWhatever, get, handleError)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (toCmd)



-- main


type alias Flags =
    { theme : String
    , language : String
    , translations : Encode.Value
    , refreshToken : Maybe String
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked >> InternalMsg
        , onUrlChange = UrlChanged >> InternalMsg
        }



-- model


type alias Error =
    { text : String
    , expiry : Time.Posix
    }


type alias Model =
    { key : Nav.Key
    , errors : List Error
    , lastMsg : Maybe Msg
    , showDebugList : Bool
    , currentTime : Time.Posix
    , timeZone : Time.Zone
    , settings : Settings.Model
    , auth : Auth.Model
    , page : Page.Model
    }


initModel : Flags -> Nav.Key -> Routing.Route -> Model
initModel { theme, language, translations, refreshToken } key route =
    let
        translations_ =
            case Decode.decodeValue translationsDecoder translations of
                Ok t ->
                    t

                Err _ ->
                    initialTranslations

        page =
            case route of
                Routing.Page page_ ->
                    page_

                _ ->
                    Page.Home
    in
    { key = key
    , errors = []
    , lastMsg = Nothing
    , showDebugList = False
    , currentTime = Time.millisToPosix 0
    , timeZone = Time.utc
    , settings = Settings.initModel theme language translations_
    , auth = Auth.initModel refreshToken
    , page = page
    }



-- update


type InternalMsg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | UpdateTime Time.Posix
    | UpdateTimeZone Time.Zone
    | RemoveError Int


type ExternalMsg
    = SettingsMsg Settings.Msg
    | AuthMsg Auth.Msg
    | PageMsg Page.Msg


type DebugMsg
    = ToggleDebug
    | CheckAuthenticated
    | AuthenticatedCheckReceived (Result DetailedError ())


type Msg
    = InternalMsg InternalMsg
    | ExternalMsg ExternalMsg
    | DebugMsg DebugMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( model_, cmd_, refreshLastMsg ) =
            case msg of
                InternalMsg subMsg ->
                    let
                        ( m, c ) =
                            updateInternal subMsg model
                    in
                    ( m, c, False )

                ExternalMsg subMsg ->
                    updateExternal subMsg model

                DebugMsg subMsg ->
                    updateDebug subMsg model

        refreshAccessToken =
            model_.errors
                |> List.reverse
                |> List.head
                |> Maybe.map .text
                |> Maybe.map (\t -> t == "The token expired.")
                |> Maybe.withDefault False
    in
    if refreshAccessToken then
        case model.auth of
            Auth.Authenticated { user, tokens } ->
                ( { model_
                    | errors =
                        model_.errors
                            |> List.reverse
                            |> List.drop 1
                            |> List.reverse
                  }
                , Cmd.batch
                    [ cmd_
                    , Auth.refreshAccessToken tokens.refresh (Just user)
                        |> Cmd.map (AuthMsg >> ExternalMsg)
                    ]
                )

            _ ->
                ( model_, cmd_ )

    else if refreshLastMsg then
        case model.lastMsg of
            Just (ExternalMsg (AuthMsg _)) ->
                ( { model_ | lastMsg = Nothing }, cmd_ )

            _ ->
                ( { model_ | lastMsg = Just msg }, cmd_ )

    else
        ( model_, cmd_ )


updateInternal : InternalMsg -> Model -> ( Model, Cmd Msg )
updateInternal msg model =
    case msg of
        LinkClicked request ->
            case request of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                route =
                    Routing.toRoute url
            in
            case route of
                Routing.SignOut ->
                    case model.auth of
                        Auth.Authenticated { tokens } ->
                            ( model
                            , Auth.signOut tokens.refresh
                                |> Cmd.map (AuthMsg >> ExternalMsg)
                            )

                        _ ->
                            ( model, Cmd.none )

                Routing.Page page ->
                    let
                        cmd =
                            case model.auth of
                                Auth.Authenticated { tokens } ->
                                    Page.initCmd model.page page tokens
                                        |> Cmd.map (PageMsg >> ExternalMsg)

                                _ ->
                                    Cmd.none
                    in
                    ( { model | page = Page.initModel model.page page }
                    , cmd
                    )

                Routing.Auth ->
                    ( model, Cmd.none )

        UpdateTime time ->
            let
                errors =
                    model.errors
                        |> List.filter
                            (\error ->
                                Time.posixToMillis error.expiry
                                    > Time.posixToMillis time
                            )
            in
            ( { model | currentTime = time, errors = errors }, Cmd.none )

        UpdateTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )

        RemoveError idx ->
            let
                errors =
                    model.errors
                        |> List.indexedMap Tuple.pair
                        |> List.filter (\error -> Tuple.first error /= idx)
                        |> List.map Tuple.second
            in
            ( { model | errors = errors }, Cmd.none )


updateExternal : ExternalMsg -> Model -> ( Model, Cmd Msg, Bool )
updateExternal msg model =
    case msg of
        SettingsMsg subMsg ->
            let
                result_ =
                    Settings.update subMsg model.settings

                errors =
                    case result_.error of
                        Just err ->
                            model.errors
                                ++ [ toError err model.currentTime ]

                        Nothing ->
                            model.errors
            in
            ( { model | settings = result_.model, errors = errors }
            , Cmd.map (SettingsMsg >> ExternalMsg) result_.cmd
            , result_.updateLastMsg
            )

        AuthMsg subMsg ->
            let
                result_ =
                    Auth.update subMsg model.auth model.key

                errors =
                    case result_.error of
                        Just err ->
                            model.errors
                                ++ [ toError err model.currentTime ]

                        Nothing ->
                            model.errors

                authCmd =
                    Cmd.map (AuthMsg >> ExternalMsg) result_.cmd

                pageCmd =
                    case result_.model of
                        Auth.Authenticated { tokens } ->
                            case model.auth of
                                Auth.RequestingInfo _ Nothing ->
                                    model.page
                                        |> Page.fromPage
                                        |> Nav.pushUrl model.key

                                _ ->
                                    Cmd.none

                        _ ->
                            Cmd.none

                ( resubmitCmd, lastMsg ) =
                    case model.lastMsg of
                        Just msg_ ->
                            if result_.resubmit then
                                ( toCmd msg_, Just (ExternalMsg msg) )

                            else
                                ( Cmd.none, Just msg_ )

                        Nothing ->
                            ( Cmd.none, Nothing )
            in
            ( { model | auth = result_.model, errors = errors, lastMsg = lastMsg }
            , Cmd.batch [ authCmd, pageCmd, resubmitCmd ]
            , False
            )

        PageMsg subMsg ->
            case model.auth of
                Auth.Authenticated { tokens } ->
                    let
                        result_ =
                            Page.update subMsg model.page tokens

                        errors =
                            case result_.error of
                                Just err ->
                                    model.errors
                                        ++ [ toError err model.currentTime ]

                                Nothing ->
                                    model.errors
                    in
                    ( { model | page = result_.model, errors = errors }
                    , Cmd.map (PageMsg >> ExternalMsg) result_.cmd
                    , result_.updateLastMsg
                    )

                _ ->
                    ( model, Cmd.none, False )


updateDebug : DebugMsg -> Model -> ( Model, Cmd Msg, Bool )
updateDebug msg model =
    case msg of
        ToggleDebug ->
            ( { model | showDebugList = not model.showDebugList }
            , Cmd.none
            , False
            )

        CheckAuthenticated ->
            let
                cmd =
                    case model.auth of
                        Auth.Authenticated { user, tokens } ->
                            get tokens
                                { url = apis.auth ++ "/authenticated"
                                , expect = expectWhatever (AuthenticatedCheckReceived >> DebugMsg)
                                }

                        _ ->
                            Cmd.none
            in
            ( model
            , cmd
            , True
            )

        AuthenticatedCheckReceived result ->
            case result of
                Ok _ ->
                    ( model, Cmd.none, False )

                Err err ->
                    let
                        error =
                            handleError err "There was an error accessing the server."

                        errors =
                            model.errors
                                ++ [ toError error model.currentTime ]
                    in
                    ( { model
                        | errors = errors
                      }
                    , Cmd.none
                    , False
                    )


initInternalCmd : Cmd InternalMsg
initInternalCmd =
    Cmd.batch
        [ Task.perform UpdateTimeZone Time.here
        , Task.perform UpdateTime Time.now
        ]


initExternalCmd : Model -> Routing.Route -> Cmd ExternalMsg
initExternalCmd model route =
    case route of
        Routing.SignOut ->
            case model.auth of
                Auth.RequestingAccess token _ ->
                    Auth.signOut token |> Cmd.map AuthMsg

                _ ->
                    Nav.pushUrl model.key "/"

        Routing.Auth ->
            Auth.initCmd model.auth |> Cmd.map AuthMsg

        Routing.Page _ ->
            Cmd.batch
                [ Auth.initCmd model.auth |> Cmd.map AuthMsg
                , Nav.pushUrl model.key "/auth"
                ]


initCmd : Model -> Routing.Route -> Cmd Msg
initCmd model route =
    Cmd.batch
        [ Cmd.map InternalMsg initInternalCmd
        , Cmd.map ExternalMsg (initExternalCmd model route)
        ]



-- view


view : Model -> Browser.Document Msg
view model =
    let
        translate =
            Language.initTranslate model.settings.language

        class_ =
            case model.settings.theme of
                Theme.Dark ->
                    ""

                Theme.Light ->
                    "light"

        ( title, body ) =
            case model.auth of
                Auth.Authenticated { user } ->
                    Page.view translate model.page user
                        |> (\( t, b ) ->
                                ( t
                                , List.map (Html.map (PageMsg >> ExternalMsg)) b
                                )
                           )

                Auth.RequestingAccess _ (Just user) ->
                    Page.view translate model.page user
                        |> (\( t, b ) ->
                                ( t
                                , List.map (Html.map (PageMsg >> ExternalMsg)) b
                                )
                           )

                Auth.RequestingInfo _ (Just user) ->
                    Page.view translate model.page user
                        |> (\( t, b ) ->
                                ( t
                                , List.map (Html.map (PageMsg >> ExternalMsg)) b
                                )
                           )

                _ ->
                    ( "Auth | Hub"
                    , [ Auth.view translate model.auth
                            |> Html.map (AuthMsg >> ExternalMsg)
                      ]
                    )
    in
    { title = title
    , body =
        [ div [ id "root", class class_ ]
            (List.append
                [ errorView translate model.errors
                , headerView translate model
                ]
                body
            )
        ]
    }


errorView : (String -> String) -> List Error -> Html Msg
errorView translate errors =
    div [ id "error" ]
        (List.indexedMap
            (\i error ->
                span [ onClick (InternalMsg (RemoveError i)) ]
                    [ error.text |> translate |> text ]
            )
            errors
            ++ [ span [] [] ]
        )


headerView : (String -> String) -> Model -> Html Msg
headerView translate model =
    let
        signedIn =
            case model.auth of
                Auth.Authenticated _ ->
                    True

                _ ->
                    False
    in
    header [ id "header" ]
        [ a [ href "/" ] [ h1 [] [ "Hub" |> translate |> text ] ]
        , debugView translate model.showDebugList
        , headerButtonsView translate model
        , Settings.view translate model.settings signedIn |> Html.map (SettingsMsg >> ExternalMsg)
        ]


debugView : (String -> String) -> Bool -> Html Msg
debugView translate showDebugList =
    if Config.debug then
        let
            class_ =
                if showDebugList then
                    "debug--open"

                else
                    ""
        in
        div [ id "debug", class class_ ]
            [ button [ onClick (DebugMsg ToggleDebug) ]
                [ svgSprite "bug" ]
            , div
                []
                [ div [ class "bg" ] []
                , button [ onClick (DebugMsg CheckAuthenticated) ]
                    [ "Refresh info" |> translate |> text ]
                ]
            ]

    else
        text ""


headerButtonsView : (String -> String) -> Model -> Html msg
headerButtonsView translate model =
    let
        adminButton =
            text ""
    in
    div [ id "header-buttons" ] [ adminButton ]



-- subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [ updateTime ]


updateTime : Sub Msg
updateTime =
    if Config.debug then
        Sub.none

    else
        Time.every 1000 (UpdateTime >> InternalMsg)



-- utils


toError : String -> Time.Posix -> Error
toError text_ currentTime =
    { text = text_
    , expiry =
        currentTime
            |> Time.posixToMillis
            |> (\t -> t + (Config.errorExpiry * 1000))
            |> Time.millisToPosix
    }



-- init


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        route =
            Routing.toRoute url

        model =
            initModel flags key route
    in
    ( model, initCmd model route )
