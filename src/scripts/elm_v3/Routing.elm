module Routing exposing (Route(..), fromRoute, toRoute)

import Page
import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, top)
import Url.Parser.Query as Query


type Route
    = SignOut
    | Auth
    | Page Page.Model


routeParser : Parser (Route -> a) a
routeParser =
    List.concat
        [ [ map SignOut (s "signout")
          , map Auth (s "auth")
          ]
        , [ map Page Page.routeParser ]
        ]
        |> oneOf


toRoute : Url -> Route
toRoute url =
    Maybe.withDefault (Page Page.Home) (parse routeParser url)


fromRoute : Route -> String
fromRoute route =
    case route of
        SignOut ->
            "/signout"

        Auth ->
            "/auth"

        Page page ->
            Page.fromPage page
