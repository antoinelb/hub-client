module Page exposing
    ( Model(..)
    , Msg
    , fromPage
    , initCmd
    , initModel
    , routeParser
    , update
    , view
    )

import Auth exposing (User)
import Html exposing (Html, a, div, nav, span, text)
import Html.Attributes exposing (class, href, id)
import Page.Admin as Admin
import Page.Budget as Budget
import Page.Home as Home
import Page.Schedule as Schedule
import Url.Parser as Parser exposing (Parser)
import Utils.Http exposing (Tokens)
import Utils.Icons exposing (svgSprite)



-- model


type Model
    = Home
    | Admin Admin.Model
    | Budget Budget.Model
    | Schedule Schedule.Model


initModel : Model -> Model -> Model
initModel oldModel newModel =
    case ( oldModel, newModel ) of
        ( Home, Home ) ->
            Home

        ( Budget model, Budget { page } ) ->
            Budget { model | page = page }

        ( Schedule model, Schedule { page } ) ->
            Schedule { model | page = page }

        ( _, _ ) ->
            newModel



-- update


type Msg
    = NoOp
    | AdminMsg Admin.Msg
    | BudgetMsg Budget.Msg
    | ScheduleMsg Schedule.Msg


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case ( msg, model ) of
        ( NoOp, _ ) ->
            return

        ( AdminMsg subMsg, Admin subModel ) ->
            let
                return_ =
                    Admin.update subMsg subModel tokens
            in
            { return
                | model = Admin return_.model
                , cmd = Cmd.map AdminMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( BudgetMsg subMsg, Budget subModel ) ->
            let
                return_ =
                    Budget.update subMsg subModel tokens
            in
            { return
                | model = Budget return_.model
                , cmd = Cmd.map BudgetMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( ScheduleMsg subMsg, Schedule subModel ) ->
            let
                return_ =
                    Schedule.update subMsg subModel tokens
            in
            { return
                | model = Schedule return_.model
                , cmd = Cmd.map ScheduleMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( _, _ ) ->
            return


initCmd : Model -> Model -> Tokens -> Cmd Msg
initCmd oldModel newModel tokens =
    case newModel of
        Home ->
            Cmd.none

        Admin model ->
            Admin.initCmd model tokens
                |> Cmd.map AdminMsg

        Budget model ->
            case oldModel of
                Budget model_ ->
                    Budget.initCmd (Just model_) model tokens
                        |> Cmd.map BudgetMsg

                _ ->
                    Budget.initCmd Nothing model tokens
                        |> Cmd.map BudgetMsg

        Schedule model ->
            case oldModel of
                Schedule model_ ->
                    Schedule.initCmd (Just model_) model tokens
                        |> Cmd.map ScheduleMsg

                _ ->
                    Schedule.initCmd Nothing model tokens
                        |> Cmd.map ScheduleMsg



-- view


view : (String -> String) -> Model -> User -> ( String, List (Html Msg) )
view translate model user =
    let
        ( title, body ) =
            case model of
                Home ->
                    ( "Hub", Home.view translate )

                Admin subModel ->
                    ( "Admin | Hub"
                    , Admin.view translate subModel
                        |> Html.map AdminMsg
                    )

                Budget subModel ->
                    ( "Budget | Hub"
                    , Budget.view translate subModel
                        |> Html.map BudgetMsg
                    )

                Schedule subModel ->
                    ( "Schedule | Hub"
                    , Schedule.view translate subModel
                        |> Html.map ScheduleMsg
                    )
    in
    ( title
    , [ navView translate model user
      , body
      ]
    )


navView : (String -> String) -> Model -> User -> Html msg
navView translate model user =
    let
        homeSection =
            case model of
                Home ->
                    pageSection translate Home.navItems (Just "")

                _ ->
                    pageSection translate Home.navItems Nothing

        adminSection =
            if user.isAdmin then
                case model of
                    Admin subModel ->
                        pageSection translate
                            Admin.navItems
                            (subModel |> Admin.fromPage |> Just)

                    _ ->
                        pageSection translate Admin.navItems Nothing

            else
                text ""

        budgetSection =
            case model of
                Budget subModel ->
                    pageSection translate
                        Budget.navItems
                        (subModel.page |> Budget.fromPage |> Just)

                _ ->
                    pageSection translate Budget.navItems Nothing

        scheduleSection =
            case model of
                Schedule subModel ->
                    pageSection translate
                        Schedule.navItems
                        (subModel.page |> Schedule.fromPage |> Just)

                _ ->
                    pageSection translate Schedule.navItems Nothing
    in
    nav [ id "nav" ]
        [ homeSection
        , budgetSection
        , scheduleSection
        , adminSection
        ]


pageSection :
    (String -> String)
    ->
        { icon : String
        , title : { text : String, link : String }
        , items : List { text : String, link : String }
        }
    -> Maybe String
    -> Html msg
pageSection translate { icon, title, items } selected =
    let
        toItem item =
            case selected of
                Just link ->
                    if link == item.link then
                        a [ href item.link, class "nav__section__selected-item" ]
                            [ item.text |> translate |> text ]

                    else
                        a [ href item.link ]
                            [ item.text |> translate |> text ]

                Nothing ->
                    a [ href item.link ]
                        [ item.text |> translate |> text ]

        class_ =
            case selected of
                Just _ ->
                    "nav__section nav__section--selected"

                Nothing ->
                    "nav__section"
    in
    div [ class class_ ]
        [ a [ href title.link, class "nav__section__title" ]
            [ svgSprite icon, span [] [ title.text |> translate |> text ] ]
        , div [ class "nav__section__items" ] (List.map toItem items)
        ]



-- routing


routeParser : Parser (Model -> a) a
routeParser =
    List.concat
        [ [ Parser.map Home Parser.top ]
        , [ Parser.map (Budget << Budget.initModel) Budget.routeParser ]
        , [ Parser.map (Schedule << Schedule.initModel) Schedule.routeParser ]
        , [ Parser.map Admin Admin.routeParser ]
        ]
        |> Parser.oneOf


fromPage : Model -> String
fromPage page =
    case page of
        Home ->
            "/"

        Admin subModel ->
            Admin.fromPage subModel

        Budget subModel ->
            Budget.fromPage subModel.page

        Schedule subModel ->
            Schedule.fromPage subModel.page
