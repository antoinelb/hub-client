module Utils.Events exposing
    ( onBackspace
    , onCtrlEnter
    , onEnter
    , onEscape
    , onKeyDown
    , onStopPropagationKeyDown
    , stopPropagationOnClick
    )

import Html exposing (Attribute)
import Html.Events exposing (on, stopPropagationOn)
import Json.Decode as Decode exposing (Decoder)


stopPropagationOnClick : msg -> Attribute msg
stopPropagationOnClick msg =
    let
        alwaysStopPropagation =
            \msg_ -> ( msg_, True )
    in
    stopPropagationOn "click" (Decode.map alwaysStopPropagation (Decode.succeed msg))


keyInfo : Decoder { key : String, ctrlKey : Bool }
keyInfo =
    Decode.map2 (\key ctrlKey -> { key = key, ctrlKey = ctrlKey })
        (Decode.field "key" Decode.string)
        (Decode.field "ctrlKey" Decode.bool)


onEnter : msg -> Attribute msg
onEnter msg_ =
    let
        isKey { key } =
            if key == "Enter" then
                Decode.succeed msg_

            else
                Decode.fail ""
    in
    on "keydown" (Decode.andThen isKey keyInfo)


onCtrlEnter : msg -> Attribute msg
onCtrlEnter msg_ =
    let
        isKey { key, ctrlKey } =
            if key == "Enter" && ctrlKey then
                Decode.succeed msg_

            else
                Decode.fail ""
    in
    on "keydown" (Decode.andThen isKey keyInfo)


onBackspace : msg -> Attribute msg
onBackspace msg_ =
    let
        isKey { key } =
            if key == "Backspace" then
                Decode.succeed msg_

            else
                Decode.fail ""
    in
    on "keydown" (Decode.andThen isKey keyInfo)


onEscape : msg -> Attribute msg
onEscape msg_ =
    let
        isKey { key } =
            if key == "Escape" then
                Decode.succeed msg_

            else
                Decode.fail ""
    in
    on "keydown" (Decode.andThen isKey keyInfo)


onKeyDown : ({ key : String, ctrlKey : Bool } -> msg) -> Attribute msg
onKeyDown msg_ =
    on "keydown" (Decode.map msg_ keyInfo)


onStopPropagationKeyDown : ({ key : String, ctrlKey : Bool } -> msg) -> Attribute msg
onStopPropagationKeyDown msg_ =
    let
        alwaysStopPropagation =
            \m -> ( m, False )
    in
    stopPropagationOn "keydown" (Decode.map alwaysStopPropagation (Decode.map msg_ keyInfo))
