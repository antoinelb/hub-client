module Page.Budget exposing
    ( Model
    , Msg
    , fromPage
    , initCmd
    , initModel
    , navItems
    , routeParser
    , update
    , view
    )

import Html exposing (Html, text)
import Page.Links exposing (Link)
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Http exposing (Tokens)
import Utils.Icons exposing (svgSprite)



-- model


type Page
    = Accounts
    | Transactions


type alias Model =
    { page : Page }


initModel : Page -> Model
initModel page =
    { page = page }



-- update


type Msg
    = NoOp


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case msg of
        NoOp ->
            return


initCmd : Maybe Model -> Model -> Tokens -> Cmd Msg
initCmd oldModel newModel tokens =
    case oldModel of
        Just _ ->
            Cmd.none

        Nothing ->
            Cmd.none



-- view


view : (String -> String) -> Model -> Html msg
view translate model =
    "Budget" |> translate |> text


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "dollar-sign"
    , title = { text = "Budget", link = "/budget" }
    , items =
        [ { text = "Accounts", link = "/budget" }
        , { text = "Transactions", link = "/budget/transactions" }
        ]
    }



-- routing


routeParser : Parser (Page -> a) a
routeParser =
    Parser.s "budget"
        </> Parser.oneOf
                [ Parser.map Accounts Parser.top
                , Parser.map Transactions (Parser.s "transactions")
                ]


fromPage : Page -> String
fromPage page =
    let
        url =
            case page of
                Accounts ->
                    ""

                Transactions ->
                    "/transactions"
    in
    "/budget" ++ url
