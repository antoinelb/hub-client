module Page.Home exposing (navItems, view)

import Html exposing (Html, a, main_, text)
import Html.Attributes exposing (href, id)
import Page.Links exposing (Link)
import Utils.Icons exposing (svgSprite)



-- view


view : (String -> String) -> Html msg
view translate =
    "Home" |> translate |> text


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "activity"
    , title = { text = "Home", link = "/" }
    , items = []
    }
