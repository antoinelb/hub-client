module Page.Admin.Auth exposing (Model, Msg, initCmd, initModel, update, view)

import Auth exposing (User, initUser, userDecoder)
import Config exposing (apis)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , h2
        , input
        , section
        , span
        , text
        )
import Html.Attributes exposing (hidden, id, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode
import Json.Encode as Encode
import Regex
import Utils.Elements exposing (checkbox)
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , expectJson
        , expectWhatever
        , get
        , handleError
        , post
        )
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (focus)
import Utils.Table as Table



-- model


type Editing
    = Adding
    | Updating String


type alias Model =
    { users : Maybe (List User)
    , tableState :
        Table.Model
            { email : String
            , isAdmin : String
            }
            Msg
    , editing : Maybe ( Editing, User, Bool )
    , deleting : Maybe User
    }


initModel : Model
initModel =
    { users = Nothing
    , tableState =
        Table.initModel
            [ ( "Email", .email, Just (ToggleEdit << Just) )
            , ( "Admin", .isAdmin, Nothing )
            ]
            .email
            False
            .email
    , editing = Nothing
    , deleting = Nothing
    }



-- update


type Msg
    = NoOp
    | TableMsg (Table.Msg Msg)
    | ReceivedUsers (Result DetailedError (List User))
    | ReceivedUser String (Result DetailedError User)
    | ToggleEdit (Maybe String)
    | Add
    | Update
    | UpdateEmail String
    | UpdateIsAdmin Bool
    | ToggleDelete
    | Delete
    | ReceivedDeleteConfirm String (Result DetailedError ())


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case msg of
        NoOp ->
            return

        TableMsg subMsg ->
            case subMsg of
                Table.ExternalMsg msg_ ->
                    update msg_ model tokens

                _ ->
                    let
                        tableState =
                            Table.update subMsg model.tableState
                    in
                    { return | model = { model | tableState = tableState } }

        ReceivedUsers result ->
            case result of
                Ok users ->
                    { return
                        | model = { model | users = Just users }
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error retrieving the users."
                    in
                    { return | error = Just error }

        ReceivedUser key result ->
            case result of
                Ok user ->
                    let
                        users =
                            model.users
                                |> Maybe.withDefault []
                                |> List.filter (\user_ -> user_.email /= key)
                                |> List.append [ user ]
                    in
                    { return
                        | model = { model | users = Just users, editing = Nothing }
                        , error = Nothing
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error retrieving the user."
                    in
                    { return | error = Just error }

        ToggleEdit maybeKey ->
            case maybeKey of
                Just key ->
                    case model.editing of
                        Just ( _, user_, _ ) ->
                            if key == user_.email then
                                { return
                                    | model =
                                        { model | editing = Nothing }
                                }

                            else
                                let
                                    user =
                                        model.users
                                            |> Maybe.withDefault []
                                            |> List.filter (\d -> d.email == key)
                                            |> List.head
                                            |> Maybe.withDefault initUser
                                in
                                { return
                                    | model =
                                        { model
                                            | editing =
                                                Just
                                                    ( Updating user.email, user, False )
                                        }
                                    , cmd = focus NoOp "user__email"
                                }

                        Nothing ->
                            let
                                user =
                                    model.users
                                        |> Maybe.withDefault []
                                        |> List.filter (\d -> d.email == key)
                                        |> List.head
                                        |> Maybe.withDefault initUser
                            in
                            { return
                                | model =
                                    { model
                                        | editing =
                                            Just
                                                ( Updating user.email, user, False )
                                    }
                                , cmd = focus NoOp "user__email"
                            }

                Nothing ->
                    case model.editing of
                        Just _ ->
                            { return
                                | model =
                                    { model | editing = Nothing }
                            }

                        Nothing ->
                            { return
                                | model =
                                    { model
                                        | editing =
                                            Just
                                                ( Adding, initUser, False )
                                    }
                                , cmd = focus NoOp "user__email"
                            }

        Add ->
            case model.editing of
                Just ( Adding, user, _ ) ->
                    if validateUser user (Maybe.withDefault [] model.users) then
                        { return
                            | cmd = addUser tokens user
                            , updateLastMsg = True
                        }

                    else
                        { return
                            | model =
                                { model | editing = Just ( Adding, user, True ) }
                        }

                _ ->
                    return

        Update ->
            case model.editing of
                Just ( Updating key, user, _ ) ->
                    if user.email == key then
                        { return | model = { model | editing = Nothing } }

                    else if validateUser user (Maybe.withDefault [] model.users) then
                        { return
                            | cmd = updateUser tokens key user
                            , updateLastMsg = True
                        }

                    else
                        { return | model = { model | editing = Just ( Updating key, user, True ) } }

                _ ->
                    return

        UpdateEmail email ->
            case model.editing of
                Just ( mode, user, showErrors ) ->
                    { return
                        | model =
                            { model
                                | editing =
                                    Just ( mode, { user | email = email }, showErrors )
                            }
                    }

                Nothing ->
                    return

        UpdateIsAdmin isAdmin ->
            case model.editing of
                Just ( mode, user, showErrors ) ->
                    { return
                        | model =
                            { model
                                | editing =
                                    Just ( mode, { user | isAdmin = isAdmin }, showErrors )
                            }
                    }

                Nothing ->
                    return

        ToggleDelete ->
            case ( model.deleting, model.editing ) of
                ( Nothing, Just ( Updating _, user, _ ) ) ->
                    { return
                        | model = { model | deleting = Just user, editing = Nothing }
                    }

                ( Just user, _ ) ->
                    { return
                        | model =
                            { model
                                | deleting = Nothing
                                , editing = Just ( Updating user.email, user, False )
                            }
                    }

                _ ->
                    return

        Delete ->
            case model.deleting of
                Just user ->
                    { return
                        | cmd = deleteUser tokens user
                        , updateLastMsg = True
                    }

                _ ->
                    return

        ReceivedDeleteConfirm email result ->
            case result of
                Ok _ ->
                    let
                        users =
                            model.users
                                |> Maybe.withDefault []
                                |> List.filter (\c -> c.email /= email)
                    in
                    { return
                        | model = { model | users = Just users, deleting = Nothing }
                    }

                Err err ->
                    let
                        error =
                            handleError
                                err
                                "There was an error deleting the user."
                    in
                    { return | error = Just error }


getUsers : Tokens -> Cmd Msg
getUsers tokens =
    get tokens
        { url = apis.auth ++ "/admin/users/"
        , expect = expectJson ReceivedUsers (Decode.list userDecoder)
        }


validateUser : User -> List User -> Bool
validateUser user users =
    validateEmail user.email users


validateEmail : String -> List User -> Bool
validateEmail email users =
    let
        email_ =
            String.trim email
    in
    if String.isEmpty email_ then
        False

    else if List.any (\u -> u.email == email_) users then
        False

    else
        let
            regex =
                Regex.fromString "^.+@\\w+\\.\\w+$"
                    |> Maybe.withDefault Regex.never
        in
        Regex.contains regex email


addUser : Tokens -> User -> Cmd Msg
addUser tokens user =
    let
        body =
            Encode.object
                [ ( "email", Encode.string (String.trim user.email) )
                , ( "is_admin", Encode.bool user.isAdmin )
                ]
    in
    post tokens
        { url = apis.auth ++ "/admin/users/create"
        , body = body
        , expect = expectJson (ReceivedUser user.email) userDecoder
        }


updateUser : Tokens -> String -> User -> Cmd Msg
updateUser tokens key user =
    let
        body =
            Encode.object
                [ ( "email", Encode.string key )
                , ( "is_admin", Encode.bool user.isAdmin )
                ]
    in
    post tokens
        { url = apis.auth ++ "/admin/users/update"
        , body = body
        , expect = expectJson (ReceivedUser key) userDecoder
        }


deleteUser : Tokens -> User -> Cmd Msg
deleteUser tokens user =
    let
        body =
            Encode.object
                [ ( "email", Encode.string user.email )
                ]
    in
    post tokens
        { url = apis.auth ++ "/admin/users/delete"
        , body = body
        , expect = expectWhatever (ReceivedDeleteConfirm user.email)
        }


initCmd : Tokens -> Cmd Msg
initCmd tokens =
    getUsers tokens



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        table users =
            let
                users_ =
                    case model.editing of
                        Just ( mode, user, _ ) ->
                            case mode of
                                Adding ->
                                    List.filter
                                        (\user_ -> user_.email /= user.email)
                                        users

                                Updating key ->
                                    List.filter
                                        (\user_ -> user_.email /= key)
                                        users

                        Nothing ->
                            users

                toUserData user =
                    { email = user.email
                    , isAdmin =
                        if user.isAdmin then
                            "✓"

                        else
                            ""
                    }

                userData =
                    List.map toUserData users_
            in
            Table.view translate model.tableState userData
                |> Html.map TableMsg

        editView_ =
            case model.editing of
                Just ( mode, user, _ ) ->
                    editView mode user

                Nothing ->
                    text ""

        buttons =
            let
                buttons_ =
                    case model.deleting of
                        Just _ ->
                            []

                        Nothing ->
                            case model.editing of
                                Just ( mode, _, _ ) ->
                                    case mode of
                                        Adding ->
                                            [ button [ onClick Add ]
                                                [ "Add" |> translate |> text ]
                                            , button [ onClick (ToggleEdit Nothing) ]
                                                [ "Cancel" |> translate |> text ]
                                            ]

                                        Updating key ->
                                            [ button [ onClick Update ]
                                                [ "Update" |> translate |> text ]
                                            , button [ onClick (ToggleEdit (Just key)) ]
                                                [ "Cancel" |> translate |> text ]
                                            , button [ onClick ToggleDelete ]
                                                [ "Delete" |> translate |> text ]
                                            ]

                                Nothing ->
                                    [ button
                                        [ id "admin__add"
                                        , onClick (ToggleEdit Nothing)
                                        ]
                                        [ "Add" |> translate |> text ]
                                    ]
            in
            div [ id "admin__edit-buttons" ] buttons_
    in
    case model.users of
        Just users ->
            section [ id "admin__users" ]
                [ buttons
                , table users
                , editView_
                , deleteView translate model.deleting
                ]

        _ ->
            section [ id "admin__users" ]
                [ div [ id "admin__loading" ] [ svgSprite "loading" ] ]


editView : Editing -> User -> Html Msg
editView mode user =
    case mode of
        Adding ->
            form [ id "admin__edit", onSubmit Add ]
                [ input
                    [ id "user__email"
                    , type_ "text"
                    , value user.email
                    , onInput UpdateEmail
                    ]
                    []
                , checkbox user.isAdmin UpdateIsAdmin
                ]

        Updating _ ->
            form [ id "admin__edit", onSubmit Update ]
                [ span [] [ text user.email ]
                , checkbox user.isAdmin UpdateIsAdmin
                ]


deleteView : (String -> String) -> Maybe User -> Html Msg
deleteView translate maybeUser =
    let
        ( hidden_, email ) =
            case maybeUser of
                Just user ->
                    ( False, user.email )

                Nothing ->
                    ( True, "" )
    in
    div [ id "admin__delete", hidden hidden_ ]
        [ h2 []
            [ translate "Are you sure you want to delete"
                ++ " "
                ++ email
                ++ "?"
                |> text
            ]
        , button [ onClick Delete ] [ translate "Delete" |> text ]
        , button [ onClick ToggleDelete ] [ translate "Cancel" |> text ]
        ]
