module Page.Schedule.TimeTracking exposing
    ( Model
    , Msg
    , Project
    , initCmd
    , initModel
    , projectDecoder
    , update
    , view
    )

import Config exposing (apis)
import Html
    exposing
        ( Html
        , button
        , div
        , form
        , h2
        , input
        , label
        , section
        , span
        , text
        )
import Html.Attributes as Attr
    exposing
        ( attribute
        , class
        , for
        , hidden
        , id
        , type_
        , value
        )
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Random
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , expectJson
        , expectWhatever
        , get
        , handleError
        , post
        )
import Utils.Misc exposing (focus)



-- model


type alias Project =
    { id : Int
    , name : String
    , color : Int
    }


type EditMode
    = Adding
    | Updating


type alias Model =
    { nextColor : Int
    , editingProject : Maybe ( EditMode, Project )
    }


initProject : Int -> Project
initProject nextColor =
    { id = 0
    , name = ""
    , color = nextColor
    }


initModel : Model
initModel =
    { nextColor = 285
    , editingProject = Nothing
    }



-- update


type Msg
    = NoOp
    | NewNextColor Int
    | ToggleEditProject (Maybe Project)
    | UpdateProjectName String
    | UpdateProjectColor String
    | AddProject
    | UpdateProject
    | DeleteProject
    | ReceivedProject EditMode (Result DetailedError Project)
    | ReceivedDeleteProject (Result DetailedError ())


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , project : Maybe Project
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , project = Nothing
            }
    in
    case msg of
        NoOp ->
            return

        NewNextColor color ->
            { return | model = { model | nextColor = color } }

        ToggleEditProject maybeProject ->
            case model.editingProject of
                Just _ ->
                    { return
                        | model = { model | editingProject = Nothing }
                    }

                Nothing ->
                    case maybeProject of
                        Just project ->
                            { return
                                | model =
                                    { model | editingProject = Just ( Updating, project ) }
                                , cmd = focus NoOp "edit-project__name"
                            }

                        Nothing ->
                            { return
                                | model =
                                    { model
                                        | editingProject =
                                            Just ( Adding, initProject model.nextColor )
                                    }
                                , cmd = focus NoOp "edit-project__name"
                            }

        UpdateProjectName name ->
            case model.editingProject of
                Just ( mode, project ) ->
                    { return
                        | model =
                            { model
                                | editingProject =
                                    Just ( mode, { project | name = name } )
                            }
                    }

                Nothing ->
                    return

        UpdateProjectColor color_ ->
            case model.editingProject of
                Just ( mode, project ) ->
                    let
                        color =
                            color_
                                |> String.toInt
                                |> Maybe.withDefault 0
                    in
                    { return
                        | model =
                            { model
                                | editingProject =
                                    Just ( mode, { project | color = color } )
                            }
                    }

                Nothing ->
                    return

        AddProject ->
            case model.editingProject of
                Just ( Adding, project ) ->
                    { return
                        | cmd = addProject tokens project
                        , updateLastMsg = True
                    }

                Just ( Updating, _ ) ->
                    return

                Nothing ->
                    return

        UpdateProject ->
            case model.editingProject of
                Just ( Updating, project ) ->
                    { return
                        | cmd = updateProject tokens project
                        , updateLastMsg = True
                    }

                Just ( Adding, _ ) ->
                    return

                Nothing ->
                    return

        DeleteProject ->
            case model.editingProject of
                Just ( Updating, project ) ->
                    { return
                        | cmd = deleteProject tokens project
                        , updateLastMsg = True
                    }

                Just ( Adding, _ ) ->
                    return

                Nothing ->
                    return

        ReceivedProject editMode result ->
            case result of
                Ok project ->
                    { return
                        | model = { model | editingProject = Nothing }
                        , project = Just project
                        , cmd = Random.generate NewNextColor (Random.int 0 360)
                    }

                Err err ->
                    let
                        error =
                            case editMode of
                                Adding ->
                                    handleError err "There was an error creating the project."

                                Updating ->
                                    handleError err "There was an error updating the project."
                    in
                    { return
                        | error = Just error
                    }

        ReceivedDeleteProject result ->
            case result of
                Ok _ ->
                    { return | model = { model | editingProject = Nothing } }

                Err err ->
                    let
                        error =
                            handleError err "There was an error deleting the project."
                    in
                    { return
                        | error = Just error
                    }


addProject : Tokens -> Project -> Cmd Msg
addProject tokens project =
    let
        body =
            Encode.object
                [ ( "name", Encode.string project.name )
                , ( "color", Encode.int project.color )
                ]
    in
    post tokens
        { url = apis.schedule ++ "/projects/add"
        , body = body
        , expect = expectJson (ReceivedProject Adding) projectDecoder
        }


updateProject : Tokens -> Project -> Cmd Msg
updateProject tokens project =
    let
        body =
            Encode.object
                [ ( "id", Encode.int project.id )
                , ( "name", Encode.string project.name )
                , ( "color", Encode.int project.color )
                ]
    in
    post tokens
        { url = apis.schedule ++ "/projects/update"
        , body = body
        , expect = expectJson (ReceivedProject Updating) projectDecoder
        }


deleteProject : Tokens -> Project -> Cmd Msg
deleteProject tokens project =
    let
        body =
            Encode.object
                [ ( "id", Encode.int project.id )
                ]
    in
    post tokens
        { url = apis.schedule ++ "/projects/delete"
        , body = body
        , expect = expectWhatever ReceivedDeleteProject
        }


initCmd : Cmd Msg
initCmd =
    Random.generate NewNextColor (Random.int 0 360)



-- view


view : (String -> String) -> Model -> List Project -> Html Msg
view translate model projects =
    section [ id "time-tracking" ]
        [ projectsView translate projects
        , editProjectView translate model.editingProject
        ]


projectsView : (String -> String) -> List Project -> Html Msg
projectsView translate projects =
    let
        projectElements project =
            [ span
                [ class "project-name"
                , attribute "style"
                    ("--project-color: "
                        ++ String.fromInt project.color
                        ++ ";"
                    )
                ]
                [ text project.name ]
            , span [] []
            , span [] []
            , span [] []
            , span [] []
            ]

        projects_ =
            projects
                |> List.sortBy .id
                |> List.map projectElements
                |> List.concat
    in
    div [ id "projects" ]
        (List.concat
            [ [ span [ class "header" ] [ "Project" |> translate |> text ]
              , span [ class "header" ] [ "Today" |> translate |> text ]
              , span [ class "header" ] [ "This week" |> translate |> text ]
              , span [ class "header" ] [ "Last week" |> translate |> text ]
              , span [ class "header" ] [ "This month" |> translate |> text ]
              ]
            , projects_
            , [ button
                    [ onClick (ToggleEditProject Nothing) ]
                    [ "New project" |> translate |> text ]
              ]
            ]
        )


editProjectView : (String -> String) -> Maybe ( EditMode, Project ) -> Html Msg
editProjectView translate maybeProject =
    let
        hidden_ =
            case maybeProject of
                Just _ ->
                    False

                Nothing ->
                    True

        nameValue =
            maybeProject
                |> Maybe.map Tuple.second
                |> Maybe.map .name
                |> Maybe.withDefault ""

        colorValue =
            maybeProject
                |> Maybe.map Tuple.second
                |> Maybe.map .color
                |> Maybe.map String.fromInt
                |> Maybe.withDefault "0"
    in
    form [ id "edit-project", onSubmit AddProject, hidden hidden_ ]
        [ h2 [] [ "New project" |> translate |> text ]
        , label
            [ for "edit-project__name" ]
            [ "Name" |> translate |> text ]
        , input
            [ id "edit-project__name"
            , type_ "text"
            , value nameValue
            , onInput UpdateProjectName
            ]
            []
        , label
            [ for "edit-project__color" ]
            [ "Colour" |> translate |> text ]
        , input
            [ id "edit-project__color"
            , type_ "number"
            , Attr.min "0"
            , Attr.max "360"
            , value colorValue
            , onInput UpdateProjectColor
            ]
            []
        , span
            [ attribute "style" ("--project-color: " ++ colorValue ++ ";") ]
            []
        , button
            [ type_ "button", onClick (ToggleEditProject Nothing) ]
            [ "Cancel" |> translate |> text ]
        , button
            [ type_ "submit" ]
            [ "Add" |> translate |> text ]
        ]



-- decoder


projectDecoder : Decoder Project
projectDecoder =
    Decode.map3 Project
        (Decode.field "id" Decode.int)
        (Decode.field "name" Decode.string)
        (Decode.field "color" Decode.int)
