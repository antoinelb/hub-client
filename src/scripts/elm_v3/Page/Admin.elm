module Page.Admin exposing
    ( Model(..)
    , Msg
    , fromPage
    , initCmd
    , initModel
    , navItems
    , routeParser
    , update
    , view
    )

import Html exposing (Html, a, div, h1, main_, section, text)
import Html.Attributes exposing (href, id)
import Page.Admin.Auth as Auth
import Page.Links exposing (Link)
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Http exposing (Tokens)
import Utils.Icons exposing (svgSprite)



-- model


type Model
    = Index
    | Auth Auth.Model


initModel : Model
initModel =
    Index


pageToText : Model -> String
pageToText page =
    case page of
        Index ->
            "Index"

        Auth _ ->
            "Auth"



-- update


type Msg
    = AuthMsg Auth.Msg


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case ( msg, model ) of
        ( AuthMsg subMsg, Auth model_ ) ->
            let
                return_ =
                    Auth.update subMsg model_ tokens
            in
            { return
                | model = Auth return_.model
                , cmd = Cmd.map AuthMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( _, _ ) ->
            return


initCmd : Model -> Tokens -> Cmd Msg
initCmd model tokens =
    case model of
        Index ->
            Cmd.none

        Auth _ ->
            Auth.initCmd tokens |> Cmd.map AuthMsg



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        content =
            case model of
                Index ->
                    indexView translate

                Auth subModel ->
                    Auth.view translate subModel |> Html.map AuthMsg

        title =
            case model of
                Index ->
                    "Admin"

                Auth _ ->
                    "Auth"
    in
    main_ [ id "admin" ]
        [ h1 [] [ title |> translate |> text ]
        , content
        ]


indexView : (String -> String) -> Html msg
indexView translate =
    let
        links =
            [ Auth Auth.initModel ]
                |> List.map
                    (\page_ ->
                        a
                            [ fromPage page_ |> href ]
                            [ page_ |> pageToText |> translate |> text ]
                    )
    in
    section [ id "admin-table" ] links


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "shield"
    , title = { text = "Admin", link = "/admin" }
    , items =
        [ { text = "Auth", link = "/admin/auth" }
        ]
    }



-- routing


routeParser : Parser (Model -> a) a
routeParser =
    Parser.s "admin"
        </> Parser.oneOf
                [ Parser.map Index Parser.top
                , Parser.map (Auth Auth.initModel) (Parser.s "auth")
                ]


fromPage : Model -> String
fromPage page =
    let
        url =
            case page of
                Index ->
                    ""

                Auth _ ->
                    "/auth"
    in
    "/admin" ++ url
