module Page.Schedule exposing
    ( Model
    , Msg
    , Page
    , fromPage
    , initCmd
    , initModel
    , navItems
    , routeParser
    , update
    , view
    )

import Config exposing (apis)
import Html exposing (Html, h1, main_, text)
import Html.Attributes exposing (id)
import Json.Decode as Decode
import Page.Links exposing (Link)
import Page.Schedule.TimeTracking as TimeTracking exposing (Project, projectDecoder)
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , expectJson
        , get
        , handleError
        , post
        )
import Utils.Icons exposing (svgSprite)



-- model


type Page
    = DayPlan
    | EventTracking
    | TimeTracking TimeTracking.Model
    | ToDo


type alias Model =
    { page : Page
    , projects : Maybe (List Project)
    }


initModel : Page -> Model
initModel page =
    { page = page
    , projects = Nothing
    }



-- update


type Msg
    = TimeTrackingMsg TimeTracking.Msg
    | ReceivedProjects (Result DetailedError (List Project))


update :
    Msg
    -> Model
    -> Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case ( msg, model.page ) of
        ( TimeTrackingMsg subMsg, TimeTracking subModel ) ->
            let
                return_ =
                    TimeTracking.update subMsg subModel tokens

                projects =
                    case return_.project of
                        Just project ->
                            model.projects
                                |> Maybe.withDefault []
                                |> List.filter (\project_ -> project_.id /= project.id)
                                |> List.append [ project ]
                                |> Just

                        Nothing ->
                            model.projects
            in
            { return
                | model =
                    { model
                        | page = TimeTracking return_.model
                        , projects = projects
                    }
                , cmd = Cmd.map TimeTrackingMsg return_.cmd
                , error = return_.error
                , updateLastMsg = return_.updateLastMsg
            }

        ( ReceivedProjects result, _ ) ->
            case result of
                Ok projects ->
                    { return
                        | model = { model | projects = Just projects }
                    }

                Err err ->
                    let
                        error =
                            handleError err "There was an error retrieving the projects."
                    in
                    { return
                        | error = Just error
                    }

        ( _, _ ) ->
            return


getProjects : Tokens -> Cmd Msg
getProjects tokens =
    get tokens
        { url = apis.schedule ++ "/projects/"
        , expect = expectJson ReceivedProjects (Decode.list projectDecoder)
        }


initCmd : Maybe Model -> Model -> Tokens -> Cmd Msg
initCmd oldModel newModel tokens =
    let
        dataCmd =
            case oldModel of
                Just _ ->
                    Cmd.none

                Nothing ->
                    Cmd.batch [ getProjects tokens ]

        pageCmd =
            case newModel.page of
                TimeTracking _ ->
                    TimeTracking.initCmd |> Cmd.map TimeTrackingMsg

                _ ->
                    Cmd.none
    in
    Cmd.batch [ dataCmd, pageCmd ]



-- view


view : (String -> String) -> Model -> Html Msg
view translate model =
    let
        body =
            case model.page of
                TimeTracking subModel ->
                    TimeTracking.view translate
                        subModel
                        (Maybe.withDefault [] model.projects)
                        |> Html.map TimeTrackingMsg

                _ ->
                    text ""
    in
    main_ [ id "schedule" ]
        [ h1 [] [ "Schedule" |> translate |> text ]
        , body
        ]


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "calendar"
    , title = { text = "Schedule", link = "/schedule" }
    , items =
        [ { text = "Day Plan", link = "/schedule" }
        , { text = "Event Tracking", link = "/schedule/event-tracking" }
        , { text = "Time Tracking", link = "/schedule/time-tracking" }
        , { text = "To do", link = "/schedule/todo" }
        ]
    }



-- routing


routeParser : Parser (Page -> a) a
routeParser =
    Parser.s "schedule"
        </> Parser.oneOf
                [ Parser.map DayPlan Parser.top
                , Parser.map EventTracking (Parser.s "event-tracking")
                , Parser.map (TimeTracking TimeTracking.initModel)
                    (Parser.s "time-tracking")
                , Parser.map ToDo (Parser.s "todo")
                ]


fromPage : Page -> String
fromPage page =
    let
        url =
            case page of
                DayPlan ->
                    ""

                EventTracking ->
                    "/event-tracking"

                TimeTracking _ ->
                    "/time-tracking"

                ToDo ->
                    "/todo"
    in
    "/schedule" ++ url
