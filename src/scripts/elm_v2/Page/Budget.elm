module Page.Budget exposing
    ( Model
    , Page
    , fromPage
    , initModel
    , navItems
    , routeParser
    , view
    )

import Html exposing (Html, text)
import Page.Links exposing (Link)
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Icons exposing (svgSprite)



-- model


type Page
    = Accounts
    | Transactions


type alias Model =
    { page : Page }


initModel : Page -> Model
initModel page =
    { page = page }



-- view


view : (String -> String) -> Html msg
view translate =
    "Budget" |> translate |> text


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "dollar-sign"
    , title = { text = "Budget", link = "/budget" }
    , items =
        [ { text = "Accounts", link = "/budget" }
        , { text = "Transactions", link = "/budget/transactions" }
        ]
    }



-- routing


routeParser : Parser (Page -> a) a
routeParser =
    Parser.s "budget"
        </> Parser.oneOf
                [ Parser.map Accounts Parser.top
                , Parser.map Transactions (Parser.s "transactions")
                ]


fromPage : Page -> String
fromPage page =
    let
        url =
            case page of
                Accounts ->
                    ""

                Transactions ->
                    "/transactions"
    in
    "/budget" ++ url
