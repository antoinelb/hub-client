module Page.Links exposing (Link)


type alias Link =
    { text : String
    , link : String
    }
