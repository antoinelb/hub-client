module Page.Schedule exposing
    ( Model
    , Page
    , fromPage
    , initModel
    , navItems
    , routeParser
    , view
    )

import Html exposing (Html, text)
import Page.Links exposing (Link)
import Url.Parser as Parser exposing ((</>), Parser)
import Utils.Icons exposing (svgSprite)



-- model


type Page
    = DayPlan
    | EventTracking
    | TimeTracking


type alias Model =
    { page : Page }


initModel : Page -> Model
initModel page =
    { page = page }



-- view


view : (String -> String) -> Html msg
view translate =
    "Schedule" |> translate |> text


navItems : { icon : String, title : Link, items : List Link }
navItems =
    { icon = "calendar"
    , title = { text = "Schedule", link = "/schedule" }
    , items =
        [ { text = "Day Plan", link = "/schedule" }
        , { text = "Event Tracking", link = "/schedule/event-tracking" }
        , { text = "Time Tracking", link = "/schedule/time-tracking" }
        ]
    }



-- routing


routeParser : Parser (Page -> a) a
routeParser =
    Parser.s "schedule"
        </> Parser.oneOf
                [ Parser.map DayPlan Parser.top
                , Parser.map EventTracking (Parser.s "event-tracking")
                , Parser.map TimeTracking (Parser.s "time-tracking")
                ]


fromPage : Page -> String
fromPage page =
    let
        url =
            case page of
                DayPlan ->
                    ""

                EventTracking ->
                    "/event-tracking"

                TimeTracking ->
                    "/time-tracking"
    in
    "/schedule" ++ url
