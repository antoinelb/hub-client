module Page.Loading exposing (view)

import Html exposing (Html, h3, main_, text)
import Html.Attributes exposing (id)
import Utils.Icons exposing (svgSprite)



-- view


view : (String -> String) -> String -> Html msg
view translate text_ =
    main_ [ id "loading" ]
        [ svgSprite "loading"
        , h3 [] [ text_ |> translate |> text ]
        ]
