module Main exposing (main)

import Auth
import Browser
import Browser.Navigation as Nav
import Config
import Html exposing (Html, a, button, div, h1, header, span, text)
import Html.Attributes exposing (class, href, id)
import Html.Events exposing (onClick)
import I18Next exposing (initialTranslations, translationsDecoder)
import Json.Decode as Decode
import Json.Encode as Encode
import Menu
import Nav
import Page
import Routing
import Settings.Language as Language
import Settings.Theme as Theme
import Task
import Time
import Url exposing (Url)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (toCmd)



-- main


type alias Flags =
    { theme : String
    , language : String
    , translations : Encode.Value
    , refreshToken : Maybe String
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked >> InternalMsg
        , onUrlChange = UrlChanged >> InternalMsg
        }



-- model


type alias Error =
    { text : String
    , expiry : Time.Posix
    }


type alias Model =
    { key : Nav.Key
    , errors : List Error
    , lastMsg : Maybe Msg
    , showDebugList : Bool
    , currentTime : Time.Posix
    , timeZone : Time.Zone
    , menu : Menu.Model
    , auth : Auth.Model
    , page : Page.Model
    }


initModel : Flags -> Routing.Route -> Nav.Key -> Model
initModel { theme, language, translations, refreshToken } route key =
    let
        translations_ =
            case Decode.decodeValue translationsDecoder translations of
                Ok t ->
                    t

                Err _ ->
                    initialTranslations
    in
    { key = key
    , errors = []
    , lastMsg = Nothing
    , showDebugList = False
    , currentTime = Time.millisToPosix 0
    , timeZone = Time.utc
    , menu = Menu.initModel theme language translations_
    , auth = Auth.initModel refreshToken
    , page = Page.initModel route
    }



-- update


type InternalMsg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | UpdateTime Time.Posix
    | UpdateTimeZone Time.Zone
    | RemoveError Int


type ExternalMsg
    = MenuMsg Menu.Msg
    | AuthMsg Auth.Msg
    | PageMsg Page.Msg


type DebugMsg
    = ToggleDebug
    | RefreshUserInfo


type Msg
    = InternalMsg InternalMsg
    | ExternalMsg ExternalMsg
    | DebugMsg DebugMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( model_, cmd_, refreshLastMsg ) =
            case msg of
                InternalMsg subMsg ->
                    let
                        ( m, c ) =
                            updateInternal subMsg model
                    in
                    ( m, c, False )

                ExternalMsg subMsg ->
                    updateExternal subMsg model

                DebugMsg subMsg ->
                    updateDebug subMsg model

        refreshAccessToken =
            model_.errors
                |> List.reverse
                |> List.head
                |> Maybe.map .text
                |> Maybe.map (\t -> t == "The token expired.")
                |> Maybe.withDefault False
    in
    if refreshAccessToken then
        case model.auth.tokens of
            Just _ ->
                ( { model_
                    | errors =
                        model_.errors
                            |> List.reverse
                            |> List.drop 1
                            |> List.reverse
                  }
                , Cmd.batch
                    [ cmd_
                    , Auth.getAccessToken model.auth.tokens |> Cmd.map (AuthMsg >> ExternalMsg)
                    ]
                )

            Nothing ->
                ( model_, cmd_ )

    else if refreshLastMsg then
        ( { model_ | lastMsg = Just msg }, cmd_ )

    else
        ( model_, cmd_ )


updateInternal : InternalMsg -> Model -> ( Model, Cmd Msg )
updateInternal msg model =
    case msg of
        LinkClicked request ->
            case request of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                route =
                    Routing.toRoute url
            in
            case route of
                Routing.SignOut ->
                    ( model
                    , Auth.signOut model.auth.tokens
                        |> Cmd.map (AuthMsg >> ExternalMsg)
                    )

                _ ->
                    ( model
                    , Routing.toRoute url
                        |> Page.updatePage model.page
                        |> Cmd.map (PageMsg >> ExternalMsg)
                    )

        UpdateTime time ->
            let
                errors =
                    model.errors
                        |> List.filter
                            (\error ->
                                Time.posixToMillis error.expiry
                                    > Time.posixToMillis time
                            )
            in
            ( { model | currentTime = time, errors = errors }, Cmd.none )

        UpdateTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )

        RemoveError idx ->
            let
                errors =
                    model.errors
                        |> List.indexedMap Tuple.pair
                        |> List.filter (\error -> Tuple.first error /= idx)
                        |> List.map Tuple.second
            in
            ( { model | errors = errors }, Cmd.none )


updateExternal : ExternalMsg -> Model -> ( Model, Cmd Msg, Bool )
updateExternal msg model =
    case msg of
        MenuMsg subMsg ->
            let
                result_ =
                    Menu.update subMsg model.menu

                errors =
                    case result_.error of
                        Just err ->
                            model.errors
                                ++ [ toError err model.currentTime ]

                        Nothing ->
                            model.errors
            in
            ( { model | menu = result_.model, errors = errors }
            , Cmd.map (MenuMsg >> ExternalMsg) result_.cmd
            , result_.updateLastMsg
            )

        AuthMsg subMsg ->
            let
                result_ =
                    Auth.update subMsg model.auth model.key

                errors =
                    case result_.error of
                        Just err ->
                            model.errors
                                ++ [ toError err model.currentTime ]

                        Nothing ->
                            model.errors

                routeCmd =
                    if result_.updatePage then
                        case result_.model.user of
                            Just _ ->
                                case model.page of
                                    Page.Auth _ ->
                                        Routing.Home
                                            |> Routing.fromRoute
                                            |> Nav.pushUrl model.key

                                    Page.Loading _ route ->
                                        route
                                            |> Routing.fromRoute
                                            |> Nav.pushUrl model.key

                                    _ ->
                                        Cmd.none

                            Nothing ->
                                case model.page of
                                    Page.Auth _ ->
                                        Cmd.none

                                    Page.Loading _ route ->
                                        route
                                            |> Routing.fromRoute
                                            |> Just
                                            |> Routing.Auth
                                            |> Routing.fromRoute
                                            |> Nav.pushUrl model.key

                                    _ ->
                                        model.page
                                            |> Page.pageToRoute
                                            |> Routing.fromRoute
                                            |> Just
                                            |> Routing.Auth
                                            |> Routing.fromRoute
                                            |> Nav.pushUrl model.key

                    else
                        Cmd.none

                ( resubmitCmd, lastMsg ) =
                    case model.lastMsg of
                        Just msg_ ->
                            if result_.reSubmit then
                                ( toCmd msg_, Nothing )

                            else
                                ( Cmd.none, Just msg_ )

                        Nothing ->
                            ( Cmd.none, Nothing )
            in
            ( { model | auth = result_.model, errors = errors, lastMsg = lastMsg }
            , Cmd.batch
                [ Cmd.map (AuthMsg >> ExternalMsg) result_.cmd
                , routeCmd
                , resubmitCmd
                ]
            , result_.updateLastMsg
            )

        PageMsg subMsg ->
            let
                result_ =
                    Page.update subMsg model.page model.auth.tokens

                errors =
                    case result_.error of
                        Just err ->
                            model.errors
                                ++ [ toError err model.currentTime ]

                        Nothing ->
                            model.errors

                ( cmd, authModel ) =
                    case ( model.page, result_.tokens ) of
                        ( Page.Auth subModel, Just tokens ) ->
                            let
                                authModel_ =
                                    Auth.updateTokens model.auth tokens
                            in
                            ( Cmd.batch
                                [ Cmd.map (PageMsg >> ExternalMsg) result_.cmd
                                , subModel.next
                                    |> Routing.fromRoute
                                    |> Nav.pushUrl model.key
                                , Auth.getUserInfo authModel_.tokens
                                    |> Cmd.map (AuthMsg >> ExternalMsg)
                                ]
                            , authModel_
                            )

                        ( _, _ ) ->
                            ( Cmd.map (PageMsg >> ExternalMsg) result_.cmd
                            , model.auth
                            )
            in
            ( { model | page = result_.model, errors = errors, auth = authModel }
            , cmd
            , result_.updateLastMsg
            )


updateDebug : DebugMsg -> Model -> ( Model, Cmd Msg, Bool )
updateDebug msg model =
    case msg of
        ToggleDebug ->
            ( { model | showDebugList = not model.showDebugList }
            , Cmd.none
            , False
            )

        RefreshUserInfo ->
            ( model
            , Auth.getUserInfo model.auth.tokens |> Cmd.map (AuthMsg >> ExternalMsg)
            , True
            )


initCmd : Model -> Routing.Route -> Cmd Msg
initCmd model route =
    let
        signOut =
            case route of
                Routing.SignOut ->
                    True

                _ ->
                    False
    in
    Cmd.batch
        [ Task.perform (UpdateTimeZone >> InternalMsg) Time.here
        , Task.perform (UpdateTime >> InternalMsg) Time.now
        , Page.initCmd model.page model.auth.tokens |> Cmd.map (PageMsg >> ExternalMsg)
        , Auth.initCmd model.auth.tokens signOut |> Cmd.map (AuthMsg >> ExternalMsg)
        ]



-- view


view : Model -> Browser.Document Msg
view model =
    let
        translate =
            Language.initTranslate model.menu.language

        class_ =
            case model.menu.theme of
                Theme.Dark ->
                    ""

                Theme.Light ->
                    "light"

        ( title, body ) =
            Page.view translate model.page model.timeZone
    in
    { title = title
    , body =
        [ div [ id "root", class class_ ]
            [ errorView translate model.errors
            , headerView translate model
            , Nav.view translate model.page
            , body |> Html.map (PageMsg >> ExternalMsg)
            ]
        ]
    }


errorView : (String -> String) -> List Error -> Html Msg
errorView translate errors =
    div [ id "error" ]
        (List.indexedMap
            (\i error ->
                span [ onClick (InternalMsg (RemoveError i)) ]
                    [ error.text |> translate |> text ]
            )
            errors
            ++ [ span [] [] ]
        )


headerView : (String -> String) -> Model -> Html Msg
headerView translate model =
    let
        signedIn =
            case model.auth.user of
                Just _ ->
                    True

                Nothing ->
                    False
    in
    header [ id "header" ]
        [ a [ href "/" ] [ h1 [] [ "Hub" |> translate |> text ] ]
        , debugView translate model.showDebugList
        , headerButtonsView translate model
        , Menu.view translate model.menu signedIn |> Html.map (MenuMsg >> ExternalMsg)
        ]


debugView : (String -> String) -> Bool -> Html Msg
debugView translate showDebugList =
    if Config.debug then
        let
            class_ =
                if showDebugList then
                    "debug--open"

                else
                    ""
        in
        div [ id "debug", class class_ ]
            [ button [ onClick (DebugMsg ToggleDebug) ]
                [ svgSprite "bug" ]
            , div
                []
                [ button [ onClick (DebugMsg RefreshUserInfo) ]
                    [ "Refresh info" |> translate |> text ]
                ]
            ]

    else
        text ""


headerButtonsView : (String -> String) -> Model -> Html msg
headerButtonsView translate model =
    let
        adminButton =
            case model.auth.user of
                Just user ->
                    if user.isAdmin then
                        case model.page of
                            Page.Admin _ ->
                                a [ href "/" ]
                                    [ svgSprite "home"
                                    , "Home" |> translate |> text
                                    ]

                            _ ->
                                a [ href "/admin" ]
                                    [ svgSprite "shield"
                                    , "Admin" |> translate |> text
                                    ]

                    else
                        text ""

                Nothing ->
                    text ""
    in
    div [ id "header-buttons" ] [ adminButton ]



-- subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [ updateTime ]


updateTime : Sub Msg
updateTime =
    if Config.debug then
        Sub.none

    else
        Time.every 1000 (UpdateTime >> InternalMsg)



-- utils


toError : String -> Time.Posix -> Error
toError text_ currentTime =
    { text = text_
    , expiry =
        currentTime
            |> Time.posixToMillis
            |> (\t -> t + (Config.errorExpiry * 1000))
            |> Time.millisToPosix
    }



-- init


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        route =
            Routing.toRoute url

        model =
            initModel flags route key
    in
    ( model, initCmd model route )
