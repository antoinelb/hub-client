module Nav exposing (view)

import Html exposing (Html, a, div, nav, span, text)
import Html.Attributes exposing (class, href, id)
import Page
import Page.Budget as Budget
import Page.Home as Home
import Page.Schedule as Schedule
import Utils.Icons exposing (svgSprite)


view : (String -> String) -> Page.Model -> Html msg
view translate page =
    let
        homeSection =
            case page of
                Page.Home ->
                    pageSection translate Home.navItems (Just "")

                _ ->
                    pageSection translate Home.navItems Nothing

        budgetSection =
            case page of
                Page.Budget subModel ->
                    pageSection translate
                        Budget.navItems
                        (subModel.page |> Budget.fromPage |> Just)

                _ ->
                    pageSection translate Budget.navItems Nothing

        scheduleSection =
            case page of
                Page.Schedule subModel ->
                    pageSection translate
                        Schedule.navItems
                        (subModel.page |> Schedule.fromPage |> Just)

                _ ->
                    pageSection translate Schedule.navItems Nothing
    in
    nav [ id "nav" ]
        [ homeSection
        , budgetSection
        , scheduleSection
        ]


pageSection :
    (String -> String)
    ->
        { icon : String
        , title : { text : String, link : String }
        , items : List { text : String, link : String }
        }
    -> Maybe String
    -> Html msg
pageSection translate { icon, title, items } selected =
    let
        toItem item =
            case selected of
                Just link ->
                    if link == item.link then
                        a [ href item.link, class "nav__section__selected-item" ]
                            [ item.text |> translate |> text ]

                    else
                        a [ href item.link ]
                            [ item.text |> translate |> text ]

                Nothing ->
                    a [ href item.link ]
                        [ item.text |> translate |> text ]

        class_ =
            case selected of
                Just _ ->
                    "nav__section nav__section--selected"

                Nothing ->
                    "nav__section"
    in
    div [ class class_ ]
        [ a [ href title.link, class "nav__section__title" ]
            [ svgSprite icon, span [] [ title.text |> translate |> text ] ]
        , div [ class "nav__section__items" ] (List.map toItem items)
        ]
