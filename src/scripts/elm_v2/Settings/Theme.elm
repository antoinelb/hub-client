port module Settings.Theme exposing (Model(..), Msg, initModel, themeButton, update)

import Browser.Dom as Dom
import Html exposing (Html, button, span, text)
import Html.Attributes exposing (id)
import Html.Events exposing (onClick)
import Task
import Utils.Icons exposing (svgSprite)



-- model


type Model
    = Dark
    | Light


initModel : String -> Model
initModel theme =
    case theme of
        "light" ->
            Light

        _ ->
            Dark



-- update


type Msg
    = NoOp
    | ToggleTheme


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToggleTheme ->
            case model of
                Dark ->
                    ( Light
                    , Cmd.batch
                        [ saveTheme "light"
                        , Task.attempt (\_ -> NoOp) (Dom.blur "theme")
                        ]
                    )

                Light ->
                    ( Dark
                    , Cmd.batch
                        [ saveTheme "dark"
                        , Task.attempt (\_ -> NoOp) (Dom.blur "theme")
                        ]
                    )



-- view


themeButton : (String -> String) -> Model -> Html Msg
themeButton translate model =
    let
        icon =
            case model of
                Dark ->
                    "sun"

                Light ->
                    "moon"

        text_ =
            case model of
                Dark ->
                    "Light"

                Light ->
                    "Dark"
    in
    button [ id "theme", onClick ToggleTheme ]
        [ svgSprite icon, span [] [ text_ ++ " theme" |> translate |> text ] ]



-- ports


port saveTheme : String -> Cmd msg
