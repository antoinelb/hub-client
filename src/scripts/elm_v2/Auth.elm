module Auth exposing
    ( Model
    , Msg
    , User
    , getAccessToken
    , getUserInfo
    , initCmd
    , initModel
    , initUser
    , signOut
    , update
    , updateTokens
    , userDecoder
    )

import Browser.Navigation as Nav
import Config exposing (apis)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode
import Utils.Http
    exposing
        ( DetailedError
        , Tokens
        , expectJson
        , expectWhatever
        , get
        , handleError
        , post
        )



-- model


type alias User =
    { email : String
    , isAdmin : Bool
    , pin : Maybe String
    }


type alias Model =
    { user : Maybe User
    , tokens : Maybe Tokens
    }


initUser : User
initUser =
    { email = ""
    , isAdmin = False
    , pin = Nothing
    }


initModel : Maybe String -> Model
initModel refreshToken =
    case refreshToken of
        Just token ->
            { user = Nothing
            , tokens =
                Just
                    { access = Nothing
                    , refresh = token
                    }
            }

        Nothing ->
            { user = Nothing
            , tokens = Nothing
            }



-- update


type Msg
    = ReceivedAccessToken (Result DetailedError { access : String })
    | ReceivedUserInfo (Result DetailedError User)
    | ReceivedSignOut (Result DetailedError ())


update :
    Msg
    -> Model
    -> Nav.Key
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , updatePage : Bool
        , reSubmit : Bool
        }
update msg model key =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , updatePage = False
            , reSubmit = False
            }
    in
    case msg of
        ReceivedAccessToken result ->
            case result of
                Ok { access } ->
                    case model.tokens of
                        Just tokens ->
                            let
                                cmd =
                                    case model.user of
                                        Just _ ->
                                            Cmd.none

                                        Nothing ->
                                            getUserInfo model.tokens
                            in
                            { return
                                | model =
                                    { model
                                        | tokens = Just { tokens | access = Just access }
                                    }
                                , cmd = cmd
                                , reSubmit = True
                            }

                        Nothing ->
                            { return
                                | error = Just "You have been logged out. Please log back in."
                                , updatePage = True
                            }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                        , updatePage = True
                    }

        ReceivedUserInfo result_ ->
            case result_ of
                Ok user_ ->
                    { return
                        | model = { model | user = Just user_ }
                        , error = Nothing
                        , updatePage = True
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                        , updatePage = True
                    }

        ReceivedSignOut result ->
            case result of
                Ok _ ->
                    { return
                        | model = { user = Nothing, tokens = Nothing }
                        , cmd = Nav.pushUrl key "/"
                    }

                Err err ->
                    let
                        error_ =
                            handleError err "There was an error accessing the server."

                        error =
                            if error_ == "You don't have access to this resource." then
                                Nothing

                            else
                                Just error_
                    in
                    { return
                        | error = error
                        , updatePage = True
                    }


getAccessToken : Maybe Tokens -> Cmd Msg
getAccessToken tokens =
    let
        tokenDecoder =
            Decode.map (\token -> { access = token }) (Decode.field "access-token" Decode.string)
    in
    get
        tokens
        { url = apis.auth ++ "/users/refresh"
        , expect = expectJson ReceivedAccessToken tokenDecoder
        }


getUserInfo : Maybe Tokens -> Cmd Msg
getUserInfo tokens =
    get tokens
        { url = apis.auth ++ "/users/info"
        , expect = expectJson ReceivedUserInfo userDecoder
        }


signOut : Maybe Tokens -> Cmd Msg
signOut tokens =
    post tokens
        { url = apis.auth ++ "/users/signout"
        , body = Encode.object []
        , expect = expectWhatever ReceivedSignOut
        }


initCmd : Maybe Tokens -> Bool -> Cmd Msg
initCmd tokens signOut_ =
    if signOut_ then
        signOut tokens

    else
        case tokens of
            Just _ ->
                getAccessToken tokens

            Nothing ->
                Cmd.none


updateTokens : Model -> { access : String, refresh : String } -> Model
updateTokens model { access, refresh } =
    { model | tokens = Just { access = Just access, refresh = refresh } }



-- decoders


userDecoder : Decoder User
userDecoder =
    Decode.map3 User
        (Decode.field "email" Decode.string)
        (Decode.field "is_admin" Decode.bool)
        (Decode.field "pin" (Decode.nullable Decode.string))
