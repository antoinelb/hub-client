module Utils.Http exposing
    ( DetailedError(..)
    , Tokens
    , expectJson
    , expectString
    , expectWhatever
    , get
    , getTask
    , handleError
    , post
    )

import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Encode exposing (Value)
import Task exposing (Task)


type DetailedError
    = BadUrl String
    | Timeout
    | NetworkError
    | BadStatus Int String
    | BadBody String


type alias Tokens =
    { refresh : String
    , access : Maybe String
    }


expectJson : (Result DetailedError a -> msg) -> Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    let
        toResult =
            \string ->
                Result.mapError Decode.errorToString (Decode.decodeString decoder string)

        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (toResult body)
    in
    Http.expectStringResponse toMsg <|
        resolve


expectString : (Result DetailedError String -> msg) -> Http.Expect msg
expectString toMsg =
    let
        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (Ok body)
    in
    Http.expectStringResponse toMsg <|
        resolve


expectWhatever : (Result DetailedError () -> msg) -> Http.Expect msg
expectWhatever toMsg =
    let
        toResult =
            \_ ->
                Ok ()

        resolve =
            \resp ->
                case resp of
                    Http.BadUrl_ url ->
                        Err (BadUrl url)

                    Http.Timeout_ ->
                        Err Timeout

                    Http.NetworkError_ ->
                        Err NetworkError

                    Http.BadStatus_ meta body ->
                        Err (BadStatus meta.statusCode body)

                    Http.GoodStatus_ _ body ->
                        Result.mapError BadBody (toResult body)
    in
    Http.expectStringResponse toMsg <|
        resolve


handleError : DetailedError -> String -> String
handleError error defaultMessage =
    case error of
        BadStatus _ error_ ->
            error_

        Timeout ->
            "The server took too long to reach."

        NetworkError ->
            "The server couldn't be reached. "
                ++ "Check your internet connection and extensions."

        _ ->
            defaultMessage


get : Maybe Tokens -> { url : String, expect : Http.Expect msg } -> Cmd msg
get maybeTokens { url, expect } =
    let
        headers =
            case maybeTokens of
                Just { access, refresh } ->
                    case access of
                        Just access_ ->
                            [ Http.header "refresh-token" refresh
                            , Http.header "access-token" access_
                            ]

                        Nothing ->
                            [ Http.header "refresh-token" refresh ]

                Nothing ->
                    []
    in
    Http.request
        { method = "GET"
        , url = url
        , body = Http.emptyBody
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


post : Maybe Tokens -> { url : String, body : Value, expect : Http.Expect msg } -> Cmd msg
post maybeTokens { url, body, expect } =
    let
        headers =
            case maybeTokens of
                Just { access, refresh } ->
                    case access of
                        Just access_ ->
                            [ Http.header "refresh-token" refresh
                            , Http.header "access-token" access_
                            ]

                        Nothing ->
                            [ Http.header "refresh-token" refresh ]

                Nothing ->
                    []
    in
    Http.request
        { method = "POST"
        , url = url
        , body = Http.jsonBody body
        , expect = expect
        , headers = headers
        , timeout = Nothing
        , tracker = Nothing
        }


getTask : { url : String, decoder : Decoder a } -> Task DetailedError a
getTask { url, decoder } =
    Http.riskyTask
        { method = "GET"
        , url = url
        , body = Http.emptyBody
        , resolver = Http.stringResolver <| handleJsonResponse <| decoder
        , headers = []
        , timeout = Nothing
        }


handleJsonResponse : Decoder a -> Http.Response String -> Result DetailedError a
handleJsonResponse decoder response =
    case response of
        Http.BadUrl_ url ->
            Err (BadUrl url)

        Http.Timeout_ ->
            Err Timeout

        Http.NetworkError_ ->
            Err NetworkError

        Http.BadStatus_ meta body ->
            Err (BadStatus meta.statusCode body)

        Http.GoodStatus_ _ body ->
            case Decode.decodeString decoder body of
                Err _ ->
                    Err (BadBody body)

                Ok result ->
                    Ok result
