module Utils.Table exposing (Model, Msg(..), initModel, update, view)

import Html exposing (Html, div, input, text)
import Html.Attributes exposing (class, placeholder)
import Html.Events exposing (onClick, onInput)



-- model


type alias Column data msg =
    { label : String
    , key : data -> String
    , query : String
    , msg : Maybe (String -> msg)
    }


type alias Model data msg =
    { columns : List (Column data msg)
    , sortBy : data -> String
    , reverseSort : Bool
    , index : data -> String
    }


initModel :
    List ( String, data -> String, Maybe (String -> msg) )
    -> (data -> String)
    -> Bool
    -> (data -> String)
    -> Model data msg
initModel columns sortBy reverseSort index =
    let
        toColumn ( label, key, msg ) =
            { label = label
            , key = key
            , query = ""
            , msg = msg
            }
    in
    { columns = List.map toColumn columns
    , sortBy = sortBy
    , reverseSort = reverseSort
    , index = index
    }



-- update


type Msg msg
    = SetQuery String String
    | ExternalMsg msg


update : Msg msg -> Model data msg -> Model data msg
update msg model =
    case msg of
        SetQuery label query ->
            let
                setQuery col =
                    if col.label == label then
                        { col | query = query }

                    else
                        col

                columns =
                    List.map setQuery model.columns
            in
            { model | columns = columns }

        ExternalMsg _ ->
            model



-- view


view : (String -> String) -> Model data msg -> List data -> Html (Msg msg)
view translate model data =
    let
        toHeader col =
            div []
                [ col.label |> translate |> text
                , input
                    [ placeholder "Filter"
                    , onInput (SetQuery col.label)
                    ]
                    []
                ]

        header =
            div [ class "table__header" ] (List.map toHeader model.columns)

        toCell row col =
            case col.msg of
                Just msg ->
                    div
                        [ class "table--clickable"
                        , onClick (model.index row |> msg)
                        ]
                        [ col.key row |> text ]
                        |> Html.map ExternalMsg

                Nothing ->
                    div [] [ col.key row |> text ]

        toRow row =
            List.map (toCell row) model.columns

        body =
            data
                |> filterData model
                |> List.map toRow
                |> List.concat
                |> div [ class "table__body" ]

        tableClass =
            "table-" ++ (List.length model.columns |> String.fromInt)
    in
    div [ "table " ++ tableClass |> class ]
        [ header, body ]



-- [ header ]


filterData : Model data msg -> List data -> List data
filterData model data =
    let
        filterColumn row col =
            col.key row |> String.contains col.query

        filterRow row =
            model.columns
                |> List.map (filterColumn row)
                |> List.foldl (&&) True

        sortBy row =
            let
                val =
                    model.sortBy row

                asNumber =
                    String.toInt val
            in
            case asNumber of
                Just _ ->
                    String.padLeft 32 '0' val

                Nothing ->
                    val
    in
    if model.reverseSort then
        List.filter filterRow data
            |> List.sortBy sortBy
            |> List.reverse

    else
        List.filter filterRow data
            |> List.sortBy sortBy
