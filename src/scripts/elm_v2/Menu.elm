module Menu exposing (Model, Msg(..), initModel, update, view)

import Html exposing (Html, a, aside, button, div, span, text)
import Html.Attributes exposing (attribute, class, href, id)
import Html.Events exposing (onClick)
import I18Next exposing (Translations)
import Settings.Language as Language
import Settings.Theme as Theme
import Utils.Events exposing (onEscape, stopPropagationOnClick)
import Utils.Icons exposing (svgSprite)
import Utils.Misc exposing (loseFocus)
import Utils.Touch as Touch



-- model


type alias Model =
    { menuOpen : Bool
    , touchPositionX : Maybe Float
    , theme : Theme.Model
    , language : Language.Model
    }


initModel : String -> String -> Translations -> Model
initModel theme language translations =
    { menuOpen = False
    , touchPositionX = Nothing
    , theme = Theme.initModel theme
    , language = Language.initModel translations language
    }



-- update


type Msg
    = NoOp
    | ToggleMenu
    | StartMenuSwipe Touch.Touch
    | EndMenuSwipe Touch.Touch
    | ThemeMsg Theme.Msg
    | LanguageMsg Language.Msg


update :
    Msg
    -> Model
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        }
update msg model =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            }
    in
    case msg of
        NoOp ->
            return

        ToggleMenu ->
            { return
                | model = { model | menuOpen = not model.menuOpen }
                , cmd = loseFocus NoOp "menu__button"
            }

        StartMenuSwipe touchEvent ->
            { return | model = { model | touchPositionX = Just touchEvent.clientX } }

        EndMenuSwipe touchEvent ->
            case model.touchPositionX of
                Nothing ->
                    return

                Just positionX ->
                    if model.menuOpen then
                        if positionX < touchEvent.clientX then
                            { return
                                | model =
                                    { model
                                        | menuOpen = False
                                        , touchPositionX = Nothing
                                    }
                            }

                        else
                            { return | model = { model | touchPositionX = Nothing } }

                    else if positionX > touchEvent.clientX then
                        { return
                            | model =
                                { model
                                    | menuOpen = True
                                    , touchPositionX = Nothing
                                }
                        }

                    else
                        { return
                            | model =
                                { model | touchPositionX = Nothing }
                        }

        ThemeMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Theme.update subMsg model.theme
            in
            { return
                | model = { model | theme = subModel }
                , cmd = Cmd.map ThemeMsg cmd
            }

        LanguageMsg subMsg ->
            let
                ( subModel, cmd ) =
                    Language.update subMsg model.language
            in
            { return
                | model = { model | language = subModel }
                , cmd = Cmd.map LanguageMsg cmd
            }



-- view


view : (String -> String) -> Model -> Bool -> Html Msg
view translate model signedIn =
    let
        attributes =
            if model.menuOpen then
                [ id "menu"
                , class "menu--open"
                , stopPropagationOnClick ToggleMenu
                , onEscape ToggleMenu
                ]

            else
                [ id "menu" ]
    in
    div attributes
        [ touchAreaView
        , menuIconView translate
        , asideView translate model signedIn
        ]


menuIconView : (String -> String) -> Html Msg
menuIconView translate =
    button
        [ id "menu__button"
        , attribute "aria-label" (translate "Menu")
        , stopPropagationOnClick ToggleMenu
        ]
        [ svgSprite "menu" ]


asideView : (String -> String) -> Model -> Bool -> Html Msg
asideView translate model signedIn =
    let
        signOutBtn_ =
            if signedIn then
                signOutBtn translate

            else
                text ""
    in
    aside
        [ stopPropagationOnClick NoOp
        , Touch.onTouchEvent Touch.TouchStart StartMenuSwipe
        , Touch.onTouchEvent Touch.TouchEnd EndMenuSwipe
        ]
        [ div [ id "menu__bg" ] []
        , Theme.themeButton translate model.theme |> Html.map ThemeMsg
        , Language.languageButton model.language |> Html.map LanguageMsg
        , signOutBtn_
        ]


touchAreaView : Html Msg
touchAreaView =
    div
        [ id "menu__swipe-area"
        , Touch.onTouchEvent Touch.TouchStart StartMenuSwipe
        , Touch.onTouchEvent Touch.TouchEnd EndMenuSwipe
        , stopPropagationOnClick ToggleMenu
        ]
        []


signOutBtn : (String -> String) -> Html Msg
signOutBtn translate =
    a [ href "/signout", onClick ToggleMenu ]
        [ svgSprite "logout", span [] [ "Sign out" |> translate |> text ] ]
