module Routing exposing (Route(..), fromRoute, toRoute)

import Page.Admin as Admin
import Page.Budget as Budget
import Page.Schedule as Schedule
import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, top)
import Url.Parser.Query as Query


type Route
    = Home
    | SignOut
    | Auth (Maybe String)
    | Admin Admin.Model
    | Budget Budget.Page
    | Schedule Schedule.Page


routeParser : Parser (Route -> a) a
routeParser =
    List.concat
        [ [ map Home top
          , map SignOut (s "signout")
          , map Auth (s "auth" <?> Query.string "next")
          ]
        , [ map Admin Admin.routeParser ]
        , [ map Budget Budget.routeParser ]
        , [ map Schedule Schedule.routeParser ]
        ]
        |> oneOf


toRoute : Url -> Route
toRoute url =
    Maybe.withDefault Home (parse routeParser url)


fromRoute : Route -> String
fromRoute route =
    case route of
        Home ->
            "/"

        SignOut ->
            "/signout"

        Auth maybeNext ->
            case maybeNext of
                Just next ->
                    if next == "/" || next == "/auth" || next == "/signout" then
                        "/auth"

                    else
                        "/auth?next=" ++ next

                Nothing ->
                    "/auth"

        Admin subPage ->
            Admin.fromPage subPage

        Budget subPage ->
            Budget.fromPage subPage

        Schedule subPage ->
            Schedule.fromPage subPage
