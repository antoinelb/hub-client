port module Page exposing
    ( Model(..)
    , Msg
    , initCmd
    , initModel
    , pageToRoute
    , update
    , updatePage
    , view
    )

import Html exposing (Html)
import Page.Admin as Admin
import Page.Auth as Auth
import Page.Budget as Budget
import Page.Home as Home
import Page.Loading as Loading
import Page.Schedule as Schedule
import Routing exposing (Route)
import Time
import Url
import Utils.Http exposing (Tokens)
import Utils.Misc exposing (toCmd)



-- model


type Model
    = Home
    | Loading String Route
    | Auth Auth.Model
    | Admin Admin.Model
    | Budget Budget.Model
    | Schedule Schedule.Model


initModel : Route -> Model
initModel route =
    Loading "" route


routeToPage : Model -> Route -> Model
routeToPage previousModel route =
    case route of
        Routing.Home ->
            Home

        Routing.SignOut ->
            Home

        Routing.Auth next ->
            let
                next_ =
                    next
                        |> Maybe.withDefault "/"
                        |> Url.fromString
                        |> Maybe.map Routing.toRoute
                        |> Maybe.withDefault Routing.Home
            in
            Auth (Auth.initModel next_)

        Routing.Admin admin ->
            Admin admin

        Routing.Budget page ->
            Budget (Budget.initModel page)

        Routing.Schedule page ->
            Schedule (Schedule.initModel page)


pageToRoute : Model -> Route
pageToRoute model =
    case model of
        Home ->
            Routing.Home

        Loading _ route ->
            route

        Auth _ ->
            Routing.Auth Nothing

        Admin subModel ->
            Routing.Admin subModel

        Budget subModel ->
            Routing.Budget subModel.page

        Schedule subModel ->
            Routing.Schedule subModel.page



-- update


type Msg
    = UpdatePage Model
    | AuthMsg Auth.Msg
    | AdminMsg Admin.Msg


update :
    Msg
    -> Model
    -> Maybe Tokens
    ->
        { model : Model
        , cmd : Cmd Msg
        , error : Maybe String
        , updateLastMsg : Bool
        , tokens : Maybe { access : String, refresh : String }
        }
update msg model tokens =
    let
        return =
            { model = model
            , cmd = Cmd.none
            , error = Nothing
            , updateLastMsg = False
            , tokens = Nothing
            }
    in
    case ( model, msg ) of
        ( _, UpdatePage page ) ->
            let
                ( cmd, updateLastMsg ) =
                    case page of
                        Loading _ _ ->
                            ( updateLoading True, False )

                        _ ->
                            ( Cmd.batch [ updateLoading False, initCmd page tokens ]
                            , True
                            )
            in
            { return | model = page, cmd = cmd, updateLastMsg = updateLastMsg }

        ( Auth subModel, AuthMsg subMsg ) ->
            let
                result_ =
                    Auth.update subMsg subModel
            in
            { return
                | model = Auth result_.model
                , cmd = Cmd.map AuthMsg result_.cmd
                , error = result_.error
                , updateLastMsg = result_.updateLastMsg
                , tokens = result_.tokens
            }

        ( Admin subModel, AdminMsg subMsg ) ->
            let
                result_ =
                    Admin.update subMsg subModel tokens
            in
            { return
                | model = Admin result_.model
                , cmd = Cmd.map AdminMsg result_.cmd
                , error = result_.error
                , updateLastMsg = result_.updateLastMsg
            }

        ( _, _ ) ->
            return


updatePage : Model -> Route -> Cmd Msg
updatePage previousModel route =
    routeToPage previousModel route |> UpdatePage |> toCmd


initCmd : Model -> Maybe Tokens -> Cmd Msg
initCmd model tokens =
    case model of
        Home ->
            Cmd.none

        Loading _ _ ->
            updateLoading True

        Auth _ ->
            Auth.initCmd |> Cmd.map AuthMsg

        Admin subModel ->
            Admin.initCmd subModel tokens |> Cmd.map AdminMsg

        Budget _ ->
            Cmd.none

        Schedule _ ->
            Cmd.none



-- view


view : (String -> String) -> Model -> Time.Zone -> ( String, Html Msg )
view translate model timeZone =
    case model of
        Home ->
            ( "Hub", Home.view translate )

        Loading text _ ->
            ( "Hub", Loading.view translate text )

        Auth subModel ->
            ( translate "Auth" ++ " | Hub"
            , Auth.view translate subModel |> Html.map AuthMsg
            )

        Admin subModel ->
            ( translate "Admin" ++ " | Hub"
            , Admin.view translate subModel |> Html.map AdminMsg
            )

        Budget _ ->
            ( translate "Budget" ++ " | Hub", Budget.view translate )

        Schedule _ ->
            ( translate "Schedule" ++ " | Hub", Schedule.view translate )



-- ports


port updateLoading : Bool -> Cmd msg
