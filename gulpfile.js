require("dotenv").config();

const babel = require("rollup-plugin-babel");
const browserSync = require("browser-sync").create();
const commonjs = require("rollup-plugin-commonjs");
const concat = require("gulp-concat");
const crypto = require("crypto");
const cssNano = require("cssnano");
const cssPresetEnv = require("postcss-preset-env");
const elm = require("gulp-elm");
const fs = require("fs");
const gulp = require("gulp");
const gulpTerser = require("gulp-terser");
const jsonMinify = require("gulp-jsonminify");
const merge = require("merge-stream");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const replace = require("gulp-replace");
const resolve = require("rollup-plugin-node-resolve");
const rollup = require("gulp-better-rollup");
const sourcemaps = require("gulp-sourcemaps");
const stylelint = require("stylelint");
const svgSprite = require("gulp-svg-sprite");
const { eslint } = require("rollup-plugin-eslint");
const { terser } = require("rollup-plugin-terser");
const { version } = require("./package.json");

const hash = path =>
  new Promise((resolve, reject) => {
    fs.createReadStream(path)
      .on("error", reject)
      .pipe(crypto.createHash("sha256").setEncoding("hex"))
      .once("finish", function () {
        resolve(this.read()); // eslint-disable-line
      });
  });

async function index(useProdBuild) {
  let build = gulp.src("./src/index.html");

  if (useProdBuild) {
    if (
      !fs.existsSync("./dist/styles.css") ||
      !fs.existsSync("./dist/scripts")
    ) {
      setTimeout(index, 500);
    }
    const stylesHash = await hash("./dist/styles.css");
    const scriptsHash = await hash("./dist/scripts.js");
    build = build
      .pipe(
        replace(
          /(\s*<link rel="stylesheet" href="\/styles.css)(?:\?hash=.{8})?(" \/>)/,
          "$1?hash=" + stylesHash.slice(0, 8) + "$2",
        ),
      )
      .pipe(
        replace(
          /(\s*<script src="\/scripts.js)(?:\?hash=.{8})?(" defer><\/script>)/,
          "$1?hash=" + scriptsHash.slice(0, 8) + "$2",
        ),
      );
  }

  build = build.pipe(gulp.dest("./dist"));

  return build;
}

function styles(useProdBuild) {
  let build = gulp.src("./src/styles/**/*.css", {
    allowEmpty: true,
  });

  if (!useProdBuild) {
    build = build.pipe(sourcemaps.init());
  }

  build = build
    .pipe(
      postcss([
        stylelint({ rules: { "no-empty-source": null } }),
        cssPresetEnv({ stage: 0 }),
        cssNano(),
      ]),
    )
    .on("error", function (err) {
      console.log(err.toString());
      this.emit("end"); // eslint-disable-line
    })
    .pipe(concat("styles.css"));

  if (!useProdBuild) {
    build = build.pipe(sourcemaps.write("."));
  }

  build = build.pipe(gulp.dest("./dist"));

  if (!useProdBuild) {
    build = build.pipe(browserSync.stream());
  }

  return build;
}

async function scripts(useProdBuild) {
  process.env["ICONS_HASH"] = await hash("./dist/icons/icons.svg");
  process.env["VERSION"] = version;

  let elmBuild = gulp.src("src/scripts/elm/Main.elm");
  let jsBuild = gulp.src("src/scripts/js/index.js");

  if (!useProdBuild) {
    elmBuild = elmBuild.pipe(sourcemaps.init());
    jsBuild = jsBuild.pipe(sourcemaps.init());
  }

  elmBuild = elmBuild
    .pipe(elm({ optimize: useProdBuild, debug: !useProdBuild }))
    .on("error", function (err) {
      console.log(err.toString());
      this.emit("end"); // eslint-disable-line
    })
    .pipe(
      replace(
        /{{\s*(\S+)\s*}}/g,
        (_, variable, __, ___) => process.env[variable.toUpperCase()],
      ),
    );
  jsBuild = jsBuild
    .pipe(
      rollup(
        {
          plugins: [
            eslint({
              exclude: ["**.css"],
            }),
            babel({ babelrc: false, exclude: ["node_modules/**"] }),
            resolve({
              mainFields: ["jsnext", "module", "main", "browser"],
            }),
            commonjs(),
            terser(),
          ],
          external: ["Elm"],
        },
        {
          name: "scripts",
          format: "iife",
          globals: {
            Elm: "Elm",
          },
        },
      ),
    )
    .on("error", function (err) {
      console.log(err.toString());
      this.emit("end"); // eslint-disable-line
    })
    .pipe(
      replace(
        /{{\s*(\S+)\s*}}/g,
        (_, variable, __, ___) => process.env[variable.toUpperCase()],
      ),
    );

  let build = merge(elmBuild, jsBuild)
    .pipe(concat("scripts.js"))
    .pipe(gulpTerser());

  if (!useProdBuild) {
    build = build.pipe(sourcemaps.write("."));
  }

  build = build.pipe(gulp.dest("./dist"));

  if (!useProdBuild) {
    build = build.pipe(browserSync.stream());
  }

  return build;
}

function icons() {
  return gulp
    .src("./src/assets/icons/*.svg")
    .pipe(
      svgSprite({
        mode: {
          symbol: {
            inline: true,
          },
        },
        svg: {
          namespaceIDs: false,
          rootAttributes: {
            id: "svg-sprite",
            class: "svg-sprite",
          },
        },
      }),
    )
    .pipe(replace(' style="position:absolute"', "")) // eslint-disable-line
    .pipe(rename("icons.svg"))
    .pipe(gulp.dest("dist/icons"));
}

function favicons() {
  return gulp.src("./src/assets/favicons/*").pipe(gulp.dest("dist/icons"));
}

function translations() {
  return gulp
    .src("./src/translations.json")
    .pipe(jsonMinify())
    .pipe(gulp.dest("./dist"));
}

function fonts() {
  return gulp.src("./src/assets/fonts/*").pipe(gulp.dest("dist/fonts"));
}

function manifest() {
  return gulp
    .src("./src/manifest.json")
    .pipe(jsonMinify())
    .pipe(gulp.dest("./dist"));
}

async function serviceWorker(useProdBuild) {
  let build = gulp.src("src/scripts/js/service_worker.js");

  if (!useProdBuild) {
    build = build.pipe(sourcemaps.init());
  }

  build = build
    .pipe(
      rollup(
        {
          plugins: [
            eslint({
              exclude: ["**.css"],
            }),
            babel({ babelrc: false, exclude: ["node_modules/**"] }),
            resolve({
              mainFields: ["jsnext", "module", "main", "browser"],
            }),
            commonjs(),
            terser(),
          ],
        },
        {
          name: "scripts",
          format: "iife",
        },
      ),
    )
    .on("error", function (err) {
      console.log(err.toString());
      this.emit("end"); // eslint-disable-line
    });

  if (!useProdBuild) {
    build = build.pipe(sourcemaps.write("."));
  }

  build = build.pipe(gulp.dest("./dist"));

  if (!useProdBuild) {
    build = build.pipe(browserSync.stream());
  }

  return build;
}

function watch() {
  browserSync.init({
    server: "dist",
    notify: false,
    open: false,
    single: true,
  });
  gulp.watch("./src/index.html", async () => await index(false));
  gulp.watch("./dist/index.html").on("change", browserSync.reload);
  gulp.watch("./src/styles/**/*.css", () => styles(false));
  gulp.watch("./src/scripts/elm/**/*.elm", () => scripts(false));
  gulp.watch(
    ["./src/scripts/js/**/*.js", "!./src/scripts/js/service_worker"],
    () => scripts(false),
  );
  gulp.watch("./src/assets/icons/*.svg", icons);
  gulp.watch("./src/assets/favicons/*", favicons);
  gulp.watch("./src/translations.json", translations);
  gulp.watch("./src/assets/fonts/*", fonts);
  gulp.watch("./src/manifest.json", manifest);
  gulp.watch("./src/scripts/js/service_worker.js", () => serviceWorker(false));
  gulp.watch("./dist/translations.json").on("change", browserSync.reload);
  gulp.watch("./dist/manifest.json").on("change", browserSync.reload);
}

const build = gulp.series(
  icons,
  gulp.parallel(
    async function scripts_() {
      return await scripts(true);
    },
    function styles_() {
      return styles(true);
    },
    favicons,
    translations,
    fonts,
    manifest,
    async function serviceWorker_() {
      return await serviceWorker(true);
    },
  ),
  async function index_() {
    return await index(true);
  },
);

const buildDev = gulp.series(
  icons,
  gulp.parallel(
    async function scripts_() {
      return await scripts(false);
    },
    function styles_() {
      return styles(false);
    },
    favicons,
    translations,
    fonts,
    manifest,
    async function serviceWorker_() {
      return await serviceWorker(true);
    },
    async function index_() {
      return await index(false);
    },
  ),
);

exports.index = async () => await index(false);
exports.styles = () => styles(false);
exports.scripts = () => scripts(false);
exports.icons = icons;
exports.favicons = favicons;
exports.translations = translations;
exports.fonts = fonts;
exports.manifest = manifest;
exports.build = build;
exports["build-dev"] = buildDev;
exports.watch = watch;
