module.exports = {
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  globals: {
    Elm: "readonly",
  },
  extends: "eslint:recommended",
  parserOptions: {
    ecmaVersion: 11,
    sourceType: "module",
  },
  rules: {
    indent: ["error", 2],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "arrow-parens": "off",
    "require-jsdoc": "off",
    "guard-for-in": "off",
    "spaced-comment": "off",
    "object-curly-spacing": ["error", "always"],
    "operator-linebreak": "off",
    "space-before-function-paren": "off",
    "no-unused-vars": ["error", { argsIgnorePattern: "^_+$" }],
  },
};
